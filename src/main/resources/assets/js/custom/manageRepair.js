/**
 * Created by RAVI KALUARACHCHI on 11/11/2019.
 */

$(document).ready(function () {

    table = $('#repairTable').DataTable({
        processing: true,
        ajax: {
            url: "/repair/getAllRepairs",
            "type": "POST",
            "data": function () {
                var repairSearchCriteria = {};
                repairSearchCriteria.repairCode = $("#searchRepairByCode").val();
                return repairSearchCriteria;
            },
            dataSrc: function (json) {

                console.log(json.tableData);
                return json.tableData;
            }
        },
        columns: [
            {title: "Repair Id", data: "repairId"},
            {title: "Repair Name", data: "repairName"},
            {title: "Repair Code", data: "repairCode"},
            {title: "Repair Type", data: "repairType"},
            {title: "Repair Charge", data: "repairCharge"},
            {title: "Description", data: "repairDescription"}
        ],
        columnDefs: [
            /*{
             targets: [8],
             visible: false,
             searchable: false
             }/*,
             {
             targets: [2],
             visible: false,
             searchable: false
             },
             {
             targets: [7],
             visible: false,
             searchable: false
             }*!/*/
        ]
    });


    // table click event
    $('#repairTable tbody').on('click', 'tr', function () {
        var data = table.row(this).data();
        fillFormData(data);
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
            clickedRow = false;
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            clickedRow = true;
        }
        disableUpdate(false);
        disableDelete(false);
    });



    $("#repairSave").click(function (e) {

        e.stopImmediatePropagation();
        e.preventDefault();
        const swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
        });

        swalWithBootstrapButtons({
            title: 'Confirmation',
            text: "Are you sure you want Save it?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Save',
            cancelButtonText: 'Cancel'
        }).then(function (result) {
            if (result.value) {
        var repairDTO = {};
        repairDTO.repairId = $("#repairId").val();
        repairDTO.repairName = $("#repairName").val();
        repairDTO.repairCode = $("#repairCode").val();
        repairDTO.repairType = $("#repairType").val();
        repairDTO.repairCharge = $("#repairCharge").val();
        repairDTO.repairDescription = $("#repairDescription").val();


        $.ajax({
            url: '/repair/saveRepair', // or whatever
            dataType: 'json',
            type: 'post',
            data: repairDTO,
            success: function (response) {
                if (response.success) {
                    swal({
                        type: 'success',
                        title: 'Save',
                        text: 'Repair Save Successful'
                    })
                    repairClear(true);
                } else {
                    swal(
                        'Error',
                        'There is an issue in saving',
                        'error'
                    )
                }
            }
        })
        ;
            }
            }
        );
    });


    $("#repairUpdateButton1").click(function (e) {
        e.stopImmediatePropagation();
        e.preventDefault();
        const swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
        });

        swalWithBootstrapButtons({
            title: 'Confirmation',
            text: "Are you sure you want Update it?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Update',
            cancelButtonText: 'Cancel'
        }).then(function (result) {
                if (result.value) {

                    var repairDTO = {};
                    repairDTO.repairId = $("#repairId").val();
                    repairDTO.repairName = $("#repairName").val();
                    repairDTO.repairCode = $("#repairCode").val();
                    repairDTO.repairType = $("#repairType").val();
                    repairDTO.repairCharge = $("#repairCharge").val();
                    repairDTO.repairDescription = $("#repairDescription").val();

                    $.ajax({
                        url: '/repair/updateRepair', // or whatever
                        dataType: 'json',
                        type: 'post',
                        data: repairDTO,
                        success: function (response) {
                            if (response.success) {
                                swal({
                                    type: 'success',
                                    title: 'Updated',
                                    text: 'Row is successfully updated'
                                })
                                repairClear(true);
                            } else {
                                swal(
                                    'Error',
                                    'There is an issue in updating',
                                    'error'
                                )
                            }
                        }
                    })
                    ;
                }
            }
        );
    });


    disableFormData(true);
    disableUpdate(true);
    disableDelete(true);
    $("#repairSave").show();
    $("#repairUpdateButton1").hide();

    $("#searchRepair").click(function (e) {
        e.preventDefault();
        table.ajax.reload();
    });


});

function repairRefresh() {
    location.reload();
}

function repairAdd() {
    resetForm();
    disableFormData(false);
    disableUpdate(true);
    /*disableDelete(true);*/
    table.ajax.reload();
    /*$("#repairSave").prop('disabled', true).removeClass( 'disabled' );*/
    $("#repairUpdateButton1").hide();


}

function repairClear(disable) {
    $("#repairDeleteButton").prop('disabled', disable);
    resetForm();
    disableFormData(true);
    disableUpdate(true);
    disableDelete(true);
    table.ajax.reload();
    $( "#repairUpdateButton1" ).hide();
    $( "#repairSave" ).show();
    $( "#repairSave" ).prop('disabled', true).addClass( 'disabled' );

}


function resetForm() {
    $(':input', '#repairForm')
        .not(':button, :submit, :reset, :hidden')
        .val('')
        .removeAttr('checked')
        .removeAttr('selected');
}

function disableFormData(disable) {
    $("#repairId").prop('disabled', disable);
    $("#repairName").prop('disabled', disable);
    $("#repairCode").prop('disabled', disable);
    $("#repairType").prop('disabled', disable);
    $("#repairCharge").prop('disabled', disable);
    $("#repairDescription").prop('disabled', disable);

}

function fillFormData(row) {
    $("#repairId").val(row.repairId);
    $("#repairName").val(row.repairName);
    $("#repairCode").val(row.repairCode);
    $("#repairType").val(row.repairType);
    $("#repairCharge").val(row.repairCharge);
    $("#repairDescription").val(row.repairDescription);

}


function disableUpdate(disable) {
    $("#repairUpdateButton").prop('disabled', disable);

}

function disableDelete(disable) {
    $("#repairDeleteButton").prop('disabled', disable);

}



// Edit Button Click Function
function repairUpdate() {
    if (clickedRow) {
        disableFormData(true);
        $("#repairName").prop('disabled', false);
        /*$("#repairCode").prop('disabled', false);*/
        $("#repairType").prop('disabled', false);
        $("#repairCharge").prop('disabled', false);
        $("#repairDescription").prop('disabled', false);
        $("#repairUpdateButton1").prop('disabled',false).removeClass('disabled');
        $("#repairUpdateButton1").show();
        $("#repairSave").hide();

    } else {
        alert("Please Select a Row first !!!!");
    }
}



/*function repairDelete() {
    if (clickedRow) {
        const swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
        });

        swalWithBootstrapButtons({
            title: 'Confirmation',
            text: "Are you sure you want delete it?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Delete',
            cancelButtonText: 'Cancel'
        }).then(function (result) {
                if (result.value) {
                    var repairDTO = {};
                    repairDTO.repairId = $("#repairId").val();
                    repairDTO.repairName = $("#repairName").val();
                    repairDTO.repairCode = $("#repairCode").val();
                    repairDTO.repairType = $("#repairType").val();
                    repairDTO.repairCharge = $("#repairCharge").val();
                    repairDTO.repairDescription = $("#repairDescription").val();

                    $.ajax({
                        url: '/repair/deleteRepair', // or whatever
                        dataType: 'json',
                        type: 'post',
                        data: repairDTO,
                        success: function (response) {
                            if (response.success) {
                                swal({
                                    type: 'success',
                                    title: 'Deleted',
                                    text: 'Row is successfully deleted'
                                })
                                repairClear(true);
                            } else {
                                swal(
                                    'Error',
                                    'There is an issue in deleting',
                                    'error'
                                )
                            }
                        }
                    })
                    ;
                }
            }
        );
    } else {
        swal({
            type: 'error',
            title: 'Error',
            text: 'Please Select a Row first'
        })
    }

}*/



/*function searchEmployee(e) {
 e.preventDefault();
 table.ajax.reload();
 }*/
