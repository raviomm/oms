/**
 * Created by RAVI KALUARACHCHI on 12/15/2017.
 */

$(document).ready(function () {

    var table = $("#reportTable").DataTable({
        "aaSorting": [],
        "scrollX": true,
        "scrollY": 530,
        "scrollCollapse": true,
        "lengthMenu": [
            [100, 400, 1000, 5000, -1],
            [100, 400, 1000, 5000, "All"]
        ],
        "retrieve": true,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
    });


    //year month picker for the
    $(function () {
        $('#datetimepicker11').datetimepicker({
            viewMode: 'years',
            format: 'MM/YYYY',
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
        });
       $('#datetimepicker11').on('dp.change', function(n, o) {
           console.log('hkjkjk')
           reportTypeOnChange();
        });
    });


});




//Reset Button for the Report Page
function reportResetButton() {
    
    var table = $("#reportTable").DataTable({
        "aaSorting": [],
        "scrollX": true,
        "scrollY": 530,
        "scrollCollapse": true,
        "lengthMenu": [
            [100, 400, 1000, 5000, -1],
            [100, 400, 1000, 5000, "All"]
        ],
        "retrieve": true
    });


}

function reportTypeOnChange() {

    var year = $("#datetimepicker11").find("input").val().split("/")[1];
    var month = $("#datetimepicker11").find("input").val().split("/")[0];

    if(year == undefined || year == null){
        year = "2019";
    }
    if(month == undefined || month == null){
        month = "11";
    }

    var selectedType = $("#reportType").val();
    $('#reportTable').DataTable().destroy();
    $('#reportTable').empty();
    $("#dateInput1").removeAttr('disabled');
    switch(selectedType) {
        case 'Monthly Jobs':

            table = $('#reportTable').DataTable({
                processing: true,
                ajax: {
                    url: "/report/getAllJobsMonthWise?year="+year+"&month="+month,
                    "type" : "get",
                    "data":function () {
                        var jobSearchCriteria = {};
                        return jobSearchCriteria;
                    },
                    dataSrc: function (json) {

                        console.log(json.tableData);
                        return json.tableData;
                    }

                },
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                columns: [
                    { title: "Job Id" , data: "id"},
                    { title: "Date" , data: "jobDate"},
                    { title: "Customer Name", data: "customerLastName"},
                    { title: "Customer Contact" ,data: "customerContactNumber" },
                    {title: "Technician" ,data:"jobTechnician"}

                ],
                columnDefs: [
                    {

                    }

                ],

            });
            break;

        case 'Monthly Items':
            table = $('#reportTable').DataTable({
                processing : true,
                ajax: {
                    url: "/report/getAllItemsMonthWise?year="+year+"&month="+month,
                    "type" : "GET",
                    "data":function () {
                        var itemsSearchCriteria = {};
                        return itemsSearchCriteria;
                    },
                    dataSrc: function (json) {

                        console.log(json.tableData);
                        return json.tableData;
                    }

                },
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                columns: [
                    { title: "Job Id" , data: "job.id"},
                    { title: "Item Id" , data: "item.itemId"},
                    { title: "item Name" , data: "item.itemName"},
                    { title: "Count", data: "count"}
                ],
                columnDefs: [
                    {

                    }
                ]

            });
            break;

        case 'Monthly Repairs':
            table = $('#reportTable').DataTable({
                processing : true,
                ajax: {
                    url: "/report/getAllRepairsMonthWise?year="+year+"&month="+month,
                    "type" : "GET",
                    "data":function () {
                        var repairSearchCriteria = {};
                        return repairSearchCriteria;
                    },
                    dataSrc: function (json) {

                        console.log(json.tableData);
                        if(json.tableData.length > 0 ){
                            json.tableData.forEach(function(row) {
                                console.log(row);

                                var jDate = row.job.jobDate;
                                row.job.jobDate = new Date(jDate).toString().substr(0,15);
                            });
                            return json.tableData;
                        }
                        return [];

                    }

                },
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                columns: [
                    {title: "Job Id", data: "job.id" },
                    {title: "Repair Name", data: "repair.repairName"},
                    {title: "Repair Charge", data: "repair.repairCharge"},
                    {title: "Repair Date", data: "job.jobDate"}

                ],
                columnDefs: [
                    {

                    }
                ]

            });
            break;
        case 'Monthly Rents':
            $("#dateInput1").attr('disabled','disabled');
            table = $('#reportTable').DataTable({
                processing : true,
                ajax: {
                    url:  "/report/getAllRentsMonthWise?year="+year+"&month="+month,
                    "type" : "GET",
                    "data":function () {
                        var rentSearchCriteria = {};
                        return rentSearchCriteria;
                    },
                    dataSrc: function (json) {

                        console.log(json.tableData);
                        return json.tableData;
                    }

                },
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],

                columns: [
                    {title: "Job Id", data: "job.id"},
                    {title: "Rent code", data: "rent.rentCode" },
                    {title: "Resource", data: "rent.rentResource"},
                    {title: "Charge", data: "rent.rentCharge"}




                ],
                columnDefs: [
                    {

                    }
                ]

            });
            break;

        case 'Monthly Invoices':
            $("#dateInput1").attr('disabled','disabled');
            table = $('#reportTable').DataTable({
                processing : true,
                ajax: {
                    url:  "/report/getAllInvocesMonthWise?year="+year+"&month="+month,
                    "type" : "GET",
                    "data":function () {
                        var invoiceSearchCriteria = {};
                        return invoiceSearchCriteria;
                    },
                    dataSrc: function (json) {

                        console.log(json.tableData);
                        console.log(json.tableData);
                        if(json.tableData.length > 0 ){
                            json.tableData.forEach(function(row) {
                                console.log(row);

                                var iDate = row.invoice.invoiceDate;
                                row.invoice.invoiceDate = new Date(iDate).toString().substr(0,15);
                            });
                            return json.tableData;
                        }
                        return [];
                    }

                },
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],

                columns: [
                    {title: "Invoice Id", data: "invoice.invoiceId" },
                    {title: "Item Name", data: "item.itemName"},
                    {title: "Quantity", data: "quantity"},
                    {title: "Date", data: "invoice.invoiceDate"},
                    {title: "Invoice Number", data: "invoice.invoiceNumber"},
                    {title: "Supplier Name", data: "invoice.vendorName"},
                    {title: "Supplier Contact", data: "invoice.vendorContactNumber"}


                ],
                columnDefs: [
                    {

                    }
                ]

            });

            break;

        default:
            var table = $("#reportTable").DataTable({
                "aaSorting": [],
                "scrollX": true,
                "scrollY": 530,
                "scrollCollapse": true,
                "lengthMenu": [
                    [100, 400, 1000, 5000, -1],
                    [100, 400, 1000, 5000, "All"]
                ],
                "retrieve": true
            });
    }

}