/**
 * Created by RAVI KALUARACHCHI on 12/8/2017.
 */
//var clickedRow = false;
//var table;


$(document).ready(function(){

    table = $('#routeTable').DataTable( {
        processing : true,
        ajax: {
            url: "/route/getAllRoutes",
            "type" : "POST",
            "data":function () {
                var routeSearchCriteria = {};
                routeSearchCriteria.routeName = $( "#searchRouteId" ).val();
                return routeSearchCriteria;
            },
            dataSrc: function (json) {

                console.log(json.tableData);
                return json.tableData;
            }
        },
        columns: [
            { title: "route Id" , data: "routeId"},
            { title: "Route Name", data: "routeName"},
            { title: "Origin",data: "routeOrigin" },
            { title: "Destination", data: "routeDestination" },
            { title: "Distance", data: "routeDistance" },
            { title: "Duration", data: "routeDuration" },
            { title: "routeStatus" ,data: "routeStatus" }
        ],
        columnDefs: [
            {
                targets: [ 0 ],
                visible: false,
                searchable: false
            }
        ]
    } );

    // table click event
    $('#routeTable tbody').on('click', 'tr', function () {
        var data = table.row( this ).data();
        fillFormData(data);
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
            clickedRow = false;
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            clickedRow = true;
        }
        disableUpdate(false);
        disableDelete(false);
    } );


    console.log("............. come to vehicle page");
    $( "#routeSaveButton" ).click(function() {
        var routeDTO = {};
        routeDTO.routeId = $( "#routeId" ).val();
        routeDTO.routeName = $( "#routeName" ).val();
        routeDTO.routeOrigin = $( "#routeOrigin" ).val();
        routeDTO.routeDestination = $( "#routeDestination" ).val();
        routeDTO.routeDistance = $( "#routeDistance" ).val();
        routeDTO.routeDuration = $( "#routeDuration" ).val();
        routeDTO.routeStatus = $( "#routeStatus" ).val();

        $.ajax({
            url : '/route/saveRoute', // or whatever
            dataType : 'json',
            type: 'post',
            data: routeDTO,
            success : function (response) {
                if(response.success){
                    alert("Successfully Saved the Route ");
                    routeClear(true);
                } else {
                    alert("Error Saving Route  !!!");
                }
            }
        })
        ;
    });

    $( "#routeUpdateButton" ).click(function(e) {
        e.stopImmediatePropagation();
        e.preventDefault();
        const swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
        });

        swalWithBootstrapButtons({
            title: 'Confirmation',
            text: "Are you sure you want Update it?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Update',
            cancelButtonText: 'Cancel'
        }).then(function (result) {
            if (result.value) {

        var routeDTO = {};
        routeDTO.routeId = $( "#routeId" ).val();
        routeDTO.routeName = $( "#routeName" ).val();
        routeDTO.routeOrigin = $( "#routeOrigin" ).val();
        routeDTO.routeDestination = $( "#routeDestination" ).val();
        routeDTO.routeDistance = $( "#routeDistance" ).val();
        routeDTO.routeDuration = $( "#routeDuration" ).val();
        routeDTO.routeStatus = $( "#routeStatus" ).val();

        $.ajax({
            url : '/route/updateRoute', // or whatever
            dataType : 'json',
            type: 'post',
            data: routeDTO,
            success : function (response) {
                if(response.success){
                    swal({
                        type: 'success',
                        title: 'Updated',
                        text: 'Row is successfully updated'
                    })
                    routeClear(true);
                } else {
                    swal(
                        'Error',
                        'There is an issue in updating',
                        'error'
                    )
                }
            }
        })
        ;
            }
            }
        );
    });


    disableFormData(true);
    disableUpdate(true);
    disableDelete(true);
    $( "#routeUpdateButton" ).hide();
    $( "#routeSaveButton" ).show();

    $("#searchRoute").click(function (e) {
        e.preventDefault();
        table.ajax.reload();
    });

});


function routeAdd() {
    resetForm();
    disableFormData(false);
    disableUpdate(true);
    disableDelete(true);
    table.ajax.reload();
    $( "#routeSaveButton" ).show();
    $( "#routeSaveButton" ).prop('disabled', false).removeClass( 'disabled' );
    $( "#routeUpdateButton" ).hide();

}

function resetForm() {
    $(':input','#routeForm')
        .not(':button, :submit, :reset, :hidden')
        .val('')
        .removeAttr('checked')
        .removeAttr('selected');
}

function disableFormData(disable) {
    $("#routeName").prop('disabled', disable);
    $("#routeOrigin").prop('disabled', disable);
    $("#routeDestination").prop('disabled', disable);
    $("#routeDistance").prop('disabled', disable);
    $("#routeDuration").prop('disabled', disable);
    $("#routeStatus").prop('disabled', disable);


}

function fillFormData(row) {
    $("#routeId").val(row.routeId);
    $("#routeName").val(row.routeName);
    $("#routeOrigin").val(row.routeOrigin);
    $("#routeDestination").val(row.routeDestination);
    $("#routeDistance").val(row.routeDistance);
    $("#routeDuration").val(row.routeDuration);
    $("#routeStatus").val(row.routeStatus);

}


function disableUpdate(disable) {
    $("#routeEditButton").prop('disabled', disable);

}

function disableDelete(disable) {
    $("#routeDeleteButton").prop('disabled', disable);

}

function routeClear(disable) {
    $("#routeDeleteButton").prop('disabled', disable);
    resetForm();
    disableFormData(true);
    disableUpdate(true);
    disableDelete(true);
    table.ajax.reload();
    $( "#routeSaveButton" ).prop('disabled', true).addClass( 'disabled' );
    $( "#routeUpdateButton" ).hide();
    $( "#routeSaveButton" ).show();

}

function routeEdit(){
    if(clickedRow){
        disableFormData(true);
        $("#routeDuration").prop('disabled', false);
        $("#routeStatus").prop('disabled', false);
        $("#routeDistance").prop('disabled', false);
        $( "#routeUpdateButton" ).show();
        $( "#routeSaveButton" ).hide();

    } else {
        alert("Please Select a Row first !!!!");
    }
}

function routeDelete(){
    if(clickedRow){

        const swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
        });

        swalWithBootstrapButtons({
            title: 'Confirmation',
            text: "Are you sure you want delete it?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Delete',
            cancelButtonText: 'Cancel'
        }).then(function (result) {
            if (result.value) {
            var routeDTO = {};
            routeDTO.routeId = $( "#routeId" ).val();
            routeDTO.routeName = $( "#routeName" ).val();
            routeDTO.routeOrigin = $( "#routeOrigin" ).val();
            routeDTO.routeDestination = $( "#routeDestination" ).val();
            routeDTO.routeDistance = $( "#routeDistance" ).val();
            routeDTO.routeDuration = $( "#routeDuration" ).val();
            routeDTO.status = $( "#routeStatus" ).val();
            $.ajax({
                url : '/route/deleteRoute', // or whatever
                dataType : 'json',
                type: 'post',
                data: routeDTO,
                success : function (response) {
                    if(response.success){
                        swal({
                            type: 'success',
                            title: 'Deleted',
                            text: 'Row is successfully deleted'
                        })
                        routeClear(true);
                    } else {
                        swal(
                            'Error',
                            'There is an issue in deleting',
                            'error'
                        )
                    }
                }
            })
            ;
            }
            }
        );
    } else  {
            swal({
                type: 'error',
                title: 'Error',
                text: 'Please Select a Row first'
            })
        }
}

/*function searchRoute(){
    table.ajax.reload();
}*/



