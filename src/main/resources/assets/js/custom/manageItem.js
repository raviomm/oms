/**
 * Created by RAVI KALUARACHCHI on 11/11/2019.
 */

$(document).ready(function () {
    table = $('#itemTable').DataTable({
        processing: true,
        ajax: {
            url: "/item/getAllItems",
            "type": "GET",
            "data": function () {
                var itemSearchCriteria = {};
                itemSearchCriteria.itemCode = $("#searchItemCode").val();
                return itemSearchCriteria;
            },
            dataSrc: function (json) {

                console.log(json.tableData);
                return json.tableData;
            }
        },
        columns: [
            {title: "Item Id", data: "itemId"},
            {title: "Item Name", data: "itemName"},
            {title: "Item Code", data: "itemCode"},
            {title: "Add Date", data: "itemAddDate"},
            {title: "Company", data: "itemCompany"},
            {title: "Horse Power", data: "itemHorsepower"},
            {title: "Retail Price", data: "retailPrice"},
            {title: "Sell Price", data: "sellPrice"},
            {title: "Status", data: "itemStatus"}
        ],
        columnDefs: [
            /*{
                targets: [8],
                visible: false,
                searchable: false
            }/*,
            {
                targets: [2],
                visible: false,
                searchable: false
            },
            {
                targets: [7],
                visible: false,
                searchable: false
            }*!/*/
        ]
    });






    // table click event
    $('#itemTable tbody').on('click', 'tr', function () {
        var data = table.row(this).data();
        fillFormData(data);
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
            clickedRow = false;
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            clickedRow = true;
        }
        disableUpdate(false);
        disableDelete(false);
    });



    $("#itemSave").click(function (e) {

        e.stopImmediatePropagation();
        e.preventDefault();
        const swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
        });
        swalWithBootstrapButtons({
            title: 'Confirmation',
            text: "Are you sure you want Save this Item?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Save',
            cancelButtonText: 'Cancel'
        }).then(function (result) {
            if (result.value) {
        var itemDTO = {};
        itemDTO.itemId = $("#itemId").val();
        itemDTO.itemName = $("#itemName").val();
        itemDTO.itemCode = $("#itemCode").val();
        itemDTO.itemAddDate = $("#itemAddDate").val();
        itemDTO.itemCompany = $("#itemCompany").val();
        itemDTO.itemHorsepower = $("#itemHorsepower").val();
        itemDTO.retailPrice = $("#retailPrice").val();
        itemDTO.sellPrice = $("#sellPrice").val();
        itemDTO.itemStatus = $("#itemStatus").val();



        $.ajax({
            url: '/item/saveItem', // or whatever
            dataType: 'json',
            type: 'post',
            data: itemDTO,
            success: function (response) {
                if (response.success) {
                    swal({
                        type: 'success',
                        title: 'Saved',
                        text: 'Item Saved Successfully '
                    })
                    itemClear(true);
                }else {
                    swal(
                        'Error',
                        'Error Saving Item  !!!',
                        'error'
                    )
                }
            }
        })
        ;
            }
            }
        );
    });


    $("#itemUpdateButton1").click(function (e) {
        e.stopImmediatePropagation();
        e.preventDefault();
        const swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
        });

        swalWithBootstrapButtons({
            title: 'Confirmation',
            text: "Are you sure you want Update it?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Update',
            cancelButtonText: 'Cancel'
        }).then(function (result) {
                if (result.value) {

                    var itemDTO = {};
                    itemDTO.itemId = $("#itemId").val();
                    itemDTO.itemName = $("#itemName").val();
                    itemDTO.itemCode = $("#itemCode").val();
                    itemDTO.itemAddDate = $("#itemAddDate").val();
                    itemDTO.sellPrice = $("#sellPrice").val();
                    itemDTO.itemCompany = $("#itemCompany").val();
                    itemDTO.itemHorsepower = $("#itemHorsepower").val();
                    itemDTO.itemStatus = $("#itemStatus").val();


                    $.ajax({
                        url: '/item/updateItem', // or whatever
                        dataType: 'json',
                        type: 'post',
                        data: itemDTO,
                        success: function (response) {
                            if (response.success) {
                                swal({
                                    type: 'success',
                                    title: 'Updated',
                                    text: 'Row is successfully updated'
                                })
                                itemClear(true);
                            } else {
                                swal(
                                    'Error',
                                    'There is an issue in updating',
                                    'error'
                                )
                            }
                        }
                    })
                    ;
                }
            }
        );
    });


    disableFormData(true);
    disableUpdate(true);
    disableDelete(true);
    $("#itemSave").show();
    $("#itemUpdateButton1").hide();

    $("#searchItem").click(function (e) {
        e.preventDefault();
        table.ajax.reload();
    });


});
function itemRefresh() {
    location.reload();
}

function itemAdd() {
    resetForm();
    disableFormData(false);
    disableUpdate(true);
    disableDelete(true);
    table.ajax.reload();
    /*$("#employeeSave").prop('disabled', false).removeClass( 'disabled' );*/
    $("#itemUpdateButton1").hide();

}

function resetForm() {
    $(':input', '#itemForm')
        .not(':button, :submit, :reset, :hidden')
        .val('')
        .removeAttr('checked')
        .removeAttr('selected');
}

function disableFormData(disable) {
    $("#itemName").prop('disabled', disable);
    $("#itemCode").prop('disabled', disable);
    $("#itemAddDate").prop('disabled', disable);
    $("#itemCompany").prop('disabled', disable);
    $("#itemHorsepower").prop('disabled', disable);
    $("#retailPrice").prop('disabled', disable);
    $("#sellPrice").prop('disabled', disable);
    $("#itemStatus").prop('disabled', disable);



}

function fillFormData(row) {
    $("#itemName").val(row.itemName);
    $("#itemCode").val(row.itemCode);
    $("#itemAddDate").val(row.itemAddDate.substring(0,10));
    $("#itemCompany").val(row.itemCompany);
    $("#itemHorsepower").val(row.itemHorsepower);
    $("#retailPrice").val(row.retailPrice);
    $("#sellPrice").val(row.sellPrice);
    $("#itemStatus").val(row.itemStatus);

}

function disableUpdate(disable) {
    $("#itemUpdateButton").prop('disabled', disable);

}

function disableDelete(disable) {
    $("#itemDeleteButton").prop('disabled', disable);

}


function itemClear(disable) {
    $("#itemDeleteButton").prop('disabled', disable);
    resetForm();
    disableFormData(true);
    disableUpdate(true);
    disableDelete(true);
    table.ajax.reload();
    $( "#itemUpdateButton1" ).hide();
    $( "#itemSave" ).show();
    $( "#itemSave" ).prop('disabled', true).addClass( 'disabled' );

}

function itemUpdate() {
    if (clickedRow) {
        disableFormData(true);
        $("#itemName").prop('disabled', false);
        /*$("#itemCode").prop('disabled', false);*/
        /*$("#itemAddDate").prop('disabled', false);*/
        $("#itemCompany").prop('disabled', false);
        $("#itemHorsepower").prop('disabled', false);
        /*$("#retailPrice").prop('disabled', false);*/
        $("#sellPrice").prop('disabled', false);
        $("#itemStatus").prop('disabled', false);
        $("#itemUpdateButton1").show();
        $("#itemSave").hide();

    } else {
        alert("Please Select a Row first !!!!");
    }
}


/*function itemDelete() {
    if (clickedRow) {
        const swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
        });

        swalWithBootstrapButtons({
            title: 'Confirmation',
            text: "Are you sure you want delete it?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Delete',
            cancelButtonText: 'Cancel'
        }).then(function (result) {
                if (result.value) {
                    var itemDTO = {};
                    itemDTO.itemId = $("#itemId").val();
                    itemDTO.itemName = $("#itemName").val();
                    itemDTO.itemCode = $("#itemCode").val();
                    itemDTO.itemAddDate = $("#itemAddDate").val();
                    itemDTO.itemCompany = $("#itemCompany").val();
                    itemDTO.itemHorsepower = $("#itemHorsepower").val();
                    itemDTO.retailPrice = $("#retailPrice").val();
                    itemDTO.sellPrice = $("#sellPrice").val();
                    itemDTO.itemStatus = $("#itemStatus").val();


                    $.ajax({
                        url: '/item/deleteItem', // or whatever
                        dataType: 'json',
                        type: 'post',
                        data: itemDTO,
                        success: function (response) {
                            if (response.success) {
                                swal({
                                    type: 'success',
                                    title: 'Deleted',
                                    text: 'Item is successfully deleted'
                                })
                                itemClear(true);
                            } else {
                                swal(
                                    'Error',
                                    'There is an issue in deleting',
                                    'error'
                                )
                            }
                        }
                    })
                    ;
                }
            }
        );
    } else {
        swal({
            type: 'error',
            title: 'Error',
            text: 'Please Select a Row first'
        })
    }

}*/



/*function searchEmployee(e) {
 e.preventDefault();
 table.ajax.reload();
 }*/
