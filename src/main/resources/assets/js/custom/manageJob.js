/**
 * Created by RAVI KALUARACHCHI on 11/11/2019.
 */
/**
 * Created by RAVI KALUARACHCHI on 12/12/2017.
 */
/**
 *Created by RAVI KALUARACHCHI on 12/8/2017.
 */

var clickedRow = false;
var table;
var table2;
var table3;
var addedInvMap = [];
var addedRent = [];
var addedServices = [];
var itemDropDownList = [];

$(document).ready(function() {



    //For Technician dropdown
    var jobTechnician = $("#jobTechnician");

    $.ajax({
        url: "/job/getAllAvailableTechnicians",
        dataType: 'json',
        type: 'get',
        success: function(response) {
            $.each(response.data, function() {
                jobTechnician.append($("<option />").val(this.employeeId).text(this.employeeFirstName));
            });
        }
    });


// table for item object
    table = $('#jobItemTable').DataTable({
        columns: [
            {title: "Item Name", data: 'selectItem'},
            {title: "Quantity", data: 'itemCount'},
            {title: "Remove"}
        ],
        columnDefs: [

            {
                targets: [2],
                data: null,
                render : function (data, type, row) {
                    return '<button id="' + row.id +'" class="btn btn-warning btn-fill btn-sm" type="button" onclick="itemsRemove(this)">Remove</button>'
                }

            }
        ],
        fnCreatedRow: function (nRow, aData, iDataIndex) {
            $(nRow).attr('data-id', aData.itemId);
        }
    });

    // item populating to drop down
    var selectItem = $("#selectItem");

    $.ajax({
        url: "/item/getAllItemsForDropdowns",
        dataType: 'json',
        type: 'get',
        success: function(response) {
            itemDropDownList = response.data;
            $.each(response.data, function() {
                selectItem.append($("<option />").val(this.itemId).text(this.itemName));
            });
        }
    });


    // for add button item
    $("#itemAdd").click(function () {
        const selectItem = $('#selectItem :selected').text();
        const itemCount = $('#itemCount').val();
        const itemAvailableCount = $('#itemAvailableCount').text();
        if(parseInt(itemAvailableCount) < parseInt(itemCount)){
            alert("Item count should be below than available count !!!")
            return false;
        }



        if (selectItem || itemCount !== undefined) {
            const index = addedInvMap.map(
                function (object, index) {

                    return object.selectItem;

                }).indexOf(selectItem);

            if (index >= 0) {
                alert("Can't add same item twice");
                document.getElementById("itemAdd").disabled = true;
            }
            else {
                const updatedItem = {selectItem: selectItem,itemCount: itemCount };
                addedInvMap.push(updatedItem);
                table.row.add(updatedItem).draw(false);
            }

        }
        return false;
    });

// item object
    $('#itemAdd tbody').on( 'click', 'button.btn-warning', function () {

        var selectItem = $(this).closest('tr').find('td:nth-child(3)').html();

        const index = addedInvMap.map(
            function (object, index) {

                return object.selectItem;

            }).indexOf(selectItem);

        if (index >= 0) {
            addedInvMap.splice(index, 1);
        }
        table
            .row( $(this).parents('tr') )
            .remove()
            .draw();
    });

    $('#selectItem').on('change', function() {
        var selectItem = $('#selectItem :selected').text();
        $.each(itemDropDownList, function() {
               if(selectItem == this.itemName){
                    $("#itemAvailableCount").text(this.count);
               }
        });

        if(this.value){
            document.getElementById("itemAdd").disabled = false;
            return;
        }
        document.getElementById("itemAdd").disabled = true;
    });






    // table for Repair object
    table2 = $('#jobRepairTable').DataTable({
        columns: [
            {title: "Repair Name", data: 'selectRepair'},
            {title: "Remove"}
        ],
        columnDefs: [

            {
                targets: [1],
                data: null,
                render : function (data, type, row) {
                    return '<button id="' + row.id +'" class="btn btn-warning btn-fill btn-sm" type="button" onclick="repairsRemove(this)">Remove</button>'
                }

            }
        ],
        fnCreatedRow: function (nRow, aData, iDataIndex) {
            $(nRow).attr('data-id', aData.repairId);
        }
    });

    // repair populating to drop down
    var selectRepair = $("#selectRepair");

    $.ajax({
        url: "/repair/getAllRepairsForDropdowns",
        dataType: 'json',
        type: 'get',
        success: function(response) {
            $.each(response.data, function() {
                selectRepair.append($("<option />").val(this.repairId).text(this.repairName));
            });
        }
    });

    // for add button repair
    $("#repairAdd").click(function () {
        const selectRepair = $('#selectRepair :selected').text();


        if (selectRepair !== undefined) {
            const index = addedServices.map(
                function (object, index) {

                    return object.selectRepair;

                }).indexOf(selectRepair);

            if (index >= 0) {
                alert("Can't add same repair twice");
                document.getElementById("repairAdd").disabled = true;
            }
            else {
                const updatedItem = {selectRepair: selectRepair };
                addedServices.push(updatedItem);
                table2.row.add(updatedItem).draw(false);
            }

        }
        return false;
    });

    // repair object
    $('#repairAdd tbody').on( 'click', 'button.btn-warning', function () {

        var selectRepair = $(this).closest('tr').find('td:nth-child(3)').html();

        const index = addedServices.map(
            function (object, index) {

                return object.selectRepair;

            }).indexOf(selectRepair);

        if (index >= 0) {
            addedServices.splice(index, 1);
        }
        table2
            .row( $(this).parents('tr') )
            .remove()
            .draw();
    });

    $('#selectRepair').on('change', function() {
        if(this.value){
            document.getElementById("repairAdd").disabled = false;
            return;
        }
        document.getElementById("repairAdd").disabled = true;
    });

// table for rent object
    table3 = $('#jobRentTable').DataTable({
        columns: [
            {title: "Rent Code", data: 'selectRent'},
            {title: "Remove"}
        ],
        columnDefs: [

            {
                targets: [1],
                data: null,
                render : function (data, type, row) {
                    return '<button id="' + row.id +'" class="btn btn-warning btn-fill btn-sm" type="button" onclick="rentsRemove(this)">Remove</button>'
                }

            }
        ],
        fnCreatedRow: function (nRow, aData, iDataIndex) {
            $(nRow).attr('data-id', aData.rentId);
        }
    });

// rent populating to drop down
    var selectRent = $("#selectRent");

    $.ajax({
        url: "/rent/getAllRentForDropdowns",
        dataType: 'json',
        type: 'get',
        success: function(response) {
            $.each(response.data, function() {
                selectRent.append($("<option />").val(this.rentId).text(this.rentCode));
            });
        }
    });

    // for add button rent
    $("#rentAdd").click(function () {
        const selectRent = $('#selectRent :selected').text();


        if (selectRent !== undefined) {
            const index = addedRent.map(
                function (object, index) {

                    return object.selectRent;

                }).indexOf(selectRent);

            if (index >= 0) {
                alert("Can't add same rent twice");
                document.getElementById("rentAdd").disabled = true;
            }
            else {
                const updatedItem = {selectRent: selectRent };
                addedRent.push(updatedItem);
                table3.row.add(updatedItem).draw(false);
            }

        }
        return false;
    });

// repair object
    $('#rentAdd tbody').on( 'click', 'button.btn-warning', function () {

        var selectRent = $(this).closest('tr').find('td:nth-child(3)').html();

        const index = addedRent.map(
            function (object, index) {

                return object.selectRent;

            }).indexOf(selectRent);

        if (index >= 0) {
            addedRent.splice(index, 1);
        }
        table3
            .row( $(this).parents('tr') )
            .remove()
            .draw();
    });

    $('#selectRent').on('change', function() {
        if(this.value){
            document.getElementById("rentAdd").disabled = false;
            return;
        }
        document.getElementById("rentAdd").disabled = true;
    });


    //Next Button
    $("#nextJob").click(function (e) {
        location.reload();

    })

    //Save Job
    $("#saveJob").click(function (e) {

        e.stopImmediatePropagation();
        e.preventDefault();
        const swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
        });
        swalWithBootstrapButtons({
            title: 'Confirmation',
            text: "Are you sure you want Save this Job?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Save',
            cancelButtonText: 'Cancel'
        }).then(function (result) {
            if (result.value) {
        var date = $("#jobDate").val();
              var jobTechnician = $("#jobTechnician").val();
              var customerNic = $("#customerNic").val();
              var customerLastName = $("#customerLastName").val();
              var customerContactNumber = $("#customerContactNumber").val();

              console.log(addedInvMap);
              console.log(addedRent);
              console.log(addedServices);
              var data = {
                  "jobDate" : date,
                  "jobTechnician" : jobTechnician,
                  "customerNic" : customerNic,
                  "customerLastName" : customerLastName,
                  "customerContactNumber" : customerContactNumber,
                  "itemList" : addedInvMap,
                  "addedRent" : addedRent,
                  "addedServices" : addedServices
              };

              $.ajax({
                      url: "/job/saveJob",
                      dataType: 'json',
                      contentType: "application/json; charset=utf-8",
                      data :  JSON.stringify(data),
                      type: 'post',
                      success: function(response) {
                         console.log(response);
                         if(response !== undefined){
                              if(response.success){
                                  swal({
                                      type: 'success',
                                      title: 'Saved',
                                      text: 'Job successfully Saved'
                                  });
                                  /*alert("Successfully Saved Job, New Job Id:" + response.data);*/
                                  jobClear(true);
                                  $("#jobItemTable tr").remove();
                                  $("#jobRepairTable tr").remove();
                                  $("#jobRentTable tr").remove();


                                  /*table.clear();
                                  table2.clear();
                                  table3.clear();*/
                                  $('#jobTechnician option:eq(0)').attr('selected','selected');
                                  $("#jobDate").val("");
                                  $("#customerNic").val("");
                                  $("#customerLastName").val("");
                                  $("#customerContactNumber").val("");

                              } else {
                                  /*alert("Savings Job Failed!!!");*/
                                  swal(
                                      'Error',
                                      'Error Saving Job  !!!',
                                      'error'
                                  )
                              }
                         } else {
                             /* alert("Savings Job Failed!!!");*/
                             swal(
                                 'Error',
                                 'Error Saving Job  !!!',
                                 'error'
                             )
                         }
                      }
                  });
                  return false;
            }
            }
        );

        $("#nextJob").show();

    });



       $("#nextJob").hide();

//
//    $("#searchFleet").click(function (e) {
//        e.preventDefault();
//        table.ajax.reload();
//    });

});

//function for remove button on item table column
function itemsRemove(object) {
    var selectItem = $(object).closest('tr').find('td:nth-child(1)').html();
    var itemCount = $(object).closest('tr').find('td:nth-child(2)').html();

    $("#selectItem").val(selectItem);
    $("#itemCount").val(itemCount);


    const index = addedInvMap.map(
        function (object, index) {

            return object.selectItem;

        }).indexOf(selectItem);
    if (index >= 0) {
        addedInvMap.splice(index, 1);
    }
    table
        .row( $(object).parents('tr') )
        .remove()
        .draw();

}

//function for remove button on repair table column
function repairsRemove(object) {
    var selectRepair = $(object).closest('tr').find('td:nth-child(1)').html();


    $("#selectRepair").val(selectRepair);



    const index = addedServices.map(
        function (object, index) {

            return object.selectRepair;

        }).indexOf(selectRepair);
    if (index >= 0) {
        addedServices.splice(index, 1);
    }
    table2
        .row( $(object).parents('tr') )
        .remove()
        .draw();

}

//function for remove button on repair table column
function rentsRemove(object) {
    var selectRent = $(object).closest('tr').find('td:nth-child(1)').html();


    $("#selectRent").val(selectRent);



    const index = addedRent.map(
        function (object, index) {

            return object.selectRent;

        }).indexOf(selectRent);
    if (index >= 0) {
        addedRent.splice(index, 1);
    }
    table3
        .row( $(object).parents('tr') )
        .remove()
        .draw();

}


function resetForm() {
    $(':input', '#jobForm')
        .not(':button, :submit, :reset, :hidden')
        .val('')
        .removeAttr('checked')
        .removeAttr('selected');
    $("#saveJob").hide();
}

function jobClear(disable) {
    resetForm();
    /*$("#jobSave").hide();
    $("#jobNext").show();*/
    /*$( "#JobSave" ).prop('disabled', true).addClass( 'disabled' );*/


}

/*function nextJob() {

    location.reload();
}*/

function disableFormData(disable) {
    $("#jobDate").prop('disabled', disable);
    $("#jobTechnician").prop('disabled', disable);
    $("#customerNic").prop('disabled', disable);
    $("#customerLastName").prop('disabled', disable);
    $("#customerContactNumber").prop('disabled', disable);
    $("#selectItem").prop('disabled', disable);
    $("#itemCount").prop('disabled', disable);
    $("#itemAdd").prop('disabled', disable);
    $("#jobItemTable").prop('disabled', disable);
    $("#selectRepair").prop('disabled', disable);
    $("#repairAdd").prop('disabled', disable);
    $("#selectRent").prop('disabled', disable);
    $("#rentAdd").prop('disabled', disable);
    $("#jobRentTable").prop('disabled', disable);
    $("#jobRepairTable").prop('disabled', disable);


}

