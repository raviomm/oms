/*
var table;
/!**
 * Created by RAVI KALUARACHCHI on 12/12/2017.
 *!/
var clickedRow = false;

$(document).ready(function() {

    table = $('#homeTable').DataTable({
        processing: true,
        ajax: {
            url: "/home/getAllHometable",
            "type": "POST",
            "data": function() {
                var fleetSearchCriteria = {};
                fleetSearchCriteria.fleetName = $("#searchFleetName").val();
                return fleetSearchCriteria;

            },
            dataSrc: function(json) {

                return json.tableData;
            }
        },
        columns: [
            {
                title: "fleet Id",
                data: "fleetId"
            },
            {
                title: "Fleet Name",
                data: "fleetName"
            },
            {
                title: "Route Used",
                data: "selectRoute"
            },
            {
                title: "Product Used",
                data: "selectProduct"
            },
            {
                title: "Vehicle Used",
                data: "selectVehicle"
            },
            {
                title: "Driver Assigned",
                data: "selectDriver"
            },
            {
                title: "Helper Assigned",
                data: "selectHelper"
            },
            {
                title: "Start Time",
                data: "fleetStartTime"
            },
            {
                title: "Petty Cash",
                data: "fleetPettyCash"
            },
            {
                title: "Petty Description",
                data: "fleetPettyCashDescription"
            },
            {
                title: "Due Date",
                data: "fleetDueDate"
            },
            {
                title: "Fleet Status",
                data: "fleetStatus"

            },
            {
                title: "ACT or INA"


            }

        ],
        columnDefs: [{
            targets: [2],
            visible: false,
            searchable: false
        },
            {
                targets: [8],
                visible: false,
                searchable: false
            },
            {
                targets: [11],
                visible: true,
                searchable: false
            },
            {
                targets: [12],
                visible: true,
                searchable: false,
                render: function ( data, type, row, meta ) {
                    var button;
                    var fleetId = row.fleetId;
                    switch(row.fleetStatus){
                        case 'ACT':
                            button = '<button id="buttonInactivate" onclick="statusComplete('+fleetId+')">Complete</button>&nbsp<button id="buttonInactivate" onclick="statusCancel('+fleetId+')">Cancel</button>';
                            break;
                        case 'INA':
                            button = '<div align="center"> <button id="buttonActivate" onclick="changeStatus('+fleetId+')">Activate</button></div>';
                            break;
                    }
                    return button;
                }
            }
        ],

        //Table Row Color code
        "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
            switch(aData.fleetStatus){
                case 'ACT':
                    $(nRow).css('color', 'green');
                    break;
                case 'INA':
                    $(nRow).css('color', 'red');
                    break;

            }
        }

    });
// New code added for  button




    // New code added for  button ends here


    // table click event
    $('#homeTable tbody').on('click', 'tr', function() {
        var data = table.row(this).data();

        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
            clickedRow = false;
        } else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            clickedRow = true;
        }


    });




});


function changeStatus(fleetId){

    var fleetDTO = {};
    fleetDTO.fleetId = fleetId;

    $.ajax({
        url: "/home/changeStatus", // or whatever
        dataType: 'json',
        type: 'post',
        data: fleetDTO,
        success: function(response) {
            if (response.success) {
                alert("Successfully Update the Fleet ");
                table.ajax.reload();
            } else {
                alert("Error Updating Fleet  !!!");
            }

        },
        error : function (error) {
            console.log(error);
        }
    });





}

function statusComplete(fleetId){

    var fleetDTO = {};
    fleetDTO.fleetId = fleetId;

    $.ajax({
        url: "/home/statusComplete", // or whatever
        dataType: 'json',
        type: 'post',
        data: fleetDTO,
        success: function(response) {
            if (response.success) {
                alert("Successfully Update the Fleet ");
                table.ajax.reload();
            } else {
                alert("Error Updating Fleet  !!!");
            }

        },
        error : function (error) {
            console.log(error);
        }
    });


}



function statusCancel(fleetId){

    var fleetDTO = {};
    fleetDTO.fleetId = fleetId;

    $.ajax({
        url: "/home/statusCancel", // or whatever
        dataType: 'json',
        type: 'post',
        data: fleetDTO,
        success: function(response) {
            if (response.success) {
                alert("Successfully Update the Fleet ");
                table.ajax.reload();
            } else {
                alert("Error Updating Fleet  !!!");
            }

        },
        error : function (error) {
            console.log(error);
        }
    });





}


*/
