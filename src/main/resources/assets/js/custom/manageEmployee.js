
$(document).ready(function () {

    table = $('#employeeTable').DataTable({
        processing: true,
        ajax: {
            url: "/employee/getAllEmployees",
            "type": "POST",
            "data": function () {
                var employeeSearchCriteria = {};
                employeeSearchCriteria.employeeNIC = $("#serchEmployeeNIC").val();
                return employeeSearchCriteria;
            },
            dataSrc: function (json) {

                console.log(json.tableData);
                return json.tableData;
            }
        },
        columns: [
            {title: "Employee Id", data: "employeeId"},
            {title: "First Name", data: "employeeFirstName"},
            {title: "Last Name", data: "employeeLastName"},
            {title: "NIC", data: "employeeNIC"},
            {title: "Employee EPF No", data: "employeeEPF"},
            {title: "Address", data: "employeeAddress"},
            {title: "Mobile Number", data: "employeeMobileNumber"},
            {title: "Email", data: "employeeEmail"},
            {title: "Basic Salary", data: "employeeBasicSalary"},
            {title: "Allowance", data: "employeeAllowance"},
            {title: "Designation", data: "employeeDesignation"},
            {title: "Status", data: "employeeStatus"}
        ],
        columnDefs: [
            {
                targets: [0],
                visible: false,
                searchable: false
            },
            {
                targets: [2],
                visible: false,
                searchable: false
            },
            {
                targets: [7],
                visible: false,
                searchable: false
            }
        ]
    });

    // table click event
    $('#employeeTable tbody').on('click', 'tr', function () {
        var data = table.row(this).data();
        fillFormData(data);
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
            clickedRow = false;
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            clickedRow = true;
        }
        disableUpdate(false);
        disableDelete(false);
    });




    $("#employeeSave").click(function (e) {

        e.stopImmediatePropagation();
        e.preventDefault();
        const swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
        });
        swalWithBootstrapButtons({
            title: 'Confirmation',
            text: "Are you sure you want Save this Employee?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Save',
            cancelButtonText: 'Cancel'
        }).then(function (result) {
            if (result.value) {

        var employeeDTO = {};
        employeeDTO.employeeId = $("#employeeId").val();
        employeeDTO.employeeFirstName = $("#employeeFirstName").val();
        employeeDTO.employeeLastName = $("#employeeLastName").val();
        employeeDTO.employeeNIC = $("#employeeNIC").val();
        employeeDTO.employeeEPF = $("#employeeEPF").val();
        employeeDTO.employeeAddress = $("#employeeAddress").val();
        employeeDTO.employeeMobileNumber = $("#employeeMobileNumber").val();
        employeeDTO.employeeEmail = $("#employeeEmail").val();
        employeeDTO.employeeBasicSalary = $("#employeeBasicSalary").val();
        employeeDTO.employeeAllowance = $("#employeeAllowance").val();
        employeeDTO.employeeDesignation = $("#employeeDesignation").val();
        employeeDTO.employeeStatus = $("#employeeStatus").val();

        $.ajax({
            url: '/employee/saveEmployees', // or whatever
            dataType: 'json',
            type: 'post',
            data: employeeDTO,
            success: function (response) {
                if (response.success) {
                    swal({
                        type: 'success',
                        title: 'Saved',
                        text: 'Employee is successfully Saved'
                    });
                    employeeClear(true);

                } else {
                    swal(
                        'Error',
                        'Error Saving Employee  !!!',
                        'error'
                    )

                }
            }
        })
        ;
            }
            }
        );
        /*document.getElementById('employeeForm').reset();*/
    });


    $("#employeeUpdateButton1").click(function (e) {
        e.stopImmediatePropagation();
        e.preventDefault();
        const swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
        });

        swalWithBootstrapButtons({
            title: 'Confirmation',
            text: "Are you sure you want Update it?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Update',
            cancelButtonText: 'Cancel'
        }).then(function (result) {
            if (result.value) {

                var employeeDTO = {};
                employeeDTO.employeeId = $("#employeeId").val();
                employeeDTO.employeeFirstName = $("#employeeFirstName").val();
                employeeDTO.employeeLastName = $("#employeeLastName").val();
                employeeDTO.employeeNIC = $("#employeeNIC").val();
                employeeDTO.employeeEPF = $("#employeeEPF").val();
                employeeDTO.employeeAddress = $("#employeeAddress").val();
                employeeDTO.employeeMobileNumber = $("#employeeMobileNumber").val();
                employeeDTO.employeeEmail = $("#employeeEmail").val();
                employeeDTO.employeeBasicSalary = $("#employeeBasicSalary").val();
                employeeDTO.employeeAllowance = $("#employeeAllowance").val();
                employeeDTO.employeeDesignation = $("#employeeDesignation").val();
                employeeDTO.employeeStatus = $("#employeeStatus").val();

                $.ajax({
                    url: '/employee/updateEmployee',
                    dataType: 'json',
                    type: 'post',
                    data: employeeDTO,
                    success: function (response) {
                        if (response.success) {
                            swal({
                                type: 'success',
                                title: 'Updated',
                                text: 'Row is successfully updated'
                            })
                            employeeClear(true);
                            
                        } else {
                            swal(
                                'Error',
                                'There is an issue in updating',
                                'error'
                            )
                        }
                    }
                })
                ;
            }
        }
    );

    });

    disableFormData(true);
    disableUpdate(true);
    disableDelete(true);
    $("#employeeSave").show();
    $("#employeeUpdateButton1").hide();

    $("#searchEmployee").click(function (e) {
        e.preventDefault();
        table.ajax.reload();
    });


});

function employeeRefresh() {
   location.reload();
}

function employeeAdd() {

    resetForm();
    disableFormData(false);
    disableUpdate(true);
    disableDelete(true);
    table.ajax.reload();

    /*$("#employeeSave").prop('disabled', false).removeClass( 'disabled' );*/
    $("#employeeUpdateButton1").hide();


}

function employeeClear(disable) {
    $("#employeeDeleteButton").prop('disabled', disable);
    resetForm();
    disableFormData(true);
    disableUpdate(true);
    disableDelete(true);
    table.ajax.reload();
    $( "#employeeUpdateButton1" ).hide();
    $( "#employeeSave" ).show();
    $( "#employeeSave" ).prop('disabled', true).addClass( 'disabled' );

}


function resetForm() {
    $(':input', '#employeeForm')
        .not(':button, :submit, :reset, :hidden')
        .val('')
        .removeAttr('checked')
        .removeAttr('selected');
}

function disableFormData(disable) {
    $("#employeeId").prop('disabled', disable);
    $("#employeeFirstName").prop('disabled', disable);
    $("#employeeLastName").prop('disabled', disable);
    $("#employeeNIC").prop('disabled', disable);
    $("#employeeEPF").prop('disabled', disable);
    $("#employeeAddress").prop('disabled', disable);
    $("#employeeMobileNumber").prop('disabled', disable);
    $("#employeeEmail").prop('disabled', disable);
    $("#employeeBasicSalary").prop('disabled', disable);
    $("#employeeAllowance").prop('disabled', disable);
    $("#employeeDesignation").prop('disabled', disable);
    $("#employeeStatus").prop('disabled', disable);


}


function fillFormData(row) {
    $("#employeeId").val(row.employeeId);
    $("#employeeFirstName").val(row.employeeFirstName);
    $("#employeeLastName").val(row.employeeLastName);
    $("#employeeNIC").val(row.employeeNIC);
    $("#employeeEPF").val(row.employeeEPF);
    $("#employeeAddress").val(row.employeeAddress);
    $("#employeeMobileNumber").val(row.employeeMobileNumber);
    $("#employeeEmail").val(row.employeeEmail);
    $("#employeeBasicSalary").val(row.employeeBasicSalary);
    $("#employeeAllowance").val(row.employeeAllowance);
    $("#employeeDesignation").val(row.employeeDesignation);
    $("#employeeStatus").val(row.employeeStatus);


}


function disableUpdate(disable) {
    $("#employeeUpdateButton").prop('disabled', disable);

}

function disableDelete(disable) {
    $("#employeeDeleteButton").prop('disabled', disable);

}





// Edit Button Click Function
function employeeUpdate() {
    if (clickedRow) {
        disableFormData(true);
        $("#employeeLastName").prop('disabled', false);
        $("#employeeAddress").prop('disabled', false);
        $("#employeeMobileNumber").prop('disabled', false);
        $("#employeeEmail").prop('disabled', false);
        $("#employeeBasicSalary").prop('disabled', false);
        $("#employeeAllowance").prop('disabled', false);
        $("#employeeDesignation").prop('disabled', false);
        $("#employeeStatus").prop('disabled', false);
        $("#employeeUpdateButton1").prop('disabled',false).removeClass('disabled');
        $("#employeeUpdateButton1").show();
        $("#employeeSave").hide();

    } else {
        alert("Please Select a Row first !!!!");
    }
}



function employeeDelete() {
    if (clickedRow) {
        const swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
        });

        swalWithBootstrapButtons({
            title: 'Confirmation',
            text: "Are you sure you want delete it?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Delete',
            cancelButtonText: 'Cancel'
        }).then(function (result) {
                if (result.value) {
                    var employeeDTO = {};
                    employeeDTO.employeeId = $("#employeeId").val();
                    employeeDTO.employeeFirstName = $("#employeeFirstName").val();
                    employeeDTO.employeeLastName = $("#employeeLastName").val();
                    employeeDTO.employeeNIC = $("#employeeNIC").val();
                    employeeDTO.employeeEPF = $("#employeeEPF").val();
                    employeeDTO.employeeAddress = $("#employeeAddress").val();
                    employeeDTO.employeeMobileNumber = $("#employeeMobileNumber").val();
                    employeeDTO.employeeEmail = $("#employeeEmail").val();
                    employeeDTO.employeeBasicSalary = $("#employeeBasicSalary").val();
                    employeeDTO.employeeAllowance = $("#employeeAllowance").val();
                    employeeDTO.employeeDesignation = $("#employeeDesignation").val();
                    employeeDTO.employeeStatus = $("#employeeStatus").val();

                    $.ajax({
                        url: '/employee/deleteEmployee', // or whatever
                        dataType: 'json',
                        type: 'post',
                        data: employeeDTO,
                        success: function (response) {
                            if (response.success) {
                                swal({
                                    type: 'success',
                                    title: 'Deleted',
                                    text: 'Row is successfully deleted'
                                })
                                employeeClear(true);
                            } else {
                                swal(
                                    'Error',
                                    'There is an issue in deleting',
                                    'error'
                                )
                            }
                        }
                    })
                    ;
                }
            }
        );
    } else {
        swal({
            type: 'error',
            title: 'Error',
            text: 'Please Select a Row first'
        })
    }

}



/*function searchEmployee(e) {
    e.preventDefault();
    table.ajax.reload();
}*/
