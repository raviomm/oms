/**
 * Created by RAVI KALUARACHCHI on 11/11/2019.
 */
$(document).ready(function () {
    table = $('#rentTable').DataTable({
        processing: true,
        ajax: {
            url: "/rent/getAllRents",
            "type": "POST",
            "data": function () {
                var itemSearchCriteria = {};
                itemSearchCriteria.rentCode = $("#searchRentByCode").val();
                return itemSearchCriteria;
            },
            dataSrc: function (json) {

                console.log(json.tableData);
                return json.tableData;
            }
        },
        columns: [
            {title: "Rent Id", data: "rentId"},
            {title: "Rent Code", data: "rentCode"},
            {title: "Resource", data: "rentResource"},
            {title: "Charge", data: "rentCharge"},
            {title: "Description", data: "rentDescription"}
        ],
        columnDefs: [
            /*{
             targets: [8],
             visible: false,
             searchable: false
             }/*,
             {
             targets: [2],
             visible: false,
             searchable: false
             },
             {
             targets: [7],
             visible: false,
             searchable: false
             }*!/*/
        ]
    });


    // table click event
    $('#rentTable tbody').on('click', 'tr', function () {
        var data = table.row(this).data();
        fillFormData(data);
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
            clickedRow = false;
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            clickedRow = true;
        }
        disableUpdate(false);
        disableDelete(false);
    });



    $("#rentSave").click(function (e) {

        e.stopImmediatePropagation();
        e.preventDefault();
        const swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
        });
        swalWithBootstrapButtons({
            title: 'Confirmation',
            text: "Are you sure you want Save this Rent?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Save',
            cancelButtonText: 'Cancel'
        }).then(function (result) {
            if (result.value) {
        var rentDTO = {};
        rentDTO.rentId = $("#rentId").val();
        rentDTO.rentCode = $("#rentCode").val();
        rentDTO.rentResource = $("#rentResource").val();
        rentDTO.rentCharge = $("#rentCharge").val();
        rentDTO.rentDescription = $("#rentDescription").val();



        $.ajax({
            url: '/rent/saveRent', // or whatever
            dataType: 'json',
            type: 'post',
            data: rentDTO,
            success: function (response) {
                if (response.success) {swal({
                    type: 'success',
                    title: 'Saved',
                    text: 'Rent Saved Successfully '
                })
                    rentClear();
                } else {
                    swal(
                    'Error',
                    'Error Saving Rent  !!!',
                    'error'
                )
                }
            }
        })
        ;
            }
            }
        );
    });


    $("#rentUpdateButton1").click(function (e) {
        e.stopImmediatePropagation();
        e.preventDefault();
        const swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
        });

        swalWithBootstrapButtons({
            title: 'Confirmation',
            text: "Are you sure you want Update it?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Update',
            cancelButtonText: 'Cancel'
        }).then(function (result) {
                if (result.value) {

                    var rentDTO = {};
                    rentDTO.rentId = $("#rentId").val();
                    rentDTO.rentCode = $("#rentCode").val();
                    rentDTO.rentResource = $("#rentResource").val();
                    rentDTO.rentCharge = $("#rentCharge").val();
                    rentDTO.rentDescription = $("#rentDescription").val();


                    $.ajax({
                        url: '/rent/updateRent', // or whatever
                        dataType: 'json',
                        type: 'post',
                        data: rentDTO,
                        success: function (response) {
                            if (response.success) {
                                swal({
                                    type: 'success',
                                    title: 'Updated',
                                    text: ' Rent successfully updated'
                                })
                                rentClear(true);
                            } else {
                                swal(
                                    'Error',
                                    'There is an issue in updating',
                                    'error'
                                )
                            }
                        }
                    })
                    ;
                }
            }
        );
    });


    disableFormData(true);
    disableUpdate(true);
    disableDelete(true);
    $("#rentSave").show();
    $("#rentUpdateButton1").hide();

    $("#searchRent").click(function (e) {
        e.preventDefault();
        table.ajax.reload();
    });


});


function rentRefresh() {
    location.reload();
}


function rentAdd() {
    resetForm();
    disableFormData(false);
    disableUpdate(true);
    /*disableDelete(true);*/
    table.ajax.reload();
    /*$("#rentSave").prop('disabled', false).removeClass( 'disabled' );*/
    $("#rentUpdateButton1").hide();


}

function rentClear(disable) {
    $("#rentDeleteButton").prop('disabled', disable);
    resetForm();
    disableFormData(true);
    disableUpdate(true);
    disableDelete(true);
    table.ajax.reload();
    $( "#rentUpdateButton1" ).hide();
    $( "#rentSave" ).show();
    $( "#rentSave" ).prop('disabled', true).addClass( 'disabled' );

}


function resetForm() {
    $(':input', '#rentForm')
        .not(':button, :submit, :reset, :hidden')
        .val('')
        .removeAttr('checked')
        .removeAttr('selected');
}

function disableFormData(disable) {
    $("#rentId").prop('disabled', disable);
    $("#rentCode").prop('disabled', disable);
    $("#rentResource").prop('disabled', disable);
    $("#rentCharge").prop('disabled', disable);
    $("#rentDescription").prop('disabled', disable);



}

function fillFormData(row) {
    $("#rentId").val(row.rentId);
    $("#rentCode").val(row.rentCode);
    $("#rentResource").val(row.rentResource);
    $("#rentCharge").val(row.rentCharge);
    $("#rentDescription").val(row.rentDescription);

}


function disableUpdate(disable) {
    $("#rentUpdateButton").prop('disabled', disable);

}

function disableDelete(disable) {
    $("#rentDeleteButton").prop('disabled', disable);

}





// Edit Button Click Function
function rentUpdate() {
    if (clickedRow) {
        disableFormData(true);
        $("#rentResource").prop('disabled', false);
        $("#rentCharge").prop('disabled', false);
        $("#rentDescription").prop('disabled', false);
        $("#rentUpdateButton1").prop('disabled',false).removeClass('disabled');
        $("#rentUpdateButton1").show();
        $("#rentSave").hide();

    } else {
        alert("Please Select a Row first !!!!");
    }
}



/*function rentDelete() {
    if (clickedRow) {
        const swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
        });

        swalWithBootstrapButtons({
            title: 'Confirmation',
            text: "Are you sure you want delete it?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Delete',
            cancelButtonText: 'Cancel'
        }).then(function (result) {
                if (result.value) {
                    var rentDTO = {};
                    rentDTO.rentId = $("#rentId").val();
                    rentDTO.rentCode = $("#rentCode").val();
                    rentDTO.rentResource = $("#rentResource").val();
                    rentDTO.rentCharge = $("#rentCharge").val();
                    rentDTO.rentDescription = $("#rentDescription").val();


                    $.ajax({
                        url: '/rent/deleteRent', // or whatever
                        dataType: 'json',
                        type: 'post',
                        data: rentDTO,
                        success: function (response) {
                            if (response.success) {
                                swal({
                                    type: 'success',
                                    title: 'Deleted',
                                    text: 'Row is successfully deleted'
                                })
                                rentClear(true);
                            } else {
                                swal(
                                    'Error',
                                    'There is an issue in deleting',
                                    'error'
                                )
                            }
                        }
                    })
                    ;
                }
            }
        );
    } else {
        swal({
            type: 'error',
            title: 'Error',
            text: 'Please Select a Row first'
        })
    }

}*/
/*function rentDelete() {
    if (clickedRow) {
        const swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
        });

        swalWithBootstrapButtons({
            title: 'Confirmation',
            text: "Are you sure you want delete it?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Delete',
            cancelButtonText: 'Cancel'
        }).then(function (result) {
                if (result.value) {
                    var rentDTO = {};
                    rentDTO.rentId = $("#rentId").val();
                    rentDTO.rentCode = $("#rentCode").val();
                    rentDTO.rentResource = $("#rentResource").val();
                    rentDTO.rentCharge = $("#rentCharge").val();
                    rentDTO.rentDescription = $("#rentDescription").val();


                    $.ajax({
                        url: '/rent/deleteRent', // or whatever
                        dataType: 'json',
                        type: 'post',
                        data: rentDTO,
                        success: function (response) {
                            if (response.success) {
                                swal({
                                    type: 'success',
                                    title: 'Deleted',
                                    text: 'Item is successfully deleted'
                                })
                                rentClear(true);
                            } else {
                                swal(
                                    'Error',
                                    'There is an issue in deleting',
                                    'error'
                                )
                            }
                        }
                    })
                    ;
                }
            }
        );
    } else {
        swal({
            type: 'error',
            title: 'Error',
            text: 'Please Select a Row first'
        })
    }

}*/

