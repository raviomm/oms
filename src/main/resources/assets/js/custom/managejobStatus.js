/**
 * Created by RAVI KALUARACHCHI on 12/12/2019.
 */
var table;

var clickedRow = false;

$(document).ready(function() {

    table = $('#jobStatusTable').DataTable({
        processing: true,
        ajax: {
            url: "/job/getAllStartedJobs",
            "type": "GET",
            "data": function() {
                var jobSearchCriteria = {};
                return jobSearchCriteria;

            },
            dataSrc: function(json) {
                /*console.log(json.data);

                return json.data;*/
                console.log(json.tableData);

                return json.tableData;

            }
        },
        columns: [
            {
                title: "Job Id",
                data: "id"
            },
            {
                title: "Date",
                data: "jobDate"
            },
            {
                title: "Technician",
                data: "jobTechnician"
            },
            {
                title: "Customer Name",
                data: "customerLastName"
            },
            {
                title: "Customer Contact",
                data: "customerContactNumber"
            },
            {
                title: "Status",
                data: "status"

            },
            {
                title: "Start or Complete"


            },
            {
                title: "Job Slip"


            }

        ],
        columnDefs: [

            {

                targets: [6],
                data: null,
                render : function (data, type, row) {
                    return '<button id="' + row.id +'" class="btn btn-success btn-fill btn-sm" type="button" onclick="statusComplete(this)">Complete</button>&nbsp<button id="' + row.id +'" class="btn btn-danger btn-fill btn-sm" type="button" onclick="statusCancel(this)">Cancel</button>'
                }

            },
            {

                targets: [7],
                data: null,
                render : function (data, type, row) {
                    return '<button id="' + row.id +'" class="btn btn-warning btn-fill btn-sm" type="button" onclick="generateJobSlip(this)">Job Slip</button>'
                }

            }
        ],

        //Table Row Color code
        "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
            switch(aData.status){
                case 'STARTED':
                    $(nRow).css('color', 'green');
                    break;
                case 'COMPLETE':
                    $(nRow).css('color', 'red');
                    break;

            }
        }



    });


    // table click event
    $('#jobStatusTable tbody').on('click', 'tr', function() {
        var data = table.row(this).data();

        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
            clickedRow = false;
        } else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            clickedRow = true;
        }


    });





});

// Salary Slip method
function generateJobSlip(obj) {

    // this is jobId
    var id = $(obj).closest('tr').find('td:first').html();


    $.ajax({
        url: "/jobStatus/generateJobSlip?id=" + id,
        // dataType: 'json',
        type: 'get',
        success: function (response) {
            console.log(response.data);
            var fileName = "test.pdf";
            if (window.navigator && window.navigator.msSaveOrOpenBlob) { // IE workaround
                var byteCharacters = atob(response.data);
                var byteNumbers = new Array(byteCharacters.length);
                for (var i = 0; i < byteCharacters.length; i++) {
                    byteNumbers[i] = byteCharacters.charCodeAt(i);
                }
                var byteArray = new Uint8Array(byteNumbers);
                var blob = new Blob([byteArray], {type: 'application/pdf'});
                //var blob_iframe = document.querySelector('#blob-src-test');
                //blob_iframe.src = blob_url;
                window.navigator.msSaveOrOpenBlob(blob, fileName);
            }
            else { // much easier if not IE
                var pdfWindow = window.open("")
                pdfWindow.document.write("<iframe width='100%' height='100%' src='data:application/pdf;base64, " + encodeURI(response.data)+"'></iframe>")
            }
        },
        error: function (error) {
            console.log(error);
        }
    });
    return false;
}

function statusComplete(obj){
    const swalWithBootstrapButtons = swal.mixin({
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false,
    });

    swalWithBootstrapButtons({
        title: 'Confirmation',
        text: "Change Status to Complete",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No'
    }).then(function (result) {
        if (result.value) {
            var id = $(obj).closest('tr').find('td:first').html();
            var jobDTO = {};
            jobDTO.id = id;

            $.ajax({
                url: "/job/completeJob", // or whatever
                dataType: 'json',
                type: 'post',
                async: false,
                data: jobDTO,
                success: function (response) {
                    if (response.success) {
                        swal({
                            type: 'success',
                            title: 'Job Completed',
                            text: 'Job Successfully Completed'
                        });
                        table.ajax.reload();
                    } else {
                        swal(
                            'Error',
                            'There is an issue in updating',
                            'error'
                        )
                    }

                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    }
    );

}


function statusCancel(obj) {
    const swalWithBootstrapButtons = swal.mixin({
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false,
    });

    swalWithBootstrapButtons({
        title: 'Confirmation',
        text: "Change Status to Cancel",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No'
    }).then(function (result) {
        if (result.value) {
            var id = $(obj).closest('tr').find('td:first').html();
            var jobDTO = {};
            jobDTO.id = id;

            $.ajax({
                url: "/job/statusCancel", // or whatever
                dataType: 'json',
                type: 'post',
                async: false,
                data: jobDTO,
                success: function (response) {
                    if (response.success) {
                        swal({
                            type: 'danger',
                            title: 'Job Cancel',
                            text: 'Job Cancelled'
                        });
                        table.ajax.reload();
                    } else {
                        swal(
                            'Error',
                            'There is an issue in cancelling',
                            'error'
                        )
                    }

                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    }
    );
}