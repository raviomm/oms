/**
 * Created by RAVI KALUARACHCHI on 11/14/2019.
 */
var addedInvMap = [];
var table;
$(document).ready(function () {

    table1 = $('#invoiceTable').DataTable({
        processing: true,
        ajax: {
            url: "/invoice/getAllInvoices",
            "type": "POST",
            "data": function () {
                var invoiceSearchCriteria = {};
                invoiceSearchCriteria.invoiceNumber = $("#searchInvoiceInput").val();
                return invoiceSearchCriteria;
            },
            dataSrc: function (json) {

                console.log(json.tableData);
                return json.tableData;
            }
        },
        columns: [
            {title: "Invoice Id", data: "invoiceId"},
            {title: "Invoice Number", data: "invoiceNumber"},
            {title: "Supplier Name", data: "vendorName"},
            {title: "Supplier Address", data: "vendorAddress"},
            {title: "Supplier Contact", data: "vendorContactNumber"}
        ],
        columnDefs: [
            /*{
             targets: [0],
             visible: false,
             searchable: false
             },
             {
             targets: [2],
             visible: false,
             searchable: false
             },
             {
             targets: [7],
             visible: false,
             searchable: false
             }*/
        ]
    });

    // table click event
    $('#invoiceTable tbody').on('click', 'tr', function () {
        var data = table1.row(this).data();
        fillFormData(data);
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
            clickedRow = false;
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            clickedRow = true;
        }
        disableUpdate(false);
        /*disableDelete(false);*/
    });




// table for item object
    table = $('#invoiceItemTable').DataTable({
        columns: [
            {title: "Item Name", data: 'selectInvoiceItem'},
            {title: "Quantity", data: 'invoiceItemCount'},
            {title: "Remove"}
        ],
        columnDefs: [

            {
                targets: [2],
                data: null,
                render : function (data, type, row) {
                    return '<button id="' + row.id +'" class="btn btn-warning btn-fill btn-sm" type="button" onclick="invoiceItemsRemove(this)">Remove</button>'
                }

            }
        ],
        fnCreatedRow: function (nRow, aData, iDataIndex) {
            $(nRow).attr('data-id', aData.itemId);
        }
    });

    // item populating to drop down
    var selectInvoiceItem = $("#selectInvoiceItem");

    $.ajax({
        url: "/item/getAllItemsForDropdowns",
        dataType: 'json',
        type: 'get',
        success: function(response) {
            $.each(response.data, function() {
                selectInvoiceItem.append($("<option />").val(this.itemId).text(this.itemName));
            });
        }
    });

    // for add button item
    $("#invoiceItemAdd").click(function () {
        const selectInvoiceItem = $('#selectInvoiceItem :selected').text();
        const invoiceItemCount = $('#invoiceItemCount').val();



        if (selectInvoiceItem || invoiceItemCount !== undefined) {
            const index = addedInvMap.map(
                function (object, index) {

                    return object.selectInvoiceItem;

                }).indexOf(selectInvoiceItem);

            if (index >= 0) {
                alert("Can't add same item twice");
                document.getElementById("invoiceItemAdd").disabled = true;
            }
            else {
                const updatedItem = {selectInvoiceItem: selectInvoiceItem,invoiceItemCount: invoiceItemCount };
                addedInvMap.push(updatedItem);
                table.row.add(updatedItem).draw(false);
            }

        }
        return false;
    });

// item object
    $('#invoiceItemAdd tbody').on( 'click', 'button.btn-warning', function () {

        var selectInvoiceItem = $(this).closest('tr').find('td:nth-child(3)').html();

        const index = addedInvMap.map(
            function (object, index) {

                return object.selectInvoiceItem;

            }).indexOf(selectInvoiceItem);

        if (index >= 0) {
            addedInvMap.splice(index, 1);
        }
        table
            .row( $(this).parents('tr') )
            .remove()
            .draw();
    });

    $('#selectInvoiceItem').on('change', function() {
        if(this.value){
            document.getElementById("invoiceItemAdd").disabled = false;
            return;
        }
        document.getElementById("invoiceItemAdd").disabled = true;
    });




    $("#invoiceSave").click(function (e) {

        var date = $("#invoiceDate").val();
        var invoiceNumber = $("#invoiceNumber").val();
        var vendorName = $("#vendorName").val();
        var vendorAddress = $("#vendorAddress").val();
        var vendorContactNumber = $("#vendorContactNumber").val();

        console.log(addedInvMap);

        var data = {
            "invoiceDate" : date,
            "invoiceNumber" : invoiceNumber,
            "vendorName" : vendorName,
            "vendorAddress" : vendorAddress,
            "vendorContactNumber" : vendorContactNumber,
            "invoiceItemList" : addedInvMap
        }

        $.ajax({
            url: "/invoice/saveInvoice",
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            data :  JSON.stringify(data),
            type: 'post',
            success: function(response) {
                console.log(response);
                if(response !== undefined){
                    if(response.success){
                        alert("Successfully Saved Invoice, New Invoice Id:" + response.data);
                        table.clear();
                        $('#invoiceNumber').val("");
                        $("#jobDate").val("");
                        $("#vendorName").val("");
                        $("#vendorAddress").val("");
                        $("#vendorContactNumber").val("");

                    } else {
                        alert("Savings Job Failed!!!");
                    }
                } else {
                    alert("Savings Job Failed!!!");
                }
            }
        });

    });





    disableFormData(true);
    disableUpdate(true);
    /*disableDelete(true);*/
    $("#invoiceSave").show();
    $("#invoiceUpdateButton1").hide();

    $("#searchInvoice").click(function (e) {
        e.preventDefault();
        table.ajax.reload();
    });


});


//function for remove button on item table column
function invoiceItemsRemove(object) {
    var selectInvoiceItem = $(object).closest('tr').find('td:nth-child(1)').html();
    var invoiceItemCount = $(object).closest('tr').find('td:nth-child(2)').html();

    $("#selectInvoiceItem").val(selectInvoiceItem);
    $("#invoiceItemCount").val(invoiceItemCount);


    const index = addedInvMap.map(
        function (object, index) {

            return object.selectInvoiceItem;

        }).indexOf(selectInvoiceItem);
    if (index >= 0) {
        addedInvMap.splice(index, 1);
    }
    table
        .row( $(object).parents('tr') )
        .remove()
        .draw();

}

function invoiceRefresh() {
    location.reload();
}


function invoiceAdd() {
    resetForm();
    disableFormData(false);
    disableUpdate(true);
    /*disableDelete(true);*/
    /*table.ajax.reload();*/
    /*$("#invoiceSave").prop('disabled', false).removeClass( 'disabled' );*/
    $("#invoiceUpdateButton1").hide();


}

// form below need to change

function resetForm() {
    $(':input', '#invoiceForm')
        .not(':button, :submit, :reset, :hidden')
        .val('')
        .removeAttr('checked')
        .removeAttr('selected');
}


function invoiceClear(disable) {
    /*$("#employeeDeleteButton").prop('disabled', disable);*/
    resetForm();
    disableFormData(true);
    disableUpdate(true);
    /*disableDelete(true);*/
    table1.ajax.reload();
    $( "#invoiceUpdateButton1" ).hide();
    $( "#invoiceSave" ).show();
    $( "#invoiceSave" ).prop('disabled', true).addClass( 'disabled' );

}
function disableFormData(disable) {
    $("#invoiceId").prop('disabled', disable);
    $("#invoiceNumber").prop('disabled', disable);
    $("#vendorName").prop('disabled', disable);
    $("#vendorAddress").prop('disabled', disable);
    $("#vendorContactNumber").prop('disabled', disable);
    $("#selectInvoiceItem").prop('disabled', disable);
    $("#invoiceDate").prop('disabled', disable);
   /* $("#invoiceItemAdd").prop('disabled', disable);
    $("#invoiceItemTable").prop('disabled', disable);*/

}

function disableUpdate(disable) {
    $("#invoiceUpdateButton1").prop('disabled', disable);

}

// Edit Button Click Function
function invoiceUpdate() {
    if (clickedRow) {
        disableFormData(true);
        $("#invoiceId").prop('disabled', false);
        $("#invoiceDate").prop('disabled', false);
        $("#invoiceNumber").prop('disabled', false);
        $("#vendorName").prop('disabled', false);
        $("#vendorAddress").prop('disabled', false);
        $("#vendorContactNumber").prop('disabled', false);
        $("#selectInvoiceItem").prop('disabled', false);
        $("#invoiceItemCount").prop('disabled', false);
        $("#invoiceItemAdd").prop('disabled',false).removeClass('disabled');
        $("#invoiceUpdateButton1").show();
        $("#invoiceSave").hide();

    } else {
        alert("Please Select a Row first !!!!");
    }
}
/*function disableDelete(disable) {
    $("#employeeDeleteButton").prop('disabled', disable);

}*/

function fillFormData(row) {
    $("#invoiceId").val(row.invoiceId);
    $("#invoiceDate").val(row.invoiceDate);
    $("#invoiceNumber").val(row.invoiceNumber);
    $("#vendorName").val(row.vendorName);
    $("#vendorAddress").val(row.vendorAddress);
    $("#vendorContactNumber").val(row.vendorContactNumber);



}


function disableUpdate(disable) {
    $("#invoiceUpdateButton").prop('disabled', disable);

}







/*// Edit Button Click Function
function employeeUpdate() {
    if (clickedRow) {
        disableFormData(true);
        $("#employeeLastName").prop('disabled', false);
        $("#employeeAddress").prop('disabled', false);
        $("#employeeMobileNumber").prop('disabled', false);
        $("#employeeEmail").prop('disabled', false);
        $("#employeeBasicSalary").prop('disabled', false);
        $("#employeeAllowance").prop('disabled', false);
        $("#employeeDesignation").prop('disabled', false);
        $("#employeeStatus").prop('disabled', false);
        $("#employeeUpdateButton1").prop('disabled',false).removeClass('disabled');
        $("#employeeUpdateButton1").show();
        $("#employeeSave").hide();

    } else {
        alert("Please Select a Row first !!!!");
    }
}*/



/*function employeeDelete() {
    if (clickedRow) {
        const swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
        });

        swalWithBootstrapButtons({
            title: 'Confirmation',
            text: "Are you sure you want delete it?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Delete',
            cancelButtonText: 'Cancel'
        }).then(function (result) {
                if (result.value) {
                    var employeeDTO = {};
                    employeeDTO.employeeId = $("#employeeId").val();
                    employeeDTO.employeeFirstName = $("#employeeFirstName").val();
                    employeeDTO.employeeLastName = $("#employeeLastName").val();
                    employeeDTO.employeeNIC = $("#employeeNIC").val();
                    employeeDTO.employeeEPF = $("#employeeEPF").val();
                    employeeDTO.employeeAddress = $("#employeeAddress").val();
                    employeeDTO.employeeMobileNumber = $("#employeeMobileNumber").val();
                    employeeDTO.employeeEmail = $("#employeeEmail").val();
                    employeeDTO.employeeBasicSalary = $("#employeeBasicSalary").val();
                    employeeDTO.employeeAllowance = $("#employeeAllowance").val();
                    employeeDTO.employeeDesignation = $("#employeeDesignation").val();
                    employeeDTO.employeeStatus = $("#employeeStatus").val();

                    $.ajax({
                        url: '/employee/deleteEmployee', // or whatever
                        dataType: 'json',
                        type: 'post',
                        data: employeeDTO,
                        success: function (response) {
                            if (response.success) {
                                swal({
                                    type: 'success',
                                    title: 'Deleted',
                                    text: 'Row is successfully deleted'
                                })
                                employeeClear(true);
                            } else {
                                swal(
                                    'Error',
                                    'There is an issue in deleting',
                                    'error'
                                )
                            }
                        }
                    })
                    ;
                }
            }
        );
    } else {
        swal({
            type: 'error',
            title: 'Error',
            text: 'Please Select a Row first'
        })
    }

}*/



/*function searchEmployee(e) {
 e.preventDefault();
 table.ajax.reload();
 }*/
