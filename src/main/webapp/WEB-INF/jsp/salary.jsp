<%--
  Created by IntelliJ IDEA.
  User: RAVI KALUARACHCHI
  Date: 12/15/2017
  Time: 7:30 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">


    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
    <meta name="viewport" content="width=device-width">


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">


    <!-- Data table css     -->
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">

    <!-- Animation library for notifications   -->datetimepicker10
    <link href="assets/css/animate.min.css" rel="stylesheet">

    <!--  Light Bootstrap Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet">


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet">


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Roboto:400,700,300" rel="stylesheet" type="text/css">
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet">


</head>
<body style="">
<div class="sidebar" data-color="green" data-image="assets/img/sidebar-5.jpg">


    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="http://www.creative-tim.com" class="simple-text">
                P & H Enterprises
            </a>
        </div>

        <ul class="nav">
            <li class="deactive">
                <a href="manageHome.html">
                    <i class="pe-7s-graph"></i>
                    <p>Home</p>
                </a>
            </li>
            <li class="">
                <a href="manageEmployee.html">
                    <i class="pe-7s-user"></i>
                    <p>Employee</p>
                </a>
            </li>

            <li >
                <a href="manageItem.html">
                    <i class="pe-7s-note2"></i>
                    <p>Item</p>
                </a>
            </li>
            <li class="">
                <a href="manageRepair.html">
                    <i class="pe-7s-note2"></i>
                    <p>Repair</p>
                </a>
            </li>
            <li class="">
                <a href="manageRent.html">
                    <i class="pe-7s-note2"></i>
                    <p>Rent</p>
                </a>
            </li>
            <li class="">
                <a href="manageJob.html">
                    <i class="pe-7s-note2"></i>
                    <p>Job</p>
                </a>
            </li>
            <li>
                <a href="managejobStatus.html">
                    <i class="pe-7s-note2"></i>
                    <p>Job Inquire</p>
                </a>
            </li>
            <li class="">
                <a href="manageInvoice.html">
                    <i class="pe-7s-graph"></i>
                    <p>Invoice</p>
                </a>
            </li>
            <li class="active">
                <a href="manageSalary.html">
                    <i class="pe-7s-note2"></i>
                    <p>Salary Calculation</p>
                </a>
            </li>
            <li>
                <a href="manageReport.html">
                    <i class="pe-7s-note2"></i>
                    <p>Report Generator</p>
                </a>
            </li>
        </ul>
    </div>
    <div class="sidebar-background" style="background-image: url(assets/img/sidebar-5.jpg) "></div>
</div>

<div class="wrapper">
    <div class="main-panel">

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-10">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Salary Calculation Page</h4>
                            </div>

                            <div class="content">

                                <div class="row">

                                        <form id="salaryForm" data-toggle="validator" role="form">
                                            <div class="col-md-6">
                                            <input id="salaryId" type="text" hidden>

                                            <div class="form-group">
                                                <label for="selectEmployee"
                                                       class="control-label required"><b>Select Employee</b></label><br>
                                                <select id="selectEmployee" style="width: 300px"
                                                        data-error="Select  Employee"
                                                        class="form-control btn btn-default dropdown-toggle" required>
                                                    <option value="">Select  Employee</option>
                                                </select>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                            <br>
                                            <table class="table table-hover" style="width: 400px ">
                                                <thead>
                                                    <tr>
                                                        <th>Description</th>
                                                        <th>Detail</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td><b>Employee Name :</b></td>
                                                        <td>
                                                        <input type='text' id="salaryEmpName"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>NIC</b></td>
                                                        <td>
                                                        <input type='text' id="empDesignation"/></td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>EPF No</b></td>
                                                        <td ><input type='text' id="salaryEpfNo"/></td></td>

                                                    </tr>
                                                    <tr>
                                                        <td><b>Basic Salary</b></td>
                                                        <td id="">
                                                        <input type='text' id="basicSalary"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>Mobile Number</b></td>
                                                        <td id="">
                                                        <input type='text' id="mobileNo"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>Attendance Allowance</b></td>
                                                        <td id="">
                                                        <input type='text' id="allowance"/>
                                                        </td>
                                                    </tr>
                                                     <tr>
                                                           <td><b>Designation</b></td>
                                                            <td id="">
                                                            <input type='text' id="designation"/>
                                                      </td>
                                                 </tr>


                                                </tbody>
                                            </table>

                                            </div>
                                            <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="attendanceCount" class="control-label required"><b>Attendance
                                                    Count</b></label><br>
                                                <select id="attendanceCount" data-error="Select the attendance count" class="form-control btn btn-default dropdown-toggle" style="width: 300px" required>
                                                    <option value="">Select Attendance count</option>
                                                    <option value="16">16</option>
                                                    <option value="25">25</option>
                                                </select>
                                                <div class="help-block with-errors"></div>
                                            </div>

                                                <br>
                                                <br>
                                            <div class="form-group">
                                                <label class="control-label required"><b>Year and Month</b></label>
                                                <div class="container">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <div class='input-group date' id='datetimepicker10'>
                                                                <input type='text' class="form-control"
                                                                       data-error="Select a year and a month"
                                                                       required id="dateInput"/>
                                                                <span class="input-group-addon">
                                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                                </span>
                                                            </div>
                                                            <div class="help-block with-errors"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <button id="calculateSalary" onclick=""
                                                    class="btn btn-success btn-fill pull-left" type="submit">Calculate
                                            </button>
                                            <br>
                                            <br>
                                            <br>
                                            <table class="table table-hover" style="width: 300px">
                                                <thead>
                                                <tr>
                                                    <th>Description</th>
                                                    <th>Detail</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td><b>Salary Amount :</b></td>
                                                    <td id="">
                                                    <input type='text' id="salaryAmount"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><b>Total Repair :</b></td>
                                                    <td id="">
                                                        <input type='text' id="totalRepair"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><b>Attendance Allowance :</b></td>
                                                    <td id="">
                                                        <input type='text' id="attendenceAllowance"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><b>Eligible Repair :</b></td>
                                                    <td id="">
                                                        <input type='text' id="eligibleRepairAmount"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><b>No of Jobs :</b></td>
                                                    <td id="">
                                                        <input type='text' id="noOfJobs"/>
                                                    </td>
                                                </tr>

                                                </tbody>
                                            </table>
                                            <br>
                                            <br>
                                            <button id="generateReportId"
                                                    class="btn btn-info btn-fill pull-left">Report
                                            </button>
                                            </div>
                                        </form>


                                </div>
                                <br> <br>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


    <footer class="footer">

    </footer>

</div>



<!--   Core JS Files   -->
<script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/validator.min.js" type="text/javascript"></script>

<!--  Checkbox, Radio & Switch Plugins -->
<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

<!--  Charts Plugin -->
<script src="assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="assets/js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="assets/js/light-bootstrap-dashboard.js"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="assets/js/demo.js"></script>
<%--<script src="assets/js/transition.js"></script>--%>
<%--<script src="assets/js/collapse.js"></script>--%>

<!-- JQuery data table adding -->
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/eonasdan-bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

<!-- Include the js file to the page -->
<script src="assets/js/custom/manageSalary.js"></script>


</body>
</html>
</body>
</html>
