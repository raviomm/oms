<html lang="en"><head>
    <meta charset="utf-8">
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">


    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
    <meta name="viewport" content="width=device-width">


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">


    <!-- Data table css     -->
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet">

    <!--  Light Bootstrap Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet">


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet">


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Roboto:400,700,300" rel="stylesheet" type="text/css">
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/sweetalert2.min.css">



    <script type="text/javascript" charset="UTF-8" src="https://maps.googleapis.com/maps-api-v3/api/js/31/0/common.js"></script>
    <script type="text/javascript" charset="UTF-8" src="https://maps.googleapis.com/maps-api-v3/api/js/31/0/util.js"></script>
    <script type="text/javascript" charset="UTF-8" src="https://maps.googleapis.com/maps-api-v3/api/js/31/0/stats.js"></script>
</head>
<body style="">
<div class="sidebar" data-color="azure" data-image="assets/img/sidebar-5.jpg">

    <!--
		Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag
	-->
    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="http://www.creative-tim.com" class="simple-text">
                Sithumina Transport
            </a>
        </div>

        <ul class="nav">
            <li class="deactive">
                <a href="manageHome.html">
                    <i class="pe-7s-graph"></i>
                    <p>Home</p>
                </a>
            </li>
            <li>
                <a href="manageEmployee.html">
                    <i class="pe-7s-user"></i>
                    <p>Employee</p>
                </a>
            </li>
            <li></li><li class="">
            <a href="manageVehicle.html">
                <i class="pe-7s-note2"></i>
                <p>Vehicle</p>
            </a>
        </li>
            <li class="active">
                <a href="manageRoute.html">
                    <i class="pe-7s-news-paper"></i>
                    <p>Route</p>
                </a>
            </li>
            <li>
                <a href="manageProduct.html">
                    <i class="pe-7s-science"></i>
                    <p>Product</p>
                </a>
            </li>
            <li>
                <a href="manageFleet.html">
                    <i class="pe-7s-map-marker"></i>
                    <p>Fleets</p>
                </a>
            </li>
            <li>
                <a href="manageFleetHistory.html">
                    <i class="pe-7s-graph"></i>
                    <p>Fleet History</p>
                </a>
            </li>
            <li>
                <a href="manageSalary.html">
                    <i class="pe-7s-note2"></i>
                    <p>Salary Calculation</p>
                </a>
            </li>
            <li>
                <a href="manageReport.html">
                    <i class="pe-7s-note2"></i>
                    <p>Report Generator</p>
                </a>
            </li>
        </ul>
    </div>
    <div class="sidebar-background" style="background-image: url(assets/img/sidebar-5.jpg) "></div></div>

<div class="wrapper">
    <div class="main-panel">

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Route Managing Page</h4>
                            </div>

                            <div class="content">

                                    <div class="row">
                                        <div class="col-md-4">
                                            <form id="routeForm" data-toggle="validator" role="form">
                                                    <input id="routeId" type="text" hidden>
                                                <div class="form-group">
                                                    <label for="routeName" class="control-label required"><b>Route Name</b></label>
                                                    <input id="routeName" type="text" class="form-control"
                                                           placeholder="Enter Route Name" value="" data-error="Enter Route Name" required>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="routeOrigin"
                                                           class="control-label required"><b>Origin</b></label>
                                                    <select id="routeOrigin"
                                                            data-error="Select Origin"
                                                            class="form-control btn btn-default dropdown-toggle" required>
                                                        <option value="">Select Origin</option>
                                                        <option value="Ranala">Ranala</option>
                                                        <option value="Rathmalana">Rathmalana</option>
                                                        <option value="Kurunegala">Kurunegala</option>
                                                        <option value="Kottawa">Kottawa</option>
                                                        <option value="Matara">Matara</option>
                                                        <option value="Kandy">Kandy</option>
                                                        <option value="Anuradhapura">Anuradhapura</option>
                                                    </select>
                                                    <div class="help-block with-errors"></div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="routeDestination"
                                                           class="control-label required"><b>Destination</b></label>
                                                    <select id="routeDestination"
                                                            data-error="Select Destination"
                                                            class="form-control btn btn-default dropdown-toggle" required>
                                                        <option value="">Select Destination</option>
                                                        <option value="Ranala">Ranala</option>
                                                        <option value="Rathmalana">Rathmalana</option>
                                                        <option value="Kurunegala">Kurunegala</option>
                                                        <option value="Kottawa">Kottawa</option>
                                                        <option value="Matara">Matara</option>
                                                        <option value="Kandy">Kandy</option>
                                                        <option value="Anuradhapura">Anuradhapura</option>
                                                    </select>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="routeDistance" class="control-label"><b>Distance</b></label>
                                                    <input id="routeDistance" type="text" class="form-control"
                                                           placeholder="Enter Distance" value="" data-error="Enter Distance" required>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="routeDuration" class="control-label"><b>Duration</b></label>
                                                    <input id="routeDuration" type="text" class="form-control"
                                                           placeholder="Enter Duration" value="" data-error="Enter Duration" required>
                                                    <div class="help-block with-errors"></div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="routeStatus"
                                                           class="control-label required"><b>Status</b></label>
                                                    <select id="routeStatus"
                                                            data-error="Select Status"
                                                            class="form-control btn btn-default dropdown-toggle" required>
                                                        <option value="">Select Status</option>
                                                        <option value="ACT">Active</option>
                                                        <option value="INA">Inactive</option>
                                                    </select>
                                                    <div class="help-block with-errors"></div>
                                                </div>

                                                    <br>
                                                    <button type="submit" id="routeSaveButton" class="btn btn-info btn-fill pull-left">Save</button>
                                                    <button type="submit" id="routeUpdateButton" class="btn btn-info btn-fill pull-left">Update</button>
                                                    <br>
                                                    <br>



                                                <br> <br>



                                            </form>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>




                    <div class="col-md-8">
                        <div class="card card-user">
                            <div class="content">
                                <%--<div class="pull-center">
                                    <h5>Search Route</h5>
                                    <input type="text" id="searchRouteId" class="form-control1" placeholder="TYPE YOUR ROUTE ID HERE" name="search"><br>
                                    <button type="submit" onclick="searchRoute()" class="btn btn-info btn-fill pull-center">Search</button>&nbsp &nbsp &nbsp


                                    <br>
                                </div>--%>
                                    <form id="routeForm2" data-toggle="validator" role="form">
                                        <div class="form-group">
                                            <label for="searchRouteId" class="control-label required"><b>Search Route</b></label>
                                            <input id="searchRouteId" type="text" style="width: 450px" class="form-control"
                                                   placeholder="Type Route Name" value=""
                                                   data-error="Type Route Name" required/>
                                            <div class="help-block with-errors"></div>
                                        </div>

                                        <button id="searchRoute" onclick=""
                                                class="btn btn-info btn-fill pull-left">Search
                                        </button>
                                    </form>

                                    <br><br>

                                <div class="content table-responsive table-full-width">
                                    <table id="routeTable" class="display" width="100%"></table>

                                    <br>
                                    <br>
                                    <div class="pull-center">
                                        <button type="submit" id="routeAddButton" onclick="routeAdd()" class="btn btn-info btn-fill pull-center">Add</button>&nbsp &nbsp &nbsp
                                        <button type="submit" id="routeEditButton" onclick="routeEdit()" class="btn btn-info btn-fill pull-center">Edit</button>&nbsp &nbsp &nbsp
                                        <button type="submit" id="routeDeleteButton" onclick="routeDelete()" class="btn btn-info btn-fill pull-center">Delete</button>&nbsp &nbsp &nbsp
                                        <button type="submit" id="routeClearButton" onclick="routeClear()" class="btn btn-info btn-fill pull-center">Clear</button>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>







            <footer class="footer">

            </footer>

        </div>
    </div>



    <!-- Alert Box -->
    <!-- Optional: include a polyfill for ES6 Promises for IE11 and Android browser -->
    <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
    <script src="assets/js/sweetalert2.min.js"></script>


    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/js/validator.min.js" type="text/javascript"></script>

    <!--  Checkbox, Radio & Switch Plugins -->
    <script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Charts Plugin -->
    <script src="assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
    <script src="assets/js/light-bootstrap-dashboard.js"></script>

    <!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
    <script src="assets/js/demo.js"></script>

    <!-- JQuery data table adding -->
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>

    <!-- Include the js file to the page -->
    <script src="assets/js/custom/manageRoute.js"></script>


</div>
</body>
</html>