<html lang="en"><head>
    <meta charset="utf-8">
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">


    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
    <meta name="viewport" content="width=device-width">


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">


    <!-- Data table css     -->
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet">

    <!--  Light Bootstrap Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet">


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet">


    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>Light Bootstrap Dashboard by Creative Tim</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

    <style>
        .img {

            height: 200px;
            width: 200px;
            margin: 0 auto;
            background-position: 50% 50%;
            background-repeat: no-repeat;
            background-size: cover;
            background-image: url("assets/img/ship.png");


        }
        img {
            border: 5px solid #ddd;
            border-radius: 4px;
            padding: 5px;
            width: 150px;
        }

        img:hover {
            box-shadow: 0 0 2px 1px rgba(0, 140, 186, 0.5);
        }


    </style>
    <script type="text/javascript" charset="UTF-8" src="https://maps.googleapis.com/maps-api-v3/api/js/31/0/common.js"></script><script type="text/javascript" charset="UTF-8" src="https://maps.googleapis.com/maps-api-v3/api/js/31/0/util.js"></script><script type="text/javascript" charset="UTF-8" src="https://maps.googleapis.com/maps-api-v3/api/js/31/0/stats.js"></script></head>

<%--<div class="sidebar" data-color="azure">

    <!--
		Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag
	-->
    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="http://www.creative-tim.com" class="simple-text">
                P & H Enterprises
            </a>
        </div>

        <ul class="nav">
            <li class="active">
                <a href="manageHome.html">
                    <i class="pe-7s-graph"></i>
                    <p>Home</p>
                </a>
            </li>
            <li>
                <a href="manageEmployee.html">
                    <i class="pe-7s-user"></i>
                    <p>Employee</p>
                </a>
            </li>
            <li class="">
                <a href="manageItem.html">
                    <i class="pe-7s-user"></i>
                    <p>Item</p>
                </a>
            </li>
            <li class="">
                <a href="manageRepair.html">
                    <i class="pe-7s-note2"></i>
                    <p>Repair</p>
                </a>
            </li>
            <li class="">
                <a href="manageRent.html">
                    <i class="pe-7s-note2"></i>
                    <p>Rent</p>
                </a>
            </li>
            <li class="">
                <a href="manageJob.html">
                    <i class="pe-7s-note2"></i>
                    <p>Job</p>
                </a>
            </li>
            &lt;%&ndash;<li class="">
            <a href="manageVehicle.html">
                <i class="pe-7s-note2"></i>
                <p>Vehicle</p>
            </a>
        </li>
            <li >
                <a href="manageRoute.html">
                    <i class="pe-7s-news-paper"></i>
                    <p>Route</p>
                </a>
            </li>
            <li>
                <a href="manageProduct.html">
                    <i class="pe-7s-science"></i>
                    <p>Product</p>
                </a>
            </li>
            <li class="">
                <a href="manageFleet.html">
                    <i class="pe-7s-map-marker"></i>
                    <p>Fleets</p>
                </a>
            </li>&ndash;%&gt;
            <li class="">
                <a href="manageInvoice.html">
                    <i class="pe-7s-graph"></i>
                    <p>Invoice</p>
                </a>
            </li>
            <li>
                <a href="manageSalary.html">
                    <i class="pe-7s-note2"></i>
                    <p>Salary Calculation</p>
                </a>
            </li>
            <li>
                <a href="manageReport.html">
                    <i class="pe-7s-note2"></i>
                    <p>Report Generator</p>
                </a>
            </li>
        </ul>
    </div>
    </div>--%>


<body>
<div class="wrapper">
    <div class="">
        <div class="content">
            <div class="container-fluid">
                <div class="row">

                    <%--<div class="col-md-4">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Email Statistics</h4>
                                <p class="category">Last Campaign Performance</p>
                            </div>
                            <div class="content">
                                <div id="chartPreferences" class="ct-chart ct-perfect-fourth"></div>

                                <div class="footer">
                                    <div class="legend">
                                        <i class="fa fa-circle text-info"></i> Open
                                        <i class="fa fa-circle text-danger"></i> Bounce
                                        <i class="fa fa-circle text-warning"></i> Unsubscribe
                                    </div>
                                    <hr>
                                    <div class="stats">
                                        <i class="fa fa-clock-o"></i> Campaign sent 2 days ago
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>--%>

                    <div class="col-md-12">

                        <div class="card">
                            <div class="img img-thumbnail center-block img-circle "></div>

                            <div class="content">
                                <p font-size="50px" class="description text-center">
                                <h2 class="text-center">Welcome to P and H Enterprises</h2>

                                </p>
                            </div>
                            <hr>

                        </div>



                    </div>
                </div>



                <div class="row">
                    <div class="col-md-14">
                        <div class="card ">
                            <div class="header">




                                <div class="content ">
                                    <div style="align-content: center">

                                        <a target="_blank" href="http://localhost:8080/manageEmployee.html">
                                            <label style="color:black">Employee</label>
                                            <img src="assets/img/employee-home-image.jpg" alt="Employee" style="width:150px">
                                        </a>
                                        <a target="_blank" href="http://localhost:8080/manageItem.html">
                                            <label style="color:black">Item</label>
                                            <img src="assets/img/item-home.png" alt="Item" style="width:150px">
                                        </a>
                                        <a target="_blank" href="http://localhost:8080/manageRepair.html">
                                            <label style="color:black">Repair</label>
                                            <img src="assets/img/repair-home.png" alt="Repair" style="width:150px">
                                        </a>
                                        <a target="_blank" href="http://localhost:8080/manageRent.html">
                                            <label style="color:black">Rent</label>
                                            <img src="assets/img/rent-home.png" alt="Rent" style="width:150px">
                                        </a>
                                        <a target="_blank" href="http://localhost:8080/manageJob.html">
                                            <label style="color:black">Job</label>
                                            <img src="assets/img/job-home.png" alt="Job" style="width:150px">
                                        </a>
                                        <a target="_blank" href="http://localhost:8080/managejobStatus.html">
                                            <label style="color:black">Job Inquire</label>
                                            <img src="assets/img/job-inquire.png" alt="Job" style="width:150px">
                                        </a>
                                        <a target="_blank" href="http://localhost:8080/manageInvoice.html">
                                            <label style="color:black">Invoice</label>
                                            <img src="assets/img/invoice-home.png" alt="Invoice" style="width:150px">
                                        </a>
                                        <a target="_blank" href="http://localhost:8080/manageSalary.html">
                                            <label style="color:black">Salary</label>
                                            <img src="assets/img/salary-home.png" alt="Salary" style="width:150px">
                                        </a>
                                        <a target="_blank" href="http://localhost:8080/manageReport.html">
                                            <label style="color:black">Report</label>
                                            <img src="assets/img/report-home.png" alt="Report" style="width:150px">
                                        </a>
                                    </div>

                                    <%--<div class="content table-responsive table-full-width">
                                        <table id="homeTable" class="display" width="100%"></table>

                                        <br>
                                        <br>

                                    </div>--%>


                                    <!-- Modal -->
                                    <div id="myModal" class="modal fade" role="dialog">
                                        <div class="modal-dialog">

                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title"></h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p></p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <! --Buttons for Table End Above>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <%--<footer class="footer">
        &lt;%&ndash;<div class="container-fluid">
            <nav class="pull-left">
                <ul>
                    <li>
                        <a href="#">
                            Home
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Company
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Portfolio
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Blog
                        </a>
                    </li>
                </ul>
            </nav>
            &lt;%&ndash;<p class="copyright pull-right">
                &copy; <script>document.write(new Date().getFullYear())</script> <a href="http://www.creative-tim.com">Creative Tim</a>, made with love for a better web
            </p>&ndash;%&gt;
        </div>&ndash;%&gt;
    </footer>--%>

</div>






<!--   Core JS Files   -->
<script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

<!--  Checkbox, Radio & Switch Plugins -->
<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

<!--  Charts Plugin -->
<script src="assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="assets/js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="assets/js/light-bootstrap-dashboard.js"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="assets/js/demo.js"></script>

<!-- JQuery data table adding -->
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>

<!-- Include the js file to the page-->
<script src="assets/js/custom/manageHome.js"></script>


</div>
</body>
</html>