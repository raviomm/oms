<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <%--<style>
        .img {
            background-image: url("assets/img/ship-cartoon-2.png");
            background-position: 50% 50%;
            background-repeat: no-repeat;
            background-size: cover;
        }
        /*.no-background {
            background-image: url("assets/img/ship.png");
        }*/
    </style>--%>
    <title>Spring Security Example</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js" type="text/javascript"></script>


</head>
<body >

</br>
</br>
<h3 style="font-size:50px;background-color: #a0d0ff ;"  align="center">Welcome to P & S Enterprises</h3>
<div class="container-fluid "    align="center"  style="background-color: grey; width: 50%; /*margin:auto;*/" >

    <label id="loginError" style="display: none;"><h4 style="color: darkred; font-size: 2em;" >Invalid User Name or Password</h4></label>
    <c:if test="${param.error ne null}">
        <%--<div style="color: white; font-size: 30px">Invalid credentials.</div>--%>
        <script>
            document.getElementById("loginError").style.display = 'block';

        </script>
    </c:if>

    <form action="/login" method="post" data-toggle="validator" role="form">
        <div align="center" class="card" style="background-color: deepskyblue;width: 90%;"  >
            <br>
            <div class="form-group">
                <label for="username" class="control-label required"><h4>UserName:</h4>
                    <input type="text"  class="form-control2" placeholder="Enter Username" id="username" name="username"  maxlength="30" size="30" value="" required>
                </label></div>
            <br><br>
            <div class="form-group">
                <label for="pwd"><h4>Password:</h4>
                    <input id="pwd" name="password" type="password" class="form-control2" placeholder="Enter password"  name="password"   maxlength="30" size="30" value=""
                           data-error="Enter correct password" required>
                </label></div>
            <br>
            <br>
            <br>
            <br>

            <br>

        </div>
        <div class="col-md-4 center-block">
            <button type="submit" class="btn btn-info btn-fill pull-left btn-lg center-block"  style="width: 200px;"  >Login</button>
        </div><br>


        <input type="hidden" name="${_csrf.parameterName}"
               value="${_csrf.token}" />
    </form>

</div>


</body>
</html>