<%--
  Created by IntelliJ IDEA.
  User: RAVI KALUARACHCHI
  Date: 11/14/2019
  Time: 10:12 AM
  To change this template use File | Settings | File Templates.
--%>
<html lang="en">
<head>
    <script>

    </script>

    <meta charset="utf-8">
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">


    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
    <meta name="viewport" content="width=device-width">


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">


    <!-- Data table css     -->
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet">

    <!--  Light Bootstrap Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet">


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet">


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Roboto:400,700,300" rel="stylesheet" type="text/css">
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/sweetalert2.min.css">

    <script type="text/javascript" charset="UTF-8"
            src="https://maps.googleapis.com/maps-api-v3/api/js/31/0/common.js"></script>
    <script type="text/javascript" charset="UTF-8"
            src="https://maps.googleapis.com/maps-api-v3/api/js/31/0/util.js"></script>
    <script type="text/javascript" charset="UTF-8"
            src="https://maps.googleapis.com/maps-api-v3/api/js/31/0/stats.js"></script>
</head>
<body style="">
<div class="sidebar" data-color="green" data-image="assets/img/sidebar-5.jpg">

    <!--
		Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag
	-->
    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="http://www.creative-tim.com" class="simple-text">
                P & H Enterprises
            </a>
        </div>

        <ul class="nav">
            <li class="deactive">
                <a href="manageHome.html">
                    <i class="pe-7s-graph"></i>
                    <p>Home</p>
                </a>
            </li>
            <li class="">
                <a href="manageEmployee.html">
                    <i class="pe-7s-user"></i>
                    <p>Employee</p>
                </a>
            </li>
            <li class="">
                <a href="manageItem.html">
                    <i class="pe-7s-user"></i>
                    <p>Item</p>
                </a>
            </li>
            <li class="">
                <a href="manageRepair.html">
                    <i class="pe-7s-note2"></i>
                    <p>Repair</p>
                </a>
            </li>
            <li class="">
                <a href="manageRent.html">
                    <i class="pe-7s-note2"></i>
                    <p>Rent</p>
                </a>
            </li>
            <li class="">
                <a href="manageJob.html">
                    <i class="pe-7s-note2"></i>
                    <p>Job</p>
                </a>
            </li>
            <li>
                <a href="managejobStatus.html">
                    <i class="pe-7s-note2"></i>
                    <p>Job Inquire</p>
                </a>
            </li>
            <li class="active">
                <a href="manageInvoice.html">
                    <i class="pe-7s-graph"></i>
                    <p>Invoice</p>
                </a>
            </li>
            <li>
                <a href="manageSalary.html">
                    <i class="pe-7s-note2"></i>
                    <p>Salary Calculation</p>
                </a>
            </li>
            <li>
                <a href="manageReport.html">
                    <i class="pe-7s-note2"></i>
                    <p>Report Generator</p>
                </a>
            </li>
        </ul>
    </div>
    <div class="sidebar-background" style="background-image: url(assets/img/sidebar-5.jpg) "></div>
</div>

<div class="wrapper">
    <div class="main-panel">
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"><b>Invoice Management</b></h4>
                            </div>

                            <div class="content">

                                <div class="row">
                                    <div class="col-md-4">
                                        <form id="invoiceForm" data-toggle="validator" role="form">
                                            <input id="invoiceId" type="text" hidden>
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label for="invoiceDate" class="control-label required"><b>Date</b></label>
                                                    <input id="invoiceDate" type="date" class="form-control"
                                                           placeholder="Enter Date" value="" data-error="Enter Date" required>
                                                    <div class="help-block with-errors"></div>
                                                </div>

                                            <div class="form-group">
                                                <label for="invoiceNumber" class="control-label required"><b>Invoice Number</b></label>
                                                <input id="invoiceNumber" type="text" class="form-control"
                                                       placeholder="Enter Invoice Number" value=""
                                                       data-error="Enter Invoice Number" required>
                                                <div class="help-block with-errors"></div>
                                            </div>

                                            <div class="form-group">
                                                <label for="vendorName" class="control-label required"><b>Supplier Name</b></label>
                                                <input id="vendorName"  type="text" class="form-control"
                                                       placeholder="Enter Vendor Name" value="" data-error="Enter Vendor Name" required>
                                                <p id="demo"></p>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="vendorAddress" class="control-label required"><b>Supplier Address</b></label>
                                                <input id="vendorAddress"  type="text" class="form-control"
                                                       placeholder="Enter Vendor Address" value="" data-error="Enter Vendor Address" required>
                                                <p id="demo"></p>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="vendorContactNumber" class="control-label required"><b>Supplier Contact Number</b></label>
                                                <input id="vendorContactNumber"  type="text" class="form-control" pattern="^([0][0-9]{9})"
                                                       placeholder="Enter Vendor Contact Number" value="" data-error="Enter Vendor Contact Number" required>
                                                <p id="demo"></p>
                                                <div class="help-block with-errors"></div>
                                            </div>

                                                <div class="form-group">
                                                    <label for="selectInvoiceItem"
                                                           class="control-label required"><b>Select Item</b></label><br>
                                                    <select id="selectInvoiceItem" style="width: 200px"
                                                            data-error="Select a Item"
                                                            class="form-control btn btn-default dropdown-toggle" required>
                                                        <option value="">Select a Item</option>
                                                    </select>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="invoiceItemCount" class="control-label required"><b>Count</b></label>
                                                    <input id="invoiceItemCount" type="text" style="width: 200px" class="form-control"
                                                           placeholder="Enter Count" value=""
                                                           data-error="Enter Count" required>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                                <button id="invoiceItemAdd"  class="btn btn-info btn-fill pull-left" disabled>Add Item
                                                </button>
                                                <br>
                                                <br>
                                                <div style="width: 400px"  class="content table-responsive table-full-width">
                                                    <table  id="invoiceItemTable"  class="display" width="100%"></table>
                                                </div>
                                                <button type="submit" id="invoiceSave"  class="btn btn-success btn-fill pull-left"> Save </button>
                                                <button type="submit" id="invoiceUpdateButton1"  class="btn btn-success btn-fill pull-left"> Update </button>

                                            <br>

                                            <br>
                                            <div class="clearfix"></div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-8">
                        <div class="card card-user">
                            <div class="content">
                                <form id="invoiceForm2" data-toggle="validator" role="form">
                                    <div class="form-group">
                                        <label for="searchInvoiceInput" class="control-label required"><b>Select Invoice</b></label>
                                        <input id="searchInvoiceInput" type="text" style="width: 450px" class="form-control"
                                               placeholder="TYPE Invoice Number" value=""
                                               data-error="TYPE Invoice Number" required/>
                                        <div class="help-block with-errors"></div>
                                    </div>

                                    <button id="searchInvoice" onclick="" type="submit"
                                            class="btn btn-secondary btn-fill pull-left">Search
                                    </button>
                                </form>
                                <br>


                                <br>


                                <br><br>

                                <div class="content table-responsive table-full-width">
                                    <table id="invoiceTable" class="display" width="100%"></table>

                                    <br>
                                    <br>
                                    <div class="pull-center">
                                        <button type="submit" id="invoiceRefreshButton" onclick="invoiceRefresh()"
                                                class="btn btn-primary btn-fill pull-center">Refresh
                                        </button>
                                        &nbsp &nbsp &nbsp
                                        <button type="submit" id="invoiceAddButton" onclick="invoiceAdd()"
                                                class="btn btn-success btn-fill pull-center">Add
                                        </button>
                                        <%--&nbsp &nbsp &nbsp
                                        <button type="submit" id="invoiceUpdateButton" onclick="invoiceUpdate()"
                                                class="btn btn-warning btn-fill pull-center">Edit
                                        </button>--%>
                                        &nbsp &nbsp &nbsp
                                        <button type="submit" id="invoiceClearButton" onclick="invoiceClear()"
                                                class="btn btn-info btn-fill pull-center">Clear
                                        </button>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



        <footer class="footer">

        </footer>
        </div>
    </div>



<!-- Alert Box -->
<!-- Optional: include a polyfill for ES6 Promises for IE11 and Android browser -->
<script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
<script src="assets/js/sweetalert2.min.js"></script>

<!--   Core JS Files   -->
<script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/validator.min.js" type="text/javascript"></script>

<!--  Checkbox, Radio & Switch Plugins -->
<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

<!--  Charts Plugin -->
<script src="assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="assets/js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="assets/js/light-bootstrap-dashboard.js"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="assets/js/demo.js"></script>

<!-- JQuery data table adding -->
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>

<!-- Include the js file to the page -->
<script src="assets/js/custom/manageInvoice.js"></script>
</div>

</body>
</html>