package com.oms.rent.services;

import com.oms.rent.dto.RentDTO;

import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 11/11/2019.
 */

public interface IRentService {

    boolean saveRent(RentDTO rentDTO);
    List<RentDTO> getAllRents();
    RentDTO getRentByCode(String rentCode);
    boolean updateRent (RentDTO rentDTO);
    /*boolean deleteRent (RentDTO rentDTO);*/

}
