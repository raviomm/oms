package com.oms.rent.services;

import com.oms.rent.adaptor.RentAdaptor;
import com.oms.rent.dao.IRentDAO;
import com.oms.rent.dto.RentDTO;
import com.oms.rent.model.Rent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 11/11/2019.
 */
@Service
public class RentService implements IRentService {

    @Autowired
    private IRentDAO rentDAO;

    public boolean saveRent(RentDTO rentDTO){

        RentAdaptor adaptor = new RentAdaptor();
        Rent rent = adaptor.toModel(rentDTO);
        return rentDAO.saveRent(rent);

    }

    public List<RentDTO> getAllRents(){

        List<RentDTO> rentDTOList = new ArrayList<>();
        RentAdaptor adaptor = new RentAdaptor();
        List<Rent> rents = rentDAO.getAllRents();

        if(rents != null & !rents.isEmpty()){

            for (Rent rent : rents){

                RentDTO rentDTO = adaptor.toDTO(rent);
                rentDTOList.add(rentDTO);

            }

        }
        return rentDTOList;

    }

    @Override
    public RentDTO getRentByCode(String rentCode){

        RentAdaptor adaptor = new RentAdaptor();
        Rent rent = rentDAO.getRentByCode(rentCode);

        RentDTO rentDTOList = adaptor.toDTO(rent);
        return rentDTOList;
    }

    @Override
    public boolean updateRent (RentDTO rentDTO){
        RentAdaptor adaptor = new RentAdaptor();
        Rent rent = adaptor.toModel(rentDTO);
        Rent existingRent = rentDAO.getRentByCode((rent.getRentCode()));
        if (existingRent != null){


            return rentDAO.updateRent(rent);

        } else {
            return false;
        }

    }

    /*@Override
    public boolean deleteRent (RentDTO rentDTO){
        RentAdaptor adaptor = new RentAdaptor();
        Rent rent = adaptor.toModel(rentDTO);
        Rent existingRent = rentDAO.getRentByCode(rent.getRentCode());
        if (existingRent !=null){
            return rentDAO.deleteRent(existingRent);
        }else{
            return false;
        }

    }*/
}
