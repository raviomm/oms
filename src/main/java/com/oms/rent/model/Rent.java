package com.oms.rent.model;

import javax.persistence.*;

/**
 * Created by RAVI KALUARACHCHI on 11/11/2019.
 */
@Entity
@Table (name = "rent")
public class Rent {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "rent_id")
    public Integer rentId;
    @Column(name = "rent_code")
    public String rentCode;
    @Column(name = "rent_resource")
    public String rentResource;
    @Column(name = "rent_charge")
    public Double rentCharge;
    @Column(name = "rent_description")
    public String rentDescription;

    public Integer getRentId() { return rentId; }

    public void setRentId(Integer rentId) { this.rentId = rentId; }

    public String getRentCode() { return rentCode; }

    public void setRentCode(String rentCode) { this.rentCode = rentCode; }

    public String getRentResource() { return rentResource; }

    public void setRentResource(String rentResource) { this.rentResource = rentResource; }

    public Double getRentCharge() { return rentCharge; }

    public void setRentCharge(Double rentCharge) { this.rentCharge = rentCharge; }

    public String getRentDescription() { return rentDescription; }

    public void setRentDescription(String rentDescription) { this.rentDescription = rentDescription; }
}
