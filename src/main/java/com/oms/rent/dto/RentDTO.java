package com.oms.rent.dto;

import java.io.Serializable;

/**
 * Created by RAVI KALUARACHCHI on 11/11/2019.
 */
public class RentDTO implements Serializable{

    private Integer rentId;
    private String rentCode;
    private String rentResource;
    private Double rentCharge;
    private String rentDescription;

    public Integer getRentId() { return rentId; }

    public void setRentId(Integer rentId) { this.rentId = rentId; }

    public String getRentCode() { return rentCode; }

    public void setRentCode(String rentCode) { this.rentCode = rentCode; }

    public String getRentResource() { return rentResource; }

    public void setRentResource(String rentResource) { this.rentResource = rentResource; }

    public Double getRentCharge() { return rentCharge; }

    public void setRentCharge(Double rentCharge) { this.rentCharge = rentCharge; }

    public String getRentDescription() { return rentDescription; }

    public void setRentDescription(String rentDescription) { this.rentDescription = rentDescription; }
}
