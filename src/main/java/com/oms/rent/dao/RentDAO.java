package com.oms.rent.dao;

import com.oms.rent.model.Rent;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 11/11/2019.
 */
@Transactional
@Repository
public class RentDAO implements IRentDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Rent getRentbyId (Integer rentId) {

        Rent rent = null ;
        String ehq1 = "FROM Rent r WHERE  r.rentId = :rentId" ;
        List<Rent> rents = entityManager.createQuery(ehq1).setParameter( "rentId",rentId).getResultList() ;
        if (rents != null && rents.size() > 0){

            rent = rents.get(0) ;
        }

        return rent ;

    }

    @Override
    public Rent getRentByCode (String rentCode) {

        Rent rent = null ;
        String ehq1 = "FROM Rent r WHERE  r.rentCode = :rentCode" ;
        List<Rent> rents = entityManager.createQuery(ehq1).setParameter( "rentCode",rentCode).getResultList() ;
        if (rents != null && rents.size() > 0){

            rent = rents.get(0) ;
        }

        return rent ;

    }

    @Override
    public boolean saveRent (Rent rent) {

        Rent persistedRent = null ;
        entityManager.persist(rent) ;
        persistedRent = getRentbyId(rent.getRentId()) ;
        if (persistedRent != null){

            return true;
        }
        return false;

    }


    @Override
    public List<Rent> getAllRents(){

        List<Rent> rents = entityManager.createQuery("select rent from Rent rent").getResultList();
        return rents;

    }

    @Override
    public boolean updateRent(Rent rent) {
        Rent persistedRent = null;
        entityManager.merge(rent);
        persistedRent = getRentByCode(rent.getRentCode());
        if(persistedRent != null){
            return  true;
        }
        return false;
    }

    /*@Override
    public boolean deleteRent(Rent rent) {
        try{
            entityManager.remove(rent);
        }catch (Exception e){
            return false;
        }

        return true;
    }*/

}
