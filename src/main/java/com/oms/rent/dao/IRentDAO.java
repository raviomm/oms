package com.oms.rent.dao;

import com.oms.rent.model.Rent;

import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 11/11/2019.
 */
public interface IRentDAO {

    Rent getRentbyId (Integer rentId);
    boolean saveRent (Rent rent);
    List<Rent> getAllRents();
    Rent getRentByCode (String rentCode);
    boolean updateRent(Rent rent);
    /*boolean deleteRent(Rent rent);*/
}
