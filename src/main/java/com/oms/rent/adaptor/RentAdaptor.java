package com.oms.rent.adaptor;

import com.oms.common.adaptor.BasicAdaptor;
import com.oms.rent.dto.RentDTO;
import com.oms.rent.model.Rent;

/**
 * Created by RAVI KALUARACHCHI on 11/11/2019.
 */
public class RentAdaptor extends BasicAdaptor< Rent, RentDTO >{
    @Override
    public Rent toModel(RentDTO dto) {
        Rent rent = new Rent();

        rent.setRentId(dto.getRentId());
        rent.setRentCode(dto.getRentCode());
        rent.setRentResource(dto.getRentResource());
        rent.setRentCharge(dto.getRentCharge());
        rent.setRentDescription(dto.getRentDescription());

        return rent;

    }

    @Override
    public RentDTO toDTO(Rent model) {

        RentDTO dto = new RentDTO();

        dto.setRentId(model.getRentId());
        dto.setRentCode(model.getRentCode());
        dto.setRentResource(model.getRentResource());
        dto.setRentCharge(model.getRentCharge());
        dto.setRentDescription(model.getRentDescription());

        return dto;

    }
}
