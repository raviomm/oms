package com.oms.product.services;

import com.oms.product.dto.ProductDTO;
import com.oms.product.model.Product;

import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 12/12/2017.
 */
public interface IProductService {

    List<ProductDTO> getAllProducts();

    ProductDTO getProductByName(String productNme);

    boolean saveProduct(ProductDTO productDTO);

    boolean updateProduct(ProductDTO productDTO);

    boolean deleteProduct(ProductDTO productDTO);
    Product getProductByID(Integer productId);



    }
