package com.oms.repair.services;

import com.oms.repair.adaptor.RepairAdaptor;
import com.oms.repair.dao.IRepairDAO;
import com.oms.repair.dto.RepairDTO;
import com.oms.repair.model.Repair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 11/11/2019.
 */
@Service
public class RepairServices implements IRepairServices {

    @Autowired
    private IRepairDAO repairDAO;
    @Override
    public boolean saveRepair(RepairDTO repairDTO){

        RepairAdaptor adaptor = new RepairAdaptor();
        Repair repair = adaptor.toModel(repairDTO);
//        employee.setCreatedTime(new Date());
//        employee.setModifiedTime(new Date());
//        employee.setVersion(0);
        return repairDAO.saveRepair(repair);

    }
    @Override
    public List<RepairDTO> getAllRepairs (){

        List<RepairDTO> repairDTOList = new ArrayList<>();
        RepairAdaptor adaptor = new RepairAdaptor();
        List<Repair> repairs = repairDAO.getAllRepairs();

        if (repairs != null & !repairs.isEmpty()){

            for (Repair repair: repairs){

                RepairDTO repairDTO = adaptor.toDTO(repair);
                repairDTOList.add(repairDTO);


            }

        }

        return repairDTOList;

    }

    @Override
    public RepairDTO getRepairByCode(String repairCode){

        RepairAdaptor adaptor = new RepairAdaptor();
        Repair repair = repairDAO.getRepairByCode(repairCode);

        RepairDTO repairDTOList = adaptor.toDTO(repair);
        return repairDTOList;
    }

    @Override
    public boolean updateRepair (RepairDTO repairDTO){
        RepairAdaptor adaptor = new RepairAdaptor();
        Repair repair = adaptor.toModel(repairDTO);
        Repair existingRepair = repairDAO.getRepairByCode(repair.getRepairCode());
        if (existingRepair != null){


            return repairDAO.updateRepair(repair);

        } else {
            return false;
        }

    }

    /*@Override
    public boolean deleteRepair (RepairDTO repairDTO){
        RepairAdaptor adaptor = new RepairAdaptor();
        Repair repair = adaptor.toModel(repairDTO);
        Repair existingRepair = repairDAO.getRepairByCode(repair.getRepairCode());
        if (existingRepair !=null){
            return repairDAO.deleteRepair(existingRepair);
        }else{
            return false;
        }

    }*/

}
