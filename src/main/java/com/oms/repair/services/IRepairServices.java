package com.oms.repair.services;

import com.oms.repair.dto.RepairDTO;

import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 11/11/2019.
 */
public interface IRepairServices {

    boolean saveRepair(RepairDTO repairDTO);
    List<RepairDTO> getAllRepairs ();
    RepairDTO getRepairByCode(String repairCode);
    boolean updateRepair (RepairDTO repairDTO);
    /*boolean deleteRepair (RepairDTO repairDTO);*/

}
