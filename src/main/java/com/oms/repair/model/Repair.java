package com.oms.repair.model;

import javax.persistence.*;

/**
 * Created by RAVI KALUARACHCHI on 11/11/2019.
 */

@Entity
@Table (name = "repair")
public class Repair {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "repair_id")
    public Integer repairId;
    @Column(name = "repair_name")
    public String repairName;
    @Column(name = "repair_code")
    public String repairCode;
    @Column(name = "repair_type")
    public String repairType;
    @Column(name = "repair_charge")
    public Double repairCharge;
    @Column(name = "repair_description")
    public String repairDescription;


    public Integer getRepairId() { return repairId; }

    public void setRepairId(Integer repairId) { this.repairId = repairId; }

    public String getRepairName() { return repairName; }

    public void setRepairName(String repairName) { this.repairName = repairName; }

    public String getRepairCode() { return repairCode; }

    public void setRepairCode(String repairCode) { this.repairCode = repairCode; }

    public String getRepairType() { return repairType; }

    public void setRepairType(String repairType) { this.repairType = repairType; }

    public Double getRepairCharge() { return repairCharge; }

    public void setRepairCharge(Double repairCharge) { this.repairCharge = repairCharge; }

    public String getRepairDescription() { return repairDescription; }

    public void setRepairDescription(String repairDescription) { this.repairDescription = repairDescription; }
}
