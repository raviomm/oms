package com.oms.repair.dto;

import java.io.Serializable;

/**
 * Created by RAVI KALUARACHCHI on 11/11/2019.
 */
public class RepairDTO implements Serializable {



    private Integer repairId;
    private String repairName;
    private String repairCode;
    private String repairType;
    private Double repairCharge;
    private String repairDescription;



    public Integer getRepairId() { return repairId; }

    public void setRepairId(Integer repairId) { this.repairId = repairId; }

    public String getRepairName() { return repairName; }

    public void setRepairName(String repairName) { this.repairName = repairName; }

    public String getRepairCode() { return repairCode; }

    public void setRepairCode(String repairCode) { this.repairCode = repairCode; }

    public String getRepairType() { return repairType; }

    public void setRepairType(String repairType) { this.repairType = repairType; }

    public Double getRepairCharge() { return repairCharge; }

    public void setRepairCharge(Double repairCharge) { this.repairCharge = repairCharge; }

    public String getRepairDescription() { return repairDescription; }

    public void setRepairDescription(String repairDescription) { this.repairDescription = repairDescription; }
}
