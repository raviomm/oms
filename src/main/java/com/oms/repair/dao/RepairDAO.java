package com.oms.repair.dao;

import com.oms.repair.model.Repair;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 11/11/2019.
 */
@Transactional
@Repository
public class RepairDAO implements IRepairDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Repair getRepairbyId (Integer repairId) {

        Repair repair = null ;
        String ehq1 = "FROM Repair r WHERE  r.repairId = :repairId" ;
        List<Repair> repairs = entityManager.createQuery(ehq1).setParameter( "repairId",repairId).getResultList() ;
        if (repairs != null && repairs.size() > 0){

            repair = repairs.get(0) ;
        }

        return repair ;

    }

    @Override
    public Repair getRepairByName (String repairName) {

        Repair repair = null ;
        String ehq1 = "FROM Repair r WHERE  r.repairName = :repairName" ;
        List<Repair> repairs = entityManager.createQuery(ehq1).setParameter( "repairName",repairName).getResultList() ;
        if (repairs != null && repairs.size() > 0){

            repair = repairs.get(0) ;
        }

        return repair ;

    }

    @Override
    public boolean saveRepair (Repair repair) {

        Repair persistedRepair = null ;
        entityManager.persist(repair) ;
        persistedRepair = getRepairbyId(repair.getRepairId()) ;
        if (persistedRepair != null){

            return true;
        }
        return false;

    }

    @Override
    public List<Repair> getAllRepairs (){

        List<Repair> repairs = entityManager.createQuery("select repair from Repair repair").getResultList();
        return repairs;
    }

    @Override
    public Repair getRepairByCode (String repairCode){
        List<Repair> repairs = entityManager.createQuery("select repair from Repair repair where repair.repairCode = :repairCode").
                setParameter("repairCode", repairCode).getResultList();
        if(repairs != null && !repairs.isEmpty()){
            return repairs.get(0);
        }
        return  null;
    }

    @Override
    public boolean updateRepair(Repair repair) {
        Repair persistedRepair = null;
        entityManager.merge(repair);
        persistedRepair = getRepairByCode(repair.getRepairCode());
        if(persistedRepair != null){
            return  true;
        }
        return false;
    }

    /*@Override
    public boolean deleteRepair(Repair repair) {
        try{
            entityManager.remove(repair);
        }catch (Exception e){
            return false;
        }

        return true;
    }*/

}
