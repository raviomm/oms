package com.oms.repair.dao;

import com.oms.repair.model.Repair;

import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 11/11/2019.
 */
public interface IRepairDAO {

    boolean saveRepair (Repair repair);
    Repair getRepairbyId (Integer repairId);
    List<Repair> getAllRepairs ();
    Repair getRepairByName (String repairName);
    Repair getRepairByCode (String repairCode);
    boolean updateRepair(Repair repair);
    /*boolean deleteRepair(Repair repair);*/

}
