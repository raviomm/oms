package com.oms.repair.adaptor;

import com.oms.common.adaptor.BasicAdaptor;
import com.oms.repair.dto.RepairDTO;
import com.oms.repair.model.Repair;

/**
 * Created by RAVI KALUARACHCHI on 11/11/2019.
 */
public class RepairAdaptor extends BasicAdaptor<Repair, RepairDTO>{


    @Override
    public Repair toModel(RepairDTO dto) {
        Repair repair = new  Repair();

        repair.setRepairId(dto.getRepairId());
        repair.setRepairName(dto.getRepairName());
        repair.setRepairCode(dto.getRepairCode());
        repair.setRepairType(dto.getRepairType());
        repair.setRepairCharge(dto.getRepairCharge());
        repair.setRepairDescription(dto.getRepairDescription());

        return repair;

    }

    @Override
    public RepairDTO toDTO(Repair model) {

        RepairDTO dto = new RepairDTO();

        dto.setRepairId(model.getRepairId());
        dto.setRepairName(model.getRepairName());
        dto.setRepairCode(model.getRepairCode());
        dto.setRepairType(model.getRepairType());
        dto.setRepairCharge(model.getRepairCharge());
        dto.setRepairDescription(model.getRepairDescription());

        return dto;

    }
}
