package com.oms.employee.services;

import com.oms.employee.dto.EmployeeDTO;
import com.oms.employee.model.Employee;

import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 11/30/2017.
 */
public interface IEmployeeService {

    List<EmployeeDTO> getAllEmployees();
    EmployeeDTO getEmployeeByNIC(String EmployeeNic);
    boolean saveEmployee(EmployeeDTO employeeDTO);
    boolean updateEmployee(EmployeeDTO employeeDTO);
    boolean deleteEmployee(EmployeeDTO employeeDTO);
    List<EmployeeDTO> getAllTechnicians();


    List<EmployeeDTO> getAllHelpers();

    Employee getEmployeeByID(Integer id);
    List<EmployeeDTO> getAllActiveEmployees();
}
