package com.oms.employee.services;

import com.oms.employee.adaptor.EmployeeAdaptor;
import com.oms.employee.dao.IEmployeeDAO;
import com.oms.employee.dto.EmployeeDTO;
import com.oms.employee.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 11/30/2017.
 */
@Service
public class EmployeeService implements IEmployeeService {

    @Autowired
    private IEmployeeDAO employeeDAO;

    @Override
    public List<EmployeeDTO> getAllEmployees() {

        EmployeeAdaptor employeeAdaptor = new EmployeeAdaptor();
        List<EmployeeDTO> dtoList = new ArrayList<>();
        List<Employee> employeesList = employeeDAO.getAllEmployees();
        if (employeesList != null & !employeesList.isEmpty()) {

            for (Employee employee : employeesList) {
                dtoList.add(employeeAdaptor.toDTO(employee));

            }
        }

        return dtoList;

    }



    @Override
    public List<EmployeeDTO> getAllTechnicians(){

        EmployeeAdaptor technicianAdaptor = new EmployeeAdaptor();
        List<EmployeeDTO> dtoTechList = new ArrayList<>();
        List<Employee> technicianList = employeeDAO.getAllTechnicians();

    if(technicianList != null & !technicianList.isEmpty()){
        for( Employee employee : technicianList )
        {
            dtoTechList.add(technicianAdaptor.toDTO(employee));

        }
    }
       return dtoTechList;
    }

    @Override
    public List<EmployeeDTO> getAllHelpers(){

        EmployeeAdaptor helperAdaptor = new EmployeeAdaptor();
        List<EmployeeDTO> dtoHelList = new ArrayList<>();
        List<Employee> helpersList = employeeDAO.getAllHelpers();

        if(helpersList != null & !helpersList.isEmpty()){
            for( Employee employee : helpersList )
            {
                dtoHelList.add(helperAdaptor.toDTO(employee));

            }
        }
        return dtoHelList;
    }

    @Override
    public Employee getEmployeeByID(Integer id) {
        Employee emp = employeeDAO.getEmployeeById(id);
        return emp;
    }


    @Override
    public EmployeeDTO getEmployeeByNIC (String EmployeeNic) {

        EmployeeAdaptor adaptor = new EmployeeAdaptor();
        Employee employee = employeeDAO.getEmployeeByNumber(EmployeeNic);
        if(employee != null){
            return adaptor.toDTO(employee);

        }
        else{
            return null;

        }


    }


    public boolean saveEmployee(EmployeeDTO employeeDTO){

        EmployeeAdaptor adaptor = new EmployeeAdaptor();
        Employee employee = adaptor.toModel(employeeDTO);
//        employee.setCreatedTime(new Date());
//        employee.setModifiedTime(new Date());
//        employee.setVersion(0);
        return employeeDAO.saveEmployee(employee);

    }


    @Override
    public boolean updateEmployee (EmployeeDTO employeeDTO){
        EmployeeAdaptor adaptor = new EmployeeAdaptor();
        Employee employee = adaptor.toModel(employeeDTO);
            Employee existingEmployee = employeeDAO.getEmployeeById(employee.getEmpId());
            if (existingEmployee != null){
//                existingEmployee.setModifiedTime(new Date());
//                existingEmployee.setVersion(existingEmployee.getVersion()+1);
                existingEmployee.setEmployeeBasicsalary(employee.getEmployeeBasicsalary());
                existingEmployee.setEmployeeAllowance(employee.getEmployeeAllowance());
                existingEmployee.setEmployeeEmail(employee.getEmployeeEmail());
                existingEmployee.setEmployeeMobile(employee.getEmployeeMobile());
                existingEmployee.setEmployeeAddress(employee.getEmployeeAddress());
                existingEmployee.setEmployeeDesignation(employee.getEmployeeDesignation());
                existingEmployee.setStatus(employee.getStatus());
                return employeeDAO.updateEmployee(existingEmployee);

            } else {
                return false;
            }

    }

    @Override
    public boolean deleteEmployee (EmployeeDTO employeeDTO){
        EmployeeAdaptor adaptor = new EmployeeAdaptor();
        Employee employee = adaptor.toModel(employeeDTO);
        Employee existingEmployee = employeeDAO.getEmployeeById(employee.getEmpId());
        if (existingEmployee !=null){
            return employeeDAO.deleteEmployee(existingEmployee);
        }else{
            return false;
        }

    }

    // for active employee report
    @Override
    public List<EmployeeDTO> getAllActiveEmployees() {

        EmployeeAdaptor employeeAdaptor = new EmployeeAdaptor();
        List<EmployeeDTO> dtoList = new ArrayList<>();
        List<Employee> employeesList = employeeDAO.getAllActiveEmployees();
        if (employeesList != null & !employeesList.isEmpty()) {

            for (Employee employee : employeesList) {
                dtoList.add(employeeAdaptor.toDTO(employee));

            }
        }

        return dtoList;

    }



}