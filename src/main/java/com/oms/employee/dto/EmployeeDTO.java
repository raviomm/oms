package com.oms.employee.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by RAVI KALUARACHCHI on 11/29/2017.
 */
public class EmployeeDTO implements Serializable {

    private static final long serialVersionUID = 7526472295222276148L;



    private Integer employeeId ;
    private String employeeFirstName ;
    private String employeeLastName ;
    private String employeeNIC ;
    private String employeeEPF;
    private String employeeAddress ;
    private int employeeMobileNumber ;
    private String employeeEmail ;
    private BigDecimal employeeBasicSalary ;
    private BigDecimal employeeAllowance ;
    private String employeeDesignation ;
    private String employeeStatus ;


    public static long getSerialVersionUID() { return serialVersionUID; }

    public Integer getEmployeeId() { return employeeId; }

    public String getEmployeeFirstName() {
        return employeeFirstName;
    }

    public String getEmployeeLastName() {
        return employeeLastName;
    }

    public String getEmployeeNIC() {
        return employeeNIC;
    }

    public String getEmployeeEPF() {
        return employeeEPF;
    }

    public String getEmployeeAddress() {
        return employeeAddress;
    }

    public int getEmployeeMobileNumber() {
        return employeeMobileNumber;
    }

    public String getEmployeeEmail() {
        return employeeEmail;
    }

    public BigDecimal getEmployeeBasicSalary() {
        return employeeBasicSalary;
    }

    public BigDecimal getEmployeeAllowance() {
        return employeeAllowance;
    }

    public String getEmployeeDesignation() {
        return employeeDesignation;
    }

    public String getEmployeeStatus() {
        return employeeStatus;
    }

    public void setEmployeeId(Integer employeeId) { this.employeeId = employeeId; }

    public void setEmployeeFirstName(String employeeFirstName) {
        this.employeeFirstName = employeeFirstName;
    }

    public void setEmployeeLastName(String employeeLastName) {
        this.employeeLastName = employeeLastName;
    }

    public void setEmployeeNIC(String employeeNIC) {
        this.employeeNIC = employeeNIC;
    }

    public void setEmployeeEPF(String employeeEPF) {
        this.employeeEPF = employeeEPF;
    }

    public void setEmployeeAddress(String employeeAddress) {
        this.employeeAddress = employeeAddress;
    }

    public void setEmployeeMobileNumber(int employeeMobileNumber) {
        this.employeeMobileNumber = employeeMobileNumber;
    }

    public void setEmployeeEmail(String employeeEmail) {
        this.employeeEmail = employeeEmail;
    }

    public void setEmployeeBasicSalary(BigDecimal employeeBasicSalary) { this.employeeBasicSalary = employeeBasicSalary; }

    public void setEmployeeAllowance(BigDecimal employeeAllowance) {
        this.employeeAllowance = employeeAllowance;
    }

    public void setEmployeeDesignation(String employeeDesignation) {
        this.employeeDesignation = employeeDesignation;
    }

    public void setEmployeeStatus(String employeeStatus) {
        this.employeeStatus = employeeStatus;
    }


}
