package com.oms.employee.adaptor;

import com.oms.common.adaptor.BasicAdaptor;
import com.oms.employee.dto.EmployeeDTO;
import com.oms.employee.model.Employee;

/**
 * Created by RAVI KALUARACHCHI on 11/30/2017.
 */
public class EmployeeAdaptor extends BasicAdaptor <Employee, EmployeeDTO> {

    @Override
    public Employee toModel (EmployeeDTO dto){

        Employee employee = new Employee();
        employee.setEmpId(dto.getEmployeeId());
        employee.setFirstName(dto.getEmployeeFirstName());
        employee.setEmployeeLastName(dto.getEmployeeLastName());
        employee.setEmployeeNIC(dto.getEmployeeNIC());
        employee.setEpfNo(dto.getEmployeeEPF());
        employee.setEmployeeAddress(dto.getEmployeeAddress());
        employee.setEmployeeEmail(dto.getEmployeeEmail());
        employee.setEmployeeMobile(dto.getEmployeeMobileNumber());
        employee.setEmployeeBasicsalary(dto.getEmployeeBasicSalary());
        employee.setEmployeeAllowance(dto.getEmployeeAllowance());
        employee.setEmployeeDesignation(dto.getEmployeeDesignation());
        employee.setStatus(dto.getEmployeeStatus());

        return employee;
    }



    public EmployeeDTO toDTO(Employee model){

        EmployeeDTO dto = new EmployeeDTO();

        dto.setEmployeeId(model.getEmpId());
        dto.setEmployeeFirstName(model.getFirstName());
        dto.setEmployeeLastName(model.getEmployeeLastName());
        dto.setEmployeeNIC(model.getEmployeeNIC());
        dto.setEmployeeEPF(model.getEpfNo());
        dto.setEmployeeAddress(model.getEmployeeAddress());
        dto.setEmployeeEmail(model.getEmployeeEmail());
        dto.setEmployeeMobileNumber(model.getEmployeeMobile());
        dto.setEmployeeBasicSalary(model.getEmployeeBasicsalary());
        dto.setEmployeeAllowance(model.getEmployeeAllowance());
        dto.setEmployeeDesignation(model.getEmployeeDesignation());
        dto.setEmployeeStatus(model.getStatus());

        return dto;


    }
}
