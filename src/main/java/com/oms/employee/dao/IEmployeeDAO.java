package com.oms.employee.dao;

import com.oms.employee.model.Employee;

import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 11/30/2017.
 */
public interface IEmployeeDAO {

    List <Employee> getAllEmployees();

    Employee getEmployeeByNumber (String employeeNumber) ;

    Employee getEmployeeByName (String employeeName) ;

    List <String> getAllEmployeeDesignation ();

    boolean saveEmployee (Employee employee) ;

    Employee getEmployeeById (Integer employeeId) ;

    boolean updateEmployee (Employee employee) ;

    boolean deleteEmployee (Employee employee) ;

    List <Employee> getAllTechnicians ();

    List <Employee> getAllHelpers ();

    Employee getEmployeeSalaryDetails (String employeenic);

    List<Employee> getAllActiveEmployees();
    Employee getEmployeeByNIC (String employeeNIC);

}
