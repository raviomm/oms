package com.oms.reporting.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class ReportServiceDTO implements Serializable {

    private String serviceName;
    private BigDecimal amount;

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
