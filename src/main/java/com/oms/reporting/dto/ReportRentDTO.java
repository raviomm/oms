package com.oms.reporting.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class ReportRentDTO implements Serializable {

    private String rentName;
    private BigDecimal amount;

    public String getRentName() {
        return rentName;
    }

    public void setRentName(String rentName) {
        this.rentName = rentName;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
