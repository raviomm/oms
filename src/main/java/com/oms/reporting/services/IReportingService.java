package com.oms.reporting.services;

import com.oms.employee.model.Employee;
import com.oms.job.dto.JobSlipDTO;
import com.oms.job.dto.SalaryDTO;

import java.math.BigDecimal;

/**
 * Created by hp on 12/15/2017.
 */
public interface IReportingService {

    public String generateSalarySlip(SalaryDTO salaryDTO, String year, String month);
    String generateJobSlip(JobSlipDTO jobSlipDTO);
}
