package com.oms.reporting.services;
import com.oms.employee.model.Employee;
import com.oms.job.dto.JobSlipDTO;
import com.oms.job.dto.SalaryDTO;
import com.oms.reporting.dto.ReportItemDTO;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.data.JRMapArrayDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.export.SimplePdfReportConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.*;

/**
 * Created by hp on 12/15/2017.
 */
@Service
public class ReportingService implements IReportingService {

    @Autowired
    private ResourceLoader resourceLoader;

    @Override
    public String generateSalarySlip(SalaryDTO salaryDTO, String year, String month) {

        String base64String = "";

        try {
            Resource resource  = resourceLoader.getResource("file:F:\\Operation Management System\\oms\\src\\main\\java\\com\\oms\\reporting\\resources\\employeeReport.jrxml");

            InputStream employeeReportStream = resource.getInputStream();
            JasperReport jasperReport
                    = JasperCompileManager.compileReport(employeeReportStream);
            Map<String, Object> parameters = new HashMap<>();
            parameters.put("employeeName",salaryDTO.getEmployee().getFirstName());
            parameters.put("amount", salaryDTO.getTotalSalary());
            parameters.put("epfNo", salaryDTO.getEmployee().getEpfNo());
            parameters.put("address", salaryDTO.getEmployee().getEmployeeAddress());
            parameters.put("month", month);
            parameters.put("year",year);
            parameters.put("employeeNIC", salaryDTO.getEmployee().getEmployeeNIC());
            parameters.put("basicSalary", salaryDTO.getEmployee().getEmployeeBasicsalary());
            parameters.put("allowance", salaryDTO.getAttendenceAllowance());
            parameters.put("noOfRepairs", salaryDTO.getNoOfJobs().toString());
            parameters.put("eligibleRepairTotal", salaryDTO.getEligibleRepairAmount().toString());




            JasperPrint jasperPrint
                    = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource(1) );
            ByteArrayOutputStream outStream = new ByteArrayOutputStream();

            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            exporter.setExporterOutput(
                    new SimpleOutputStreamExporterOutput(outStream));

            SimplePdfReportConfiguration reportConfig
                    = new SimplePdfReportConfiguration();
            reportConfig.setSizePageToContent(true);
            reportConfig.setForceLineBreakPolicy(false);

            SimplePdfExporterConfiguration exportConfig
                    = new SimplePdfExporterConfiguration();
            exportConfig.setMetadataAuthor("baeldung");
            exportConfig.setEncrypted(true);
            exportConfig.setAllowedPermissionsHint("PRINTING");

            exporter.setConfiguration(reportConfig);
            exporter.setConfiguration(exportConfig);

            exporter.exportReport();
            base64String = Base64.getEncoder().encodeToString(outStream.toByteArray());
        } catch (JRException je) {
            je.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return base64String;

    }

    @Override
    public String generateJobSlip(JobSlipDTO jobSlipDTO) {

        String base64String = "";

        JRBeanCollectionDataSource itemDatasource = new JRBeanCollectionDataSource(jobSlipDTO.getItemList(), false);
        JRBeanCollectionDataSource rentDatasource = new JRBeanCollectionDataSource(jobSlipDTO.getRentList(), false);
        JRBeanCollectionDataSource repairDatasource = new JRBeanCollectionDataSource(jobSlipDTO.getServiceList(), false);
        Map<String, Object> dataSouces =  new HashMap<>();
        dataSouces.put("jobItemDataSource", itemDatasource);
        dataSouces.put("jobRepairDataSource", repairDatasource);
        dataSouces.put("jobRentDataSource", rentDatasource);
        JRMapArrayDataSource dataSource = new JRMapArrayDataSource(new Object[]{dataSouces});

        try {
            Resource resource  = resourceLoader.getResource("file:F:\\Operation Management System\\oms\\src\\main\\java\\com\\oms\\reporting\\resources\\jobReport.jrxml");

            InputStream jobReportStream = resource.getInputStream();
            JasperReport jasperReport
                    = JasperCompileManager.compileReport(jobReportStream);
            Map<String, Object> parameters = new HashMap<>();
            parameters.put("jobDate",jobSlipDTO.getJobDate());
            parameters.put("customerName",jobSlipDTO.getCustomerLastName());
            parameters.put("customerContact", jobSlipDTO.getCustomerContactNumber());
            parameters.put("employeeName", jobSlipDTO.getEmployeeName());
            parameters.put("totalPrice", jobSlipDTO.getTotalPrice());
            parameters.put("jobId", jobSlipDTO.getJobId());
            /*parameters.put("address", salaryDTO.getEmployee().getEmployeeAddress());
            parameters.put("month", month);
            parameters.put("year",year);
            parameters.put("employeeNIC", salaryDTO.getEmployee().getEmployeeNIC());
            parameters.put("basicSalary", salaryDTO.getEmployee().getEmployeeBasicsalary());
            parameters.put("allowance", salaryDTO.getAttendenceAllowance());
            parameters.put("noOfRepairs", salaryDTO.getNoOfJobs().toString());
            parameters.put("eligibleRepairTotal", salaryDTO.getEligibleRepairAmount().toString());*/

            JasperPrint jasperPrint
                    = JasperFillManager.fillReport(jasperReport, parameters, dataSource );
            ByteArrayOutputStream outStream = new ByteArrayOutputStream();

            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            exporter.setExporterOutput(
                    new SimpleOutputStreamExporterOutput(outStream));

            SimplePdfReportConfiguration reportConfig
                    = new SimplePdfReportConfiguration();
            reportConfig.setSizePageToContent(true);
            reportConfig.setForceLineBreakPolicy(false);

            SimplePdfExporterConfiguration exportConfig
                    = new SimplePdfExporterConfiguration();
            exportConfig.setMetadataAuthor("baeldung");
            exportConfig.setEncrypted(true);
            exportConfig.setAllowedPermissionsHint("PRINTING");

            exporter.setConfiguration(reportConfig);
            exporter.setConfiguration(exportConfig);

            exporter.exportReport();
            base64String = Base64.getEncoder().encodeToString(outStream.toByteArray());
        } catch (JRException je) {
            je.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return base64String;

    }
}
