package com.oms.invoice.adaptor;

import com.oms.common.adaptor.BasicAdaptor;
import com.oms.invoice.dto.InvoiceDTO;
import com.oms.invoice.model.Invoice;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by RAVI KALUARACHCHI on 11/14/2019.
 */
public class InvoiceAdaptor extends BasicAdaptor<Invoice,InvoiceDTO>{


    @Override
    public Invoice toModel(InvoiceDTO dto) {
        Invoice invoice = new Invoice();

        invoice.setInvoiceId(dto.getInvoiceId());


        Date date1= null;
        try {
            date1 = new SimpleDateFormat("yyyy-MM-dd").parse(dto.getInvoiceDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        invoice.setInvoiceDate(date1);
        invoice.setInvoiceNumber(dto.getInvoiceNumber());
        invoice.setVendorName(dto.getVendorName());
        invoice.setVendorAddress(dto.getVendorAddress());
        invoice.setVendorContactNumber(dto.getVendorContactNumber());

        return invoice;
    }

    @Override
    public InvoiceDTO toDTO(Invoice model) {
        InvoiceDTO dto = new InvoiceDTO();

        dto.setInvoiceId(model.getInvoiceId());
        dto.setInvoiceDate(model.getInvoiceDate().toString());
        dto.setInvoiceNumber(model.getInvoiceNumber());
        dto.setVendorName(model.getVendorName());
        dto.setVendorAddress(model.getVendorAddress());
        dto.setVendorContactNumber(model.getVendorContactNumber());

        return dto;
    }
}
