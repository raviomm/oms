package com.oms.invoice.dao;

import com.oms.invoice.model.Invoice;
import com.oms.invoice.model.InvoiceItem;

import java.util.Date;
import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 11/14/2019.
 */
public interface IInvoiceDAO {

    Invoice getInvoicebyId (Integer invoiceId);
    boolean saveInvoice (Invoice invoice);
    boolean saveInvoiceItem(InvoiceItem invoiceItem);
    InvoiceItem getInvoiceItembyId (Integer invoiceItemId);
    List<String> getInvoiceByMonth(Date startDate, Date endDate);
    List<Invoice> getAllInvoices();
    Invoice getInvoiceByNumber (String invoiceNumber);
}
