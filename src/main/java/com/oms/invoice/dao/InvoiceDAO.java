package com.oms.invoice.dao;

import com.oms.invoice.model.Invoice;
import com.oms.invoice.model.InvoiceItem;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 11/14/2019.
 */
@Transactional
@Repository
public class InvoiceDAO implements IInvoiceDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Invoice getInvoicebyId (Integer invoiceId) {

        Invoice invoice = null ;
        String ehq1 = "FROM Invoice i WHERE  i.invoiceId = :invoiceId" ;
        List<Invoice> invoices = entityManager.createQuery(ehq1).setParameter( "invoiceId",invoiceId).getResultList() ;
        if (invoices != null && invoices.size() > 0){

            invoice = invoices.get(0) ;
        }

        return invoice ;

    }

    @Override
    public Invoice getInvoiceByNumber (String invoiceNumber) {

        Invoice invoice = null ;
        String ehq1 = "FROM Invoice i WHERE  i.invoiceNubmer = :invoiceNumber" ;
        List<Invoice> invoices = entityManager.createQuery(ehq1).setParameter( "invoiceNumber",invoiceNumber).getResultList() ;
        if (invoices != null && invoices.size() > 0){

            invoice = invoices.get(0) ;
        }

        return invoice ;

    }

    @Override
    public List<Invoice> getAllInvoices(){

        String hql = "FROM Invoice as i ORDER BY i.invoiceId";
        return (List<Invoice>) entityManager.createQuery(hql).getResultList();


    }

    public InvoiceItem getInvoiceItembyId (Integer invoiceItemId) {

        InvoiceItem invoiceItem = null ;
        String ehq1 = "FROM InvoiceItem i WHERE  i.invoiceItemId = :invoiceItemId" ;
        List<InvoiceItem> invoiceItems = entityManager.createQuery(ehq1).setParameter( "invoiceItemId",invoiceItemId).getResultList() ;
        if (invoiceItems != null && invoiceItems.size() > 0){

            invoiceItem = invoiceItems.get(0) ;
        }

        return invoiceItem ;

    }



    @Override
    public boolean saveInvoice (Invoice invoice) {

        Invoice persistedInvoice = null ;
        entityManager.persist(invoice) ;
        persistedInvoice = getInvoicebyId(invoice.getInvoiceId()) ;
        if (persistedInvoice != null){

            return true;
        }
        return false;

    }

    @Override
    public boolean saveInvoiceItem(InvoiceItem invoiceItem) {
        InvoiceItem persistedInvoiceItem = null;
        entityManager.persist(invoiceItem);

        persistedInvoiceItem = getInvoiceItembyId(invoiceItem.getInvoiceItemId());
        if(persistedInvoiceItem != null){

            return true;
        }
        return false;

    }

    @Override
    public List<String> getInvoiceByMonth(Date startDate, Date endDate) {
        String hql = "SELECT i FROM InvoiceItem as i WHERE i.invoice.invoiceDate between  :startDate and :endDate  ORDER BY i.invoice.invoiceId";
        return (List<String>) entityManager.createQuery(hql).setParameter("startDate",startDate).setParameter("endDate",endDate).getResultList();
    }

}
