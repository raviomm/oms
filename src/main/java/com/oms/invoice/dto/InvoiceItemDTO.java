package com.oms.invoice.dto;

import java.io.Serializable;

public class InvoiceItemDTO implements Serializable {

    private String selectInvoiceItem;
    private Integer invoiceItemCount;

    public String getSelectInvoiceItem() {
        return selectInvoiceItem;
    }

    public void setSelectInvoiceItem(String selectInvoiceItem) {
        this.selectInvoiceItem = selectInvoiceItem;
    }

    public Integer getInvoiceItemCount() {
        return invoiceItemCount;
    }

    public void setInvoiceItemCount(Integer invoiceItemCount) {
        this.invoiceItemCount = invoiceItemCount;
    }

    /*private String selectItem;
    private Integer itemCount;

    public String getSelectItem() {
        return selectItem;
    }

    public void setSelectItem(String selectItem) {
        this.selectItem = selectItem;
    }

    public Integer getItemCount() {
        return itemCount;
    }

    public void setItemCount(Integer itemCount) {
        this.itemCount = itemCount;
    }*/
}
