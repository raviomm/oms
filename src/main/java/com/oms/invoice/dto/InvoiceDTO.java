package com.oms.invoice.dto;

import java.io.Serializable;
import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 11/14/2019.
 */
public class InvoiceDTO implements Serializable{

    private Integer invoiceId;
    private String invoiceDate;
    private String invoiceNumber;
    private String vendorName;
    private String vendorAddress;
    private String vendorContactNumber;
    private List<InvoiceItemDTO> invoiceItemList;

    public Integer getInvoiceId() { return invoiceId; }

    public void setInvoiceId(Integer invoiceId) { this.invoiceId = invoiceId; }

    public String getInvoiceDate() { return invoiceDate; }

    public void setInvoiceDate(String invoiceDate) { this.invoiceDate = invoiceDate; }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getVendorName() { return vendorName; }

    public void setVendorName(String vendorName) { this.vendorName = vendorName; }

    public String getVendorAddress() { return vendorAddress; }

    public void setVendorAddress(String vendorAddress) { this.vendorAddress = vendorAddress; }

    public String getVendorContactNumber() { return vendorContactNumber; }

    public void setVendorContactNumber(String vendorContactNumber) { this.vendorContactNumber = vendorContactNumber; }

    public List<InvoiceItemDTO> getInvoiceItemList() {
        return invoiceItemList;
    }

    public void setInvoiceItemList(List<InvoiceItemDTO> invoiceItemList) {
        this.invoiceItemList = invoiceItemList;
    }
}
