package com.oms.invoice.services;

import com.oms.invoice.adaptor.InvoiceAdaptor;
import com.oms.invoice.dao.IInvoiceDAO;
import com.oms.invoice.dto.InvoiceDTO;
import com.oms.invoice.dto.InvoiceItemDTO;
import com.oms.invoice.model.Invoice;
import com.oms.invoice.model.InvoiceItem;
import com.oms.item.dao.IItemDAO;
import com.oms.item.model.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 11/14/2019.
 */
@Service
public class InvoiceService implements IInvoiceService {

    @Autowired
    private IInvoiceDAO invoiceDAO;

    @Autowired
    private IItemDAO itemDAO;

    public String saveInvoice(InvoiceDTO invoiceDTO){

        Item item = null;
        InvoiceItem invoiceItem = null;

        if(invoiceDTO.getInvoiceItemList() == null || invoiceDTO.getInvoiceItemList().isEmpty()){
            throw new RuntimeException("Invoice List cannot be empty");
        }

        InvoiceAdaptor adaptor = new InvoiceAdaptor();
        Invoice invoice = adaptor.toModel(invoiceDTO);
        invoiceDAO.saveInvoice(invoice);

        for(InvoiceItemDTO invoiceItemDTO : invoiceDTO.getInvoiceItemList()){
            item  = itemDAO.getItemByName(invoiceItemDTO.getSelectInvoiceItem());
            invoiceItem = new InvoiceItem();
            invoiceItem.setItem(item);
            invoiceItem.setInvoice(invoice);
            invoiceItem.setQuantity(invoiceItemDTO.getInvoiceItemCount());
            invoiceDAO.saveInvoiceItem(invoiceItem);

            itemDAO.createInvoiceItemTrans(invoice, item, invoiceItemDTO.getInvoiceItemCount());
        }


        if(invoice != null && invoice.getInvoiceId() != null){
            return invoice.getInvoiceId().toString();
        }
        return  "";




    }

    @Override
    public List<InvoiceDTO> getAllInvoices() {

        InvoiceAdaptor employeeAdaptor = new InvoiceAdaptor();
        List<InvoiceDTO> dtoList = new ArrayList<>();
        List<Invoice> invoiceList = invoiceDAO.getAllInvoices();
        if (invoiceList != null & !invoiceList.isEmpty()) {

            for (Invoice invoice : invoiceList) {
                dtoList.add(employeeAdaptor.toDTO(invoice));

            }
        }

        return dtoList;

    }

    //get monthly repairs to the Reports
    @Override
    public List<String> getInvoiceByMonth(String year, String month) {

        Calendar c = Calendar.getInstance();   // this takes current date

        c.clear();
        c.set(Calendar.MONTH, Integer.parseInt(month) - 1);
        c.set(Calendar.YEAR, Integer.parseInt(year));
        c.set(Calendar.DAY_OF_MONTH,1);
        Date startDate = c.getTime();
        c.set(Calendar.DAY_OF_MONTH,30);
        Date endDate = c.getTime();

        List<String> invoiceList = invoiceDAO.getInvoiceByMonth(startDate, endDate);

        return invoiceList;

    }

    @Override
    public InvoiceDTO getInvoiceByNumber (String invoiceNuber) {

        InvoiceAdaptor adaptor = new InvoiceAdaptor();
        Invoice invoice = invoiceDAO.getInvoiceByNumber(invoiceNuber);
        if(invoice != null){
            return adaptor.toDTO(invoice);

        }
        else{
            return null;

        }


    }

}
