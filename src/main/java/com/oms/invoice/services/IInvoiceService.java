package com.oms.invoice.services;

import com.oms.invoice.dto.InvoiceDTO;

import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 11/14/2019.
 */
public interface IInvoiceService {

    String saveInvoice(InvoiceDTO invoiceDTO);
    List<String> getInvoiceByMonth(String year, String month);
    List<InvoiceDTO> getAllInvoices();
    InvoiceDTO getInvoiceByNumber (String invoiceNuber);
}
