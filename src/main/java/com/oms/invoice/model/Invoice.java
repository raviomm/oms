package com.oms.invoice.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by RAVI KALUARACHCHI on 11/14/2019.
 */
@Entity
@Table(name = "invoice")
public class Invoice {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "invoice_id")
    public Integer invoiceId;
    @Column(name = "invoice_date")
    public Date invoiceDate;
    @Column(name = "invoice_number")
    public String invoiceNumber;
    @Column(name = "vendor_name")
    public String vendorName;
    @Column(name = "vendor_address")
    public String vendorAddress;
    @Column(name = "vendor_mobile")
    public String vendorContactNumber;

    public Integer getInvoiceId() { return invoiceId; }

    public void setInvoiceId(Integer invoiceId) { this.invoiceId = invoiceId; }

    public Date getInvoiceDate() { return invoiceDate; }

    public void setInvoiceDate(Date invoiceDate) { this.invoiceDate = invoiceDate; }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getVendorName() { return vendorName; }

    public void setVendorName(String vendorName) { this.vendorName = vendorName; }

    public String getVendorAddress() { return vendorAddress; }

    public void setVendorAddress(String vendorAddress) { this.vendorAddress = vendorAddress; }

    public String getVendorContactNumber() { return vendorContactNumber; }

    public void setVendorContactNumber(String vendorContactNumber) { this.vendorContactNumber = vendorContactNumber; }
}
