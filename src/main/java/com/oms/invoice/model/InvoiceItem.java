
package com.oms.invoice.model;

import com.oms.item.model.Item;

import javax.persistence.*;


/**
 * Created by RAVI KALUARACHCHI on 11/16/2019.
 */

@Entity
@Table(name = "invoice_item")
public class InvoiceItem {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "invoice_item_id")
    private Integer invoiceItemId;
    @ManyToOne
    @JoinColumn(name = "invoice_id", referencedColumnName = "invoice_id")
    private Invoice invoice;
    @ManyToOne
    @JoinColumn(name = "item_id", referencedColumnName = "item_id")
    private Item item;
    @Column(name = "quantity")
    private Integer quantity;

    public Integer getInvoiceItemId() { return invoiceItemId; }

    public void setInvoiceItemId(Integer invoiceItemId) { this.invoiceItemId = invoiceItemId; }

    public Invoice getInvoice() { return invoice; }

    public void setInvoice(Invoice invoice) { this.invoice = invoice; }

    public Item getItem() { return item; }

    public void setItem(Item item) { this.item = item; }

    public Integer getQuantity() { return quantity; }

    public void setQuantity(Integer quantity) { this.quantity = quantity; }

}
