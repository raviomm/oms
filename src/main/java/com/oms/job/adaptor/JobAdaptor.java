package com.oms.job.adaptor;

import com.oms.common.adaptor.BasicAdaptor;
import com.oms.job.dto.JobDTO;
import com.oms.job.model.Job;

import java.text.SimpleDateFormat;

/**
 * Created by RAVI KALUARACHCHI on 11/11/2019.
 */
public class JobAdaptor extends BasicAdaptor<Job, JobDTO> {

    private static SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-mm-dd");
    @Override
    public Job toModel(JobDTO dto) {
        Job job = new Job();
        job.setCustomerName(dto.getCustomerLastName());
        job.setCusMobile(dto.getCustomerContactNumber());
        job.setCustomerNIC(dto.getCustomerNic());
        try {
            job.setJobDate(dateFormatter.parse(dto.getJobDate()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return job;
    }

    @Override
    public JobDTO toDTO(Job Model) {
        JobDTO jobDTO = new JobDTO();
        jobDTO.setId(Model.getId().toString());
        jobDTO.setCustomerLastName(Model.getCustomerName());
        jobDTO.setCustomerContactNumber(Model.getCusMobile());
        jobDTO.setCustomerNic(Model.getCustomerNIC());
        jobDTO.setJobDate(Model.getJobDate().toString());
        jobDTO.setStatus(Model.getStatus());
        jobDTO.setJobTechnician(Model.getEmployeeNIC());
        return jobDTO;
    }
}
