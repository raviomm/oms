package com.oms.job.services;

import com.oms.employee.dao.IEmployeeDAO;
import com.oms.employee.dto.EmployeeDTO;
import com.oms.employee.model.Employee;
import com.oms.employee.services.IEmployeeService;
import com.oms.item.dao.IItemDAO;
import com.oms.item.dao.ItemDAO;
import com.oms.item.model.Item;
import com.oms.item.model.ItemStock;
import com.oms.job.adaptor.JobAdaptor;
import com.oms.job.dao.IJobDAO;
import com.oms.job.dto.*;
import com.oms.job.model.Job;
import com.oms.job.model.JobItem;
import com.oms.job.model.JobRent;
import com.oms.job.model.JobRepair;
import com.oms.rent.dao.IRentDAO;
import com.oms.rent.model.Rent;
import com.oms.repair.dao.IRepairDAO;
import com.oms.repair.model.Repair;
import com.oms.reporting.dto.ReportItemDTO;
import com.oms.reporting.dto.ReportRentDTO;
import com.oms.reporting.dto.ReportServiceDTO;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.apache.coyote.Adapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 11/11/2019.
 */
@Service
public class JobService implements IJobService {

    @Autowired
    private IItemDAO itemDAO;

    @Autowired
    private IEmployeeDAO employeeDAO;

    @Autowired
    private IJobDAO jobDAO;

    @Autowired
    private IRepairDAO iRepairDAO;

    @Autowired
    private IRentDAO rentDAO;

    @Autowired
    private IEmployeeService employeeService;

    public Boolean cancelJob(JobDTO jobDTO){
        String jobId = jobDTO.getId();
        Job job = jobDAO.getJobById(Integer.parseInt(jobId));
        job.setStatus("CANCEL");
        job.setFinishDate(new Date());
        boolean result = jobDAO.updateJob(job);

        List<JobItem> jobItemList = jobDAO.getJobItem(job);

        if(jobItemList!=null && !jobItemList.isEmpty()) {
            for (JobItem jobItem: jobItemList) {
                Item item = itemDAO.getItemByName(jobItem.getItem().getItemName());
                Integer quantity = jobItem.getCount();
                itemDAO.jobCancelItemTrans(item, quantity);

            }
        }

        return result;
    }

    public String saveJob(JobDTO jobDTO){
        JobAdaptor adaptor = new JobAdaptor();
        Job job = adaptor.toModel(jobDTO);
        Employee employee = employeeDAO.getEmployeeById(Integer.parseInt(jobDTO.getJobTechnician()));
        job.setTechnician(employee);
        job.setEmployeeNIC(employee.getEmployeeNIC());
        job.setStatus("STARTED");
        job = jobDAO.saveJob(job);

        if(jobDTO != null && jobDTO.getItemList() != null && !jobDTO.getItemList().isEmpty()){
            for(JobItemDTO jobItemDto : jobDTO.getItemList()){
                Item item = itemDAO.getItemByName(jobItemDto.getSelectItem());
                JobItem jobItem = new JobItem();
                jobItem.setCount(jobItemDto.getItemCount());
                jobItem.setJob(job);
                jobItem.setItem(item);
                jobDAO.saveJobItem(jobItem);

                itemDAO.createJobItemTrans(job, item, jobItemDto.getItemCount());
            }
        }

        if(jobDTO != null && jobDTO.getAddedServices() != null && !jobDTO.getAddedServices().isEmpty()){
            for(JobRepairDTO service : jobDTO.getAddedServices()){
                Repair repair =  iRepairDAO.getRepairByName(service.getSelectRepair());
                JobRepair jobRepair = new JobRepair();
                jobRepair.setJob(job);
                jobRepair.setRepair(repair);
                jobDAO.saveJobRepair(jobRepair);
            }
        }

        if(jobDTO != null && jobDTO.getAddedRent() != null && !jobDTO.getAddedRent().isEmpty()){
            for(JobRentDTO rentDTO : jobDTO.getAddedRent()){
                Rent rent =  rentDAO.getRentByCode(rentDTO.getSelectRent());
                JobRent jobRent = new JobRent();
                jobRent.setJob(job);
                jobRent.setRent(rent);
                jobDAO.saveJobRent(jobRent);
            }
        }
        if(job != null && job.getId() != null){
            return job.getId().toString();
        }
        return  "";
    }

    public Boolean completeJob(Integer jobID){
        Job job = jobDAO.getJobById(jobID);
        job.setStatus("COMPLETE");
        job.setFinishDate(new Date());
        boolean result = jobDAO.updateJob(job);
        return result;
    }



    @Override
    public List<JobDTO> getAllJobs() {
        List<JobDTO> jobDTOList = new ArrayList<JobDTO>();
        JobAdaptor adaptor = new JobAdaptor();
        List<Job> jobList = jobDAO.getAllJobs();
        if(jobList != null && !jobList.isEmpty()){
            for (Job job : jobList){
                JobDTO jobDTO = new JobDTO();
                jobDTO = adaptor.toDTO(job);
                jobDTOList.add(jobDTO);
            }

        }
        return jobDTOList;
    }

    @Override
    public List<JobDTO> getAllStartedJobs(){

        JobAdaptor adaptor = new JobAdaptor();

        List<Job> allStartedJobs = jobDAO.getAllStartedJobs();
        List<JobDTO> allStartedJobDTOs = new ArrayList<>();

        if(allStartedJobs !=null && !allStartedJobs.isEmpty()){
            for (Job startedJob: allStartedJobs) {
                JobDTO startedJobDTO = new JobDTO();
                startedJobDTO = adaptor.toDTO(startedJob);
                allStartedJobDTOs.add(startedJobDTO);
            }
        }

        return  allStartedJobDTOs;

    }

    public SalaryDTO calculateJobSalary(Integer employeeId, String year, String month, Integer attendence){

        BigDecimal repairTotal = BigDecimal.ZERO;
        BigDecimal eligibleRepairAmount = BigDecimal.ZERO;
        Integer noOfJobs = 0;
        SalaryDTO salaryDTO = new SalaryDTO();

        Employee emp = employeeDAO.getEmployeeById(employeeId);
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, Integer.parseInt(year));
        cal.set(Calendar.MONTH, Integer.parseInt(month) -1);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        Date monthStartDate = cal.getTime();

        if(month.equals("12")){
            cal.set(Calendar.YEAR, Integer.parseInt(year) + 1);
            cal.set(Calendar.MONTH, 0);
        } else {
            cal.set(Calendar.MONTH, Integer.parseInt(month));
        }

        Date monthEndDate = cal.getTime();

        List<Job> jobList = jobDAO.getEmployeeJobWithDates(emp, monthStartDate, monthEndDate);
        if(jobList != null && !jobList.isEmpty()){
            noOfJobs = jobList.size();
            for(Job job: jobList){
                List<JobRepair> jobRepairs = jobDAO.getJobRepair(job);
                for(JobRepair jobRepair : jobRepairs){
                    repairTotal = repairTotal.add(BigDecimal.valueOf(jobRepair.getRepair().getRepairCharge()));
                }
            }

            if(repairTotal.compareTo(BigDecimal.ZERO) > 0){
                if(emp.getEmployeeDesignation().equals("INTERNAL")){
                    eligibleRepairAmount = repairTotal.multiply(new BigDecimal(.25));
                } else {
                    eligibleRepairAmount = repairTotal.multiply(new BigDecimal(.6));
                }
            }
        }

        BigDecimal basicSalary = emp.getEmployeeBasicsalary();
        BigDecimal attendenceAllowance = emp.getEmployeeAllowance();
        if(attendence == 25){
            attendenceAllowance = attendenceAllowance;
        } else if(16 <= attendence && attendence < 25){
            attendenceAllowance = attendenceAllowance.multiply(new BigDecimal(0.5));
        } else {
            attendenceAllowance = BigDecimal.ZERO;
        }

        salaryDTO.setAttendenceAllowance(attendenceAllowance);
        salaryDTO.setBasicSalary(basicSalary);
        salaryDTO.setEligibleRepairAmount(eligibleRepairAmount);
        salaryDTO.setNoOfJobs(noOfJobs);
        salaryDTO.setRepairTotal(repairTotal);
        salaryDTO.setTotalSalary(attendenceAllowance.add(basicSalary).add(eligibleRepairAmount));
        salaryDTO.setEmployee(emp);

        return salaryDTO;
    }


//job slip method
    public JobSlipDTO calculateJobPrice(Integer id){

        List<ReportItemDTO> itemList = new ArrayList<>();
        List<ReportRentDTO> rentList = new ArrayList<>();
        List<ReportServiceDTO> serviceList = new ArrayList<>();
        Job job = jobDAO.getJobById(id);
        Employee employee = employeeDAO.getEmployeeByNIC(job.getTechnician().getEmployeeNIC());
        Date jobDate = job.getJobDate();
        String customerName = job.getCustomerName();
        String cusContactNum = job.getCusMobile();
        JobSlipDTO jobSlipDTO = new JobSlipDTO();


        List<JobItem> itemListAll = jobDAO.getJobItem(job);
        if(itemListAll != null && !itemListAll.isEmpty()){
            for(JobItem jobItem : itemListAll){
                ReportItemDTO reportItemDTO = new ReportItemDTO();
                reportItemDTO.setItemName(jobItem.getItem().itemName);
                reportItemDTO.setCount(jobItem.getCount());
                reportItemDTO.setAmount(new BigDecimal(jobItem.getItem().getSellPrice()).multiply(new BigDecimal(jobItem.getCount())).setScale(2, RoundingMode.FLOOR));
                itemList.add(reportItemDTO);
            }
        }

        List<JobRepair> repairListAll = jobDAO.getJobRepair(job);
        if(repairListAll != null && !repairListAll.isEmpty()){
            for(JobRepair jobRepair : repairListAll){
                ReportServiceDTO reportServiceDTO = new ReportServiceDTO();
                reportServiceDTO.setServiceName(jobRepair.getRepair().getRepairName());
                reportServiceDTO.setAmount(new BigDecimal(jobRepair.getRepair().getRepairCharge()).setScale(2, RoundingMode.FLOOR));
                serviceList.add(reportServiceDTO);
            }
        }

        List<JobRent> rentListAll = jobDAO.getJobRent(job);
        if(rentListAll != null && !rentListAll.isEmpty()){
            for(JobRent jobRent : rentListAll){
                ReportRentDTO reportRentDTO = new ReportRentDTO();
                reportRentDTO.setRentName(jobRent.getRent().getRentResource());
                reportRentDTO.setAmount(new BigDecimal(jobRent.getRent().getRentCharge()).setScale(2, RoundingMode.FLOOR));
                rentList.add(reportRentDTO);
            }
        }
        /*JRBeanCollectionDataSource itemJRBean = new JRBeanCollectionDataSource(itemListAll);
        JRBeanCollectionDataSource repairJRBean = new JRBeanCollectionDataSource(rentListAll);
        JRBeanCollectionDataSource rentJRBean = new JRBeanCollectionDataSource(repairListAll);*/

        BigDecimal itemSubTotal = BigDecimal.ZERO;
        BigDecimal repairSubTotal = BigDecimal.ZERO;
        BigDecimal rentSubTotal = BigDecimal.ZERO;

        

        if(itemListAll!=null && !itemListAll.isEmpty()){
            for(JobItem jobItem: itemListAll){
                itemSubTotal = itemSubTotal.add(BigDecimal.valueOf((jobItem.getItem().getSellPrice())*(jobItem.getItem().getCount())));
            }
        }

        if(repairListAll!=null && !repairListAll.isEmpty()){
            for(JobRepair jobRepair: repairListAll){
                repairSubTotal = repairSubTotal.add(BigDecimal.valueOf((jobRepair.getRepair().getRepairCharge())));
            }
        }

        if(rentListAll!=null && !rentListAll.isEmpty()){
            for(JobRent jobRent: rentListAll){
                rentSubTotal = rentSubTotal.add(BigDecimal.valueOf((jobRent.getRent().getRentCharge())));
            }
        }

        jobSlipDTO.setJobId(id);
        jobSlipDTO.setJobDate(jobDate);
        jobSlipDTO.setCustomerLastName(customerName);
        jobSlipDTO.setCustomerContactNumber(cusContactNum);
        jobSlipDTO.setEmployeeName(employee.getEmployeeLastName());

        /*JRBeanCollectionDataSource itemListDataSource = new JRBeanCollectionDataSource(itemListAll, false);
        jobSlipDTO.setItemListDataSource(itemListDataSource);

        JRBeanCollectionDataSource addedServicesDataSource = new JRBeanCollectionDataSource(repairListAll, false);
        jobSlipDTO.setAddedServicesDataSource(addedServicesDataSource);

        JRBeanCollectionDataSource addedRentDataSource = new JRBeanCollectionDataSource(rentListAll, false);
        jobSlipDTO.setAddedRentDataSource(addedRentDataSource);*/

        jobSlipDTO.setTotalPrice(itemSubTotal.add(repairSubTotal).add(rentSubTotal));
        jobSlipDTO.setStatus("COMPLETED");
        jobSlipDTO.setItemList(itemList);
        jobSlipDTO.setRentList(rentList);
        jobSlipDTO.setServiceList(serviceList);
        jobSlipDTO.setRepairTotal(repairSubTotal);
        jobSlipDTO.setRentTotal(rentSubTotal);
        jobSlipDTO.setItemTotal(itemSubTotal);
        return jobSlipDTO;


    }


    //get monthly jobs to the Reports
    @Override
    public List<JobDTO> getJobByDate(String year, String month) {
        Calendar c = Calendar.getInstance();   // this takes current date
        List<JobDTO> jobDTOList = new ArrayList<>();
        JobAdaptor adaptor = new JobAdaptor();

        c.clear();
        c.set(Calendar.MONTH, Integer.parseInt(month) - 1);
        c.set(Calendar.YEAR, Integer.parseInt(year));
        c.set(Calendar.DAY_OF_MONTH,1);
        Date startDate = c.getTime();
        c.set(Calendar.DAY_OF_MONTH,30);
        Date endDate = c.getTime();

        List<Job> JobList = jobDAO.getJobsByMonth(startDate, endDate);
        if(JobList != null && !JobList.isEmpty()){
            for(Job job : JobList){
                jobDTOList.add(adaptor.toDTO(job));
            }
        }

        return jobDTOList;

    }

    //get monthly items to the Reports
    @Override
    public List<String> getItemsByMonth(String year, String month) {
        /*JobAdaptor adaptor = new JobAdaptor();*/

        Calendar c = Calendar.getInstance();   // this takes current date


        c.clear();
        c.set(Calendar.MONTH, Integer.parseInt(month) - 1);
        c.set(Calendar.YEAR, Integer.parseInt(year));
        c.set(Calendar.DAY_OF_MONTH,1);
        Date startDate = c.getTime();
        c.set(Calendar.DAY_OF_MONTH,30);
        Date endDate = c.getTime();

        List<String> itemList = jobDAO.getItemsByMonth(startDate, endDate);

        return itemList;

    }

    //get monthly repairs to the Reports
    @Override
    public List<String> getRepairsByMonth(String year, String month) {

        Calendar c = Calendar.getInstance();   // this takes current date

        c.clear();
        c.set(Calendar.MONTH, Integer.parseInt(month) - 1);
        c.set(Calendar.YEAR, Integer.parseInt(year));
        c.set(Calendar.DAY_OF_MONTH,1);
        Date startDate = c.getTime();
        c.set(Calendar.DAY_OF_MONTH,30);
        Date endDate = c.getTime();

        List<String> repairList = jobDAO.getRepairsByMonth(startDate, endDate);

        return repairList;

    }

    //get monthly repairs to the Reports
    @Override
    public List<String> getRentsByMonth(String year, String month) {

        Calendar c = Calendar.getInstance();   // this takes current date

        c.clear();
        c.set(Calendar.MONTH, Integer.parseInt(month) - 1);
        c.set(Calendar.YEAR, Integer.parseInt(year));
        c.set(Calendar.DAY_OF_MONTH,1);
        Date startDate = c.getTime();
        c.set(Calendar.DAY_OF_MONTH,30);
        Date endDate = c.getTime();

        List<String> rentList = jobDAO.getRentsByMonth(startDate, endDate);

        return rentList;

    }

    //get available technicians
    @Override
    public  List<EmployeeDTO> getUsableTechnicians(){

        List<EmployeeDTO> dtoList = new ArrayList<>();
        List<EmployeeDTO> technicianList = employeeService.getAllEmployees();
        List<Job> activeTechnicians = jobDAO.getAllStartedTechnicians();
        Boolean occupy = false;

        if(activeTechnicians.size() >0) {
            for (EmployeeDTO technicianD : technicianList) {
                occupy=false;
                for (Job aTechnician : activeTechnicians) {
                    if(technicianD.getEmployeeId() == aTechnician.getTechnician().getEmpId()){
                        occupy = true;
                        break;
                    }

                }
                if(!occupy){
                    dtoList.add(technicianD);
                }
            }
        }else {
            return technicianList;
        }

        return dtoList;


    }


}
