package com.oms.job.services;

import com.oms.employee.dto.EmployeeDTO;
import com.oms.job.dto.JobDTO;
import com.oms.job.dto.JobItemDTO;
import com.oms.job.dto.JobSlipDTO;
import com.oms.job.dto.SalaryDTO;
import com.oms.job.model.Job;

import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 11/11/2019.
 */
public interface IJobService {

    String saveJob(JobDTO jobDTO);
    SalaryDTO calculateJobSalary(Integer employeeId, String year, String month, Integer attendence);
    Boolean completeJob(Integer jobID);
    List<JobDTO> getAllJobs();
    List<JobDTO> getJobByDate(String year, String month);
    List<String> getItemsByMonth(String year, String month);
    List<String> getRepairsByMonth(String year, String month);
    List<String> getRentsByMonth(String year, String month);
    List<JobDTO> getAllStartedJobs();
    Boolean cancelJob(JobDTO jobDTO);

    JobSlipDTO calculateJobPrice(Integer id);
    List<EmployeeDTO> getUsableTechnicians();
}
