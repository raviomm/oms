package com.oms.job.dto;

import com.oms.employee.model.Employee;

import java.io.Serializable;
import java.math.BigDecimal;

public class SalaryDTO implements Serializable {

    private BigDecimal repairTotal;
    private BigDecimal eligibleRepairAmount;
    private BigDecimal basicSalary;
    private BigDecimal attendenceAllowance;
    private Integer noOfJobs;
    private BigDecimal totalSalary;
    private Employee employee;

    public BigDecimal getRepairTotal() {
        return repairTotal;
    }

    public void setRepairTotal(BigDecimal repairTotal) {
        this.repairTotal = repairTotal;
    }

    public BigDecimal getEligibleRepairAmount() {
        return eligibleRepairAmount;
    }

    public void setEligibleRepairAmount(BigDecimal eligibleRepairAmount) {
        this.eligibleRepairAmount = eligibleRepairAmount;
    }

    public BigDecimal getBasicSalary() {
        return basicSalary;
    }

    public void setBasicSalary(BigDecimal basicSalary) {
        this.basicSalary = basicSalary;
    }

    public BigDecimal getAttendenceAllowance() {
        return attendenceAllowance;
    }

    public void setAttendenceAllowance(BigDecimal attendenceAllowance) {
        this.attendenceAllowance = attendenceAllowance;
    }

    public Integer getNoOfJobs() {
        return noOfJobs;
    }

    public void setNoOfJobs(Integer noOfJobs) {
        this.noOfJobs = noOfJobs;
    }

    public BigDecimal getTotalSalary() {
        return totalSalary;
    }

    public void setTotalSalary(BigDecimal totalSalary) {
        this.totalSalary = totalSalary;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
}
