package com.oms.job.dto;

import java.io.Serializable;

public class JobRepairDTO implements Serializable {

    private String selectRepair;

    public String getSelectRepair() {
        return selectRepair;
    }

    public void setSelectRepair(String selectRepair) {
        this.selectRepair = selectRepair;
    }
}
