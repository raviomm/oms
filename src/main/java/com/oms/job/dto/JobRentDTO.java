package com.oms.job.dto;

import java.io.Serializable;

public class JobRentDTO implements Serializable {

    private String selectRent;

    public String getSelectRent() {
        return selectRent;
    }

    public void setSelectRent(String selectRent) {
        this.selectRent = selectRent;
    }
}
