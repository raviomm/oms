package com.oms.job.dto;

import java.io.Serializable;

public class JobItemDTO implements Serializable {
    private String selectItem;
    private Integer itemCount;


    public String getSelectItem() {
        return selectItem;
    }

    public void setSelectItem(String selectItem) {
        this.selectItem = selectItem;
    }

    public Integer getItemCount() {
        return itemCount;
    }

    public void setItemCount(Integer itemCount) {
        this.itemCount = itemCount;
    }
    
}
