package com.oms.job.dto;

import java.io.Serializable;
import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 11/11/2019.
 */
public class JobDTO implements Serializable {

    private String id;
    private String jobDate;
    private String jobTechnician;
    private String customerNic;
    private String customerLastName;
    private String customerContactNumber;
    private List<JobItemDTO> itemList;
    private List<JobRentDTO> addedRent;
    private List<JobRepairDTO> addedServices;
    private String status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJobDate() {
        return jobDate;
    }

    public void setJobDate(String jobDate) {
        this.jobDate = jobDate;
    }

    public String getJobTechnician() {
        return jobTechnician;
    }

    public void setJobTechnician(String jobTechnician) {
        this.jobTechnician = jobTechnician;
    }

    public String getCustomerNic() {
        return customerNic;
    }

    public void setCustomerNic(String customerNic) {
        this.customerNic = customerNic;
    }

    public String getCustomerLastName() {
        return customerLastName;
    }

    public void setCustomerLastName(String customerLastName) {
        this.customerLastName = customerLastName;
    }

    public String getCustomerContactNumber() {
        return customerContactNumber;
    }

    public void setCustomerContactNumber(String customerContactNumber) {
        this.customerContactNumber = customerContactNumber;
    }

    public List<JobItemDTO> getItemList() {
        return itemList;
    }

    public void setItemList(List<JobItemDTO> itemList) {
        this.itemList = itemList;
    }

    public List<JobRentDTO> getAddedRent() {
        return addedRent;
    }

    public void setAddedRent(List<JobRentDTO> addedRent) {
        this.addedRent = addedRent;
    }

    public List<JobRepairDTO> getAddedServices() {
        return addedServices;
    }

    public void setAddedServices(List<JobRepairDTO> addedServices) {
        this.addedServices = addedServices;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
