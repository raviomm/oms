package com.oms.job.dto;

import com.oms.reporting.dto.ReportItemDTO;
import com.oms.reporting.dto.ReportRentDTO;
import com.oms.reporting.dto.ReportServiceDTO;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Created by Ravi Madhushanka on 1/7/2020.
 */
public class JobSlipDTO implements Serializable {
    private Integer jobId;
    private Date jobDate;
    private String customerLastName;
    private String customerContactNumber;
    private String employeeName;
    private JRBeanCollectionDataSource itemListDataSource;
    private JRBeanCollectionDataSource addedServicesDataSource;
    private JRBeanCollectionDataSource addedRentDataSource;
    private BigDecimal totalPrice;
    private String status;
    List<ReportItemDTO> itemList;
    List<ReportRentDTO> rentList;
    List<ReportServiceDTO> serviceList;
    private BigDecimal itemTotal;
    private BigDecimal rentTotal;
    private BigDecimal repairTotal;

    public Integer getJobId() { return jobId; }

    public void setJobId(Integer jobId) { this.jobId = jobId; }

    public Date getJobDate() { return jobDate; }

    public void setJobDate(Date jobDate) { this.jobDate = jobDate; }

    public String getCustomerLastName() { return customerLastName; }

    public void setCustomerLastName(String customerLastName) { this.customerLastName = customerLastName; }

    public String getCustomerContactNumber() { return customerContactNumber; }

    public void setCustomerContactNumber(String customerContactNumber) { this.customerContactNumber = customerContactNumber; }

    public String getEmployeeName() { return employeeName; }

    public void setEmployeeName(String employeeName) { this.employeeName = employeeName; }

    public JRBeanCollectionDataSource getItemListDataSource() { return itemListDataSource; }

    public void setItemListDataSource(JRBeanCollectionDataSource itemListDataSource) { this.itemListDataSource = itemListDataSource; }

    public JRBeanCollectionDataSource getAddedServicesDataSource() { return addedServicesDataSource; }

    public void setAddedServicesDataSource(JRBeanCollectionDataSource addedServicesDataSource) { this.addedServicesDataSource = addedServicesDataSource; }

    public JRBeanCollectionDataSource getAddedRentDataSource() { return addedRentDataSource; }

    public void setAddedRentDataSource(JRBeanCollectionDataSource addedRentDataSource) { this.addedRentDataSource = addedRentDataSource; }

    public BigDecimal getTotalPrice() { return totalPrice; }

    public void setTotalPrice(BigDecimal totalPrice) { this.totalPrice = totalPrice; }

    public String getStatus() { return status; }

    public void setStatus(String status) { this.status = status; }

    public List<ReportItemDTO> getItemList() {
        return itemList;
    }

    public void setItemList(List<ReportItemDTO> itemList) {
        this.itemList = itemList;
    }

    public List<ReportRentDTO> getRentList() {
        return rentList;
    }

    public void setRentList(List<ReportRentDTO> rentList) {
        this.rentList = rentList;
    }

    public List<ReportServiceDTO> getServiceList() {
        return serviceList;
    }

    public void setServiceList(List<ReportServiceDTO> serviceList) {
        this.serviceList = serviceList;
    }

    public BigDecimal getItemTotal() {
        return itemTotal;
    }

    public void setItemTotal(BigDecimal itemTotal) {
        this.itemTotal = itemTotal;
    }

    public BigDecimal getRentTotal() {
        return rentTotal;
    }

    public void setRentTotal(BigDecimal rentTotal) {
        this.rentTotal = rentTotal;
    }

    public BigDecimal getRepairTotal() {
        return repairTotal;
    }

    public void setRepairTotal(BigDecimal repairTotal) {
        this.repairTotal = repairTotal;
    }
}
