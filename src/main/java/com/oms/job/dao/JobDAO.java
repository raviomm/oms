package com.oms.job.dao;

import com.oms.employee.model.Employee;
import com.oms.job.model.Job;
import com.oms.job.model.JobItem;
import com.oms.job.model.JobRent;
import com.oms.job.model.JobRepair;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 11/11/2019.
 */
@Transactional
@Repository

public class JobDAO implements IJobDAO{

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Job saveJob(Job job) {
        entityManager.persist(job) ;
        return job;
    }

    @Override
    public void saveJobItem(JobItem jobItem) {
        entityManager.persist(jobItem) ;
    }

    @Override
    public void saveJobRepair(JobRepair jobRepair) {
        entityManager.persist(jobRepair) ;
    }

    @Override
    public void saveJobRent(JobRent jobRent) {
        entityManager.persist(jobRent) ;
    }

    @Override
    public List<Job> getEmployeeJobWithDates(Employee emp, Date startDate, Date endDate){
        List<Job> jobList = null;
        try {
            jobList = entityManager.createQuery("from Job job where job.technician.empId = :empId and job.finishDate between :startDate and :endDate ").
                    setParameter("empId", emp.getEmpId()).setParameter("startDate", startDate).setParameter("endDate", endDate).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jobList;
    }

    @Override
    public List<JobRepair> getJobRepair(Job job){
        List<JobRepair> jobRepairs = null;
        try {
            jobRepairs = entityManager.createQuery("from JobRepair jobRepair where jobRepair.job.id = :jobID ").
                    setParameter("jobID", job.getId()).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return jobRepairs;
    }

    @Override
    public Job getJobById(Integer jobId) {
        Job job = null;
        try {
            job = (Job)entityManager.createQuery("from Job job where job.id = :jobID ").
                    setParameter("jobID", jobId).getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return job;
    }

    @Override
    public boolean updateJob(Job job) {
        try {
            entityManager.merge(job);
        }catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }


    @Override
    public List<Job> getAllStartedJobs() {


        /*String hq1 = "SELECT * FROM BookingForm b  inner join `Inventory` i on b.booking_id=i.inventory_item_id ORDER BY inventoryItemId";*/
        String hq1 = "SELECT j FROM  Job j where j.status in ('STARTED') ORDER BY j.id ";
        return (List<Job>) entityManager.createQuery(hq1).getResultList();



    }

    @Override
    public List<Job> getAllJobs() {
        List<Job> jobList = null;
        try {
            jobList = entityManager.createQuery("from Job job").getResultList();
        }catch (Exception e) {
            e.printStackTrace();

        }
        return jobList;
    }

    @Override
    public List<Job> getJobsByMonth(Date startDate, Date endDate) {
        String hql = "SELECT j FROM Job as j WHERE j.jobDate between  :startDate and :endDate  ORDER BY j.id";
        return (List<Job>) entityManager.createQuery(hql).setParameter("startDate",startDate).setParameter("endDate",endDate).getResultList();
    }

    @Override
    public List<String> getItemsByMonth(Date startDate, Date endDate) {
        String hql = "SELECT j FROM JobItem as j WHERE j.job.jobDate between  :startDate and :endDate  ORDER BY j.job.id";
        return (List<String>) entityManager.createQuery(hql).setParameter("startDate",startDate).setParameter("endDate",endDate).getResultList();
    }

    @Override
    public List<String> getRepairsByMonth(Date startDate, Date endDate) {
        String hql = "SELECT r FROM JobRepair as r WHERE r.job.jobDate between  :startDate and :endDate  ORDER BY r.job.id";
        return (List<String>) entityManager.createQuery(hql).setParameter("startDate",startDate).setParameter("endDate",endDate).getResultList();
    }

    @Override
    public List<String> getRentsByMonth(Date startDate, Date endDate) {
        String hql = "SELECT r FROM JobRent as r WHERE r.job.jobDate between  :startDate and :endDate  ORDER BY r.job.id";
        return (List<String>) entityManager.createQuery(hql).setParameter("startDate",startDate).setParameter("endDate",endDate).getResultList();
    }


    @Override
    public List<String> getJobItemList(Job job){

        String hql = "from JobItem jobItem where jobItem.job.id = :jobID";
        return (List<String>) entityManager.createQuery(hql).setParameter("id",job.getId()).getResultList();

    }

    @Override
    public List<String> getJobRepairList(Job job){

        String hql = "from JobRepair jobRepair where jobRepair.job.id = :jobID";
        return (List<String>) entityManager.createQuery(hql).setParameter("id",job.getId()).getResultList();

    }

    @Override
    public List<String> getJobRentList(Job job){

        String hql = "from JobRent jobRent where jobRent.job.id = :jobID";
        return (List<String>) entityManager.createQuery(hql).setParameter("id",job.getId()).getResultList();

    }


    @Override
    public List<JobItem> getJobItem(Job job){
        List<JobItem> jobItems = null;
        try {
            jobItems = entityManager.createQuery("from JobItem jobItem where jobItem.job.id = :jobID ").
                    setParameter("jobID", job.getId()).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return jobItems;
    }

    @Override
    public List<JobRent> getJobRent(Job job){
        List<JobRent> jobRents = null;
        try {
            jobRents = entityManager.createQuery("from JobRent jobRent where jobRent.job.id = :jobID ").
                    setParameter("jobID", job.getId()).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return jobRents;
    }

    @Override
    public Integer getItemIdByJobId(Integer jobId){
        String hq1 = "SELECT j.itemId FROM JobItem as j where j.jobId = :jobId";
        return (Integer) entityManager.createQuery(hq1).setParameter("jobId",jobId).getSingleResult();
    }


    @Override
    public List <Job> getAllStartedTechnicians(){
        String hq1 = "SELECT j FROM  Job j where j.status in ('STARTED')";
        return  (List<Job>) entityManager.createQuery(hq1).getResultList();

    }

}
