package com.oms.job.dao;

import com.oms.employee.model.Employee;
import com.oms.job.model.Job;
import com.oms.job.model.JobItem;
import com.oms.job.model.JobRent;
import com.oms.job.model.JobRepair;

import java.util.Date;
import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 11/11/2019.
 */
public interface IJobDAO {

    Job saveJob(Job job);
    void saveJobItem(JobItem jobItem);
    void saveJobRepair(JobRepair jobRepair);
    void saveJobRent(JobRent jobRent);
    List<Job> getEmployeeJobWithDates(Employee emp, Date startDate, Date endDate);
    List<JobRepair> getJobRepair(Job job);
    Job getJobById(Integer jobId);
    boolean updateJob(Job job);
    List<Job> getAllJobs();
    List<Job> getJobsByMonth(Date startDate, Date endDate);
    /*List<Integer> getEmpIdList(Date startDate,Date endDate);*/
    List<String> getItemsByMonth(Date startDate, Date endDate);
    List<String> getRepairsByMonth(Date startDate, Date endDate);
    List<String> getRentsByMonth(Date startDate, Date endDate);

    List<JobItem> getJobItem(Job job);
    List<JobRent> getJobRent(Job job);
    List<Job> getAllStartedJobs();
    Integer getItemIdByJobId(Integer jobId);

    List<String> getJobItemList(Job job);
    List<String> getJobRepairList(Job job);
    List<String> getJobRentList(Job job);
    List <Job> getAllStartedTechnicians();



}
