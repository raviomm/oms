package com.oms.job.model;

import com.oms.repair.model.Repair;

import javax.persistence.*;

@Entity
@Table(name="job_repair")
public class JobRepair {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer id;
    @ManyToOne
    @JoinColumn(name = "job_id", referencedColumnName = "id")
    private Job job;
    @ManyToOne
    @JoinColumn(name = "repair_id", referencedColumnName = "repair_id")
    private Repair repair;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public Repair getRepair() {
        return repair;
    }

    public void setRepair(Repair repair) {
        this.repair = repair;
    }
}
