package com.oms.job.model;

import com.oms.rent.model.Rent;

import javax.persistence.*;

@Entity
@Table(name="job_rent")
public class JobRent {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer id;
    @ManyToOne
    @JoinColumn(name = "job_id", referencedColumnName = "id")
    private Job job;
    @ManyToOne
    @JoinColumn(name = "rent_id", referencedColumnName = "rent_id")
    private Rent rent;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public Rent getRent() {
        return rent;
    }

    public void setRent(Rent rent) {
        this.rent = rent;
    }
}
