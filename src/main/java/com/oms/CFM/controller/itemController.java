package com.oms.CFM.controller;

import com.oms.common.dto.Response;
import com.oms.item.dto.ItemDTO;
import com.oms.item.services.IItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 11/11/2019.
 */
@Controller
@RequestMapping("/item")
public class itemController {
    @Autowired
    private IItemService itemService;

    @RequestMapping(value = "/saveItem", method = RequestMethod.POST)
    @ResponseBody
    public Response saveItem(final ItemDTO itemDTO) {
        Response response = new Response();
        boolean saveSuccess = itemService.saveItem(itemDTO);
        response.setSuccess(saveSuccess);
        return response;

    }

    @RequestMapping(value = "/getAllItems", method = RequestMethod.GET)
    @ResponseBody
    public Response getAllItems(String itemCode) {
        Response<ItemDTO> response = new Response();
        List<ItemDTO> items = new ArrayList<>();
        if(itemCode !=  null && itemCode != ""){
            ItemDTO item = itemService.getItemByCode(itemCode);
            if(item != null){
                items.add(item);
            }
        } else {
            items = itemService.getAllItems();
        }

        response.setTableData(items);
        response.setSuccess(true);
        return response;


    }

    @RequestMapping(value = "/getAllItemsForDropdowns", method = RequestMethod.GET)
    @ResponseBody
    public Response getAllItemsForDropdown() {
        Response response = new Response();
        List<ItemDTO> itemDTOList = itemService.getAllItems();
        response.setSuccess(true);
        response.setData(itemDTOList);
        return response;

    }

    @RequestMapping(value = "/updateItem", method = RequestMethod.POST)
    @ResponseBody
    public Response updateItem(final ItemDTO itemDTO) {
        Response response = new Response();
        boolean saveSuccess = itemService.updateItem(itemDTO);
        response.setSuccess(saveSuccess);
        return response;
    }

   /* @RequestMapping(value = "/deleteItem", method = RequestMethod.POST)
    @ResponseBody
    public Response deleteItem(final ItemDTO itemDTO) {
        Response response = new Response();
        boolean saveSuccess = itemService.deleteItem(itemDTO);
        response.setSuccess(saveSuccess);
        return response;
    }*/



}
