package com.oms.CFM.controller;

import com.oms.common.dto.Response;
import com.oms.employee.dto.EmployeeDTO;
import com.oms.job.dto.JobDTO;
import com.oms.job.model.Job;
import com.oms.job.services.IJobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 11/11/2019.
 */
@Controller
@RequestMapping("/job")
public class jobController {

    @Autowired
    private IJobService jobService;

    @RequestMapping(value = "/saveJob", method = RequestMethod.POST)
    public ResponseEntity<Response> saveJob(@RequestBody JobDTO jobDTO) {
        Response response = new Response();
        String jobId = jobService.saveJob(jobDTO);
        if(jobId != null && !jobId.isEmpty()){
            response.setSuccess(true);
            jobDTO.setId(jobId);
            response.setData(jobId);
            return new ResponseEntity<Response>(response, HttpStatus.OK);
        }
        response.setSuccess(false);
        return new ResponseEntity<Response>(response, HttpStatus.INTERNAL_SERVER_ERROR);

    }


    @RequestMapping(value = "/completeJob", method = RequestMethod.POST)
    @ResponseBody
    public Response statusComplete(final JobDTO jobDTO) {
        Response response = new Response();
        boolean saveSuccess = jobService.completeJob(Integer.parseInt(jobDTO.getId()));
        response.setSuccess(saveSuccess);
        return response;
    }

    @RequestMapping(value = "/statusCancel", method = RequestMethod.POST)
    @ResponseBody
    public Response statusCancel(final JobDTO jobDTO) {
        Response response = new Response();
        boolean saveSuccess = jobService.cancelJob(jobDTO);
        response.setSuccess(saveSuccess);
        return response;
    }

    @RequestMapping(value = "/getAllJobs", method = RequestMethod.GET)
    public ResponseEntity<Response<List<JobDTO>>> getAllJobs() {
        Response<List<JobDTO>> response = new Response();
        List<JobDTO> jobList = jobService.getAllJobs();
        if(jobList != null && !jobList.isEmpty()){
            response.setSuccess(true);
            response.setData(jobList);
            return new ResponseEntity<Response<List<JobDTO>>>(response, HttpStatus.OK);
        }
        response.setSuccess(false);
        return new ResponseEntity<Response<List<JobDTO>>>(response, HttpStatus.INTERNAL_SERVER_ERROR);

    }

    @RequestMapping(value = "/getAllStartedJobs", method = RequestMethod.GET)
    @ResponseBody
    public Response getAllStartedJobs(){

        Response<JobDTO> response = new Response();
        List<JobDTO> allStartedJobs = jobService.getAllStartedJobs();
        response.setTableData(allStartedJobs);
        response.setSuccess(true);
        return response;



    }

    @RequestMapping(value = "/getAllAvailableTechnicians", method = RequestMethod.GET)
    @ResponseBody
    public Response getAllAvailableTechnicians() {
        Response response = new Response();
        List<EmployeeDTO> technicianDTOList = jobService.getUsableTechnicians();
        response.setSuccess(true);
        response.setData(technicianDTOList);
        return response;

    }
}
