package com.oms.CFM.controller;

import com.oms.employee.services.IEmployeeService;
import com.oms.item.services.IItemService;
import com.oms.reporting.services.IReportingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class BaseController {


    @Autowired
    private IEmployeeService employeeService;

    @Autowired
    private IReportingService reportingService;

    @Autowired
    private IItemService itemService;


    @RequestMapping(value = { "/", "/welcome**" }, method = RequestMethod.GET)
    public ModelAndView welcomePage() {

        ModelAndView model = new ModelAndView();
        model.addObject("title", "Spring Security Custom Login Form");
        model.addObject("message", "This is welcome page!");
        model.setViewName("hello");
        return model;

    }

    @RequestMapping(value = "/admin**", method = RequestMethod.GET)
    public ModelAndView adminPage() {

        ModelAndView model = new ModelAndView();
        model.addObject("title", "Spring Security Custom Login Form");
        model.addObject("message", "This is protected page!");
        model.setViewName("admin");

        return model;

    }

    //Spring Security see this :
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView login(
            @RequestParam(value = "error", required = false) String error,
            @RequestParam(value = "logout", required = false) String logout) {

        ModelAndView model = new ModelAndView();
        if (error != null) {
            model.addObject("error", "Invalid username and password!");
        }

        if (logout != null) {
            model.addObject("msg", "You've been logged out successfully.");
        }
        model.setViewName("login");

        return model;

    }

    @RequestMapping(value = "/dashboard", method = RequestMethod.GET)
    public ModelAndView dashBoardPage() {

        ModelAndView model = new ModelAndView();
        model.setViewName("dashboard");

        return model;

    }

    @RequestMapping(value = "/manageVehicle", method = RequestMethod.GET)
    public ModelAndView manageVehiclePage() {

        ModelAndView model = new ModelAndView();
        model.setViewName("vehicle");

        return model;

    }



    @RequestMapping(value = "/manageEmployee", method = RequestMethod.GET)
    public ModelAndView manageEmployeePage() {

        ModelAndView model = new ModelAndView();
        model.setViewName("employee");

        return model;

    }

    @RequestMapping(value = "/manageRoute", method = RequestMethod.GET)
    public ModelAndView manageRoutePage() {

        ModelAndView model = new ModelAndView();
        model.setViewName("route");

        return model;

    }

    @RequestMapping(value = "/manageProduct", method = RequestMethod.GET)
    public ModelAndView manageProductPage() {

        ModelAndView model = new ModelAndView();
        model.setViewName("product");

        return model;

    }

    @RequestMapping(value = "/manageFleet", method = RequestMethod.GET)
    public ModelAndView manageFleetPage() {

        ModelAndView model = new ModelAndView();
        model.setViewName("fleet");

        return model;

    }

    @RequestMapping(value = "/manageSalary", method = RequestMethod.GET)
    public ModelAndView manageSalaryPage(){

        ModelAndView model = new ModelAndView();
        model.setViewName("salary");

        return model;
    }

    @RequestMapping(value = "/manageReport", method = RequestMethod.GET)
    public ModelAndView manageReportPage(){

        ModelAndView model = new ModelAndView();
        model.setViewName("report");

        return model;
    }

    @RequestMapping(value = "/manageHome", method = RequestMethod.GET)
    public ModelAndView manageHomePage(){

        ModelAndView model = new ModelAndView();
        model.setViewName("home");

        return model;
    }

    @RequestMapping(value = "/manageFleetHistory", method = RequestMethod.GET)
    public ModelAndView manageFleetHistoryPage(){

        ModelAndView model = new ModelAndView();
        model.setViewName("fleethistory");

        return model;
    }


    @RequestMapping(value = "/manageItem", method = RequestMethod.GET)
    public ModelAndView manageItem(){

        ModelAndView model = new ModelAndView();
        model.setViewName("item");

        return model;
    }

    @RequestMapping(value = "/manageRepair", method = RequestMethod.GET)
    public ModelAndView manageRepair(){

        ModelAndView model = new ModelAndView();
        model.setViewName("repair");

        return model;
    }

    @RequestMapping(value = "/manageRent", method = RequestMethod.GET)
    public ModelAndView manageRent(){

        ModelAndView model = new ModelAndView();
        model.setViewName("rent");

        return model;
    }

    @RequestMapping(value = "/manageJob", method = RequestMethod.GET)
    public ModelAndView manageJob(){

        ModelAndView model = new ModelAndView();
        model.setViewName("job");

        return model;
    }

    @RequestMapping(value = "/manageInvoice", method = RequestMethod.GET)
    public ModelAndView manageInvoice(){

        ModelAndView model = new ModelAndView();
        model.setViewName("invoice");

        return model;
    }

    @RequestMapping(value = "/managejobStatus", method = RequestMethod.GET)
    public ModelAndView manageJobStatus(){

        ModelAndView model = new ModelAndView();
        model.setViewName("jobStatus");

        return model;
    }

}
