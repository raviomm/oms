package com.oms.CFM.controller;

import com.oms.common.dto.Response;
import com.oms.rent.dto.RentDTO;
import com.oms.rent.services.IRentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 11/11/2019.
 */
@Controller
@RequestMapping("/rent")
public class rentController {

    @Autowired
    private IRentService rentService;

    @RequestMapping(value = "/saveRent", method = RequestMethod.POST)
    @ResponseBody
    public Response saveRent(final RentDTO rentDTO) {
        Response response = new Response();
        boolean saveSuccess = rentService.saveRent(rentDTO);
        response.setSuccess(saveSuccess);
        return response;

    }

    @RequestMapping(value = "/getAllRents", method = RequestMethod.POST)
    @ResponseBody
    public Response getAllRents(String rentCode) {
        Response<RentDTO> response = new Response();
        List<RentDTO> rents = new ArrayList<>();
        if(rentCode !=  null && rentCode != ""){
            RentDTO rent = rentService.getRentByCode(rentCode);
            if(rent != null){
                rents.add(rent);
            }
        } else {
            rents = rentService.getAllRents();
        }

        response.setTableData(rents);
        response.setSuccess(true);
        return response;

    }

    @RequestMapping(value = "/updateRent", method = RequestMethod.POST)
    @ResponseBody
    public Response updateRent(final RentDTO rentDTO) {
        Response response = new Response();
        boolean saveSuccess = rentService.updateRent(rentDTO);
        response.setSuccess(saveSuccess);
        return response;
    }

    @RequestMapping(value = "/getAllRentForDropdowns", method = RequestMethod.GET)
    @ResponseBody
    public Response getAllRentForDropdowns() {
        Response response = new Response();
        List<RentDTO> rentDTOList = rentService.getAllRents();
        response.setSuccess(true);
        response.setData(rentDTOList);
        return response;

    }

    /*@RequestMapping(value = "/deleteRent", method = RequestMethod.POST)
    @ResponseBody
    public Response deleteRent(final RentDTO rentDTO) {
        Response response = new Response();
        boolean saveSuccess = rentService.deleteRent(rentDTO);
        response.setSuccess(saveSuccess);
        return response;
    }*/

}
