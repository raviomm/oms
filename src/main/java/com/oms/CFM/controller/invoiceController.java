package com.oms.CFM.controller;

import com.oms.common.dto.Response;
import com.oms.invoice.dto.InvoiceDTO;
import com.oms.invoice.services.IInvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 11/14/2019.
 */
@Controller
@RequestMapping("/invoice")
public class invoiceController {


    @Autowired
    private IInvoiceService invoiceService;

    /*@RequestMapping(value = "/saveInvoice", method = RequestMethod.POST)
    @ResponseBody
    public Response saveInvoice(final InvoiceDTO invoiceDTO) {
        Response response = new Response();
        boolean saveSuccess = invoiceService.saveInvoice(invoiceDTO);
        response.setSuccess(saveSuccess);
        return response;

    }*/
    @RequestMapping(value = "/saveInvoice", method = RequestMethod.POST)
    public ResponseEntity<Response> saveInvoice(@RequestBody InvoiceDTO invoiceDTO) {
        Response response = new Response();
        String invoiceId = invoiceService.saveInvoice(invoiceDTO);
        if(invoiceId != null && !invoiceId.isEmpty()){
            response.setSuccess(true);
            invoiceDTO.setInvoiceId(Integer.parseInt(invoiceId));
            response.setData(invoiceId);
            return new ResponseEntity<Response>(response, HttpStatus.OK);
        }
        response.setSuccess(false);
        return new ResponseEntity<Response>(response, HttpStatus.INTERNAL_SERVER_ERROR);

    }

    @RequestMapping(value = "getAllInvoices", method = RequestMethod.POST)
    @ResponseBody
    public Response getAllInvoices(String invoiceNumber) {
        Response<InvoiceDTO> response = new Response();
        List<InvoiceDTO> invoices = new ArrayList<>();
        if(invoiceNumber !=  null && invoiceNumber != ""){
            InvoiceDTO invoice = invoiceService.getInvoiceByNumber(invoiceNumber);
            if(invoice != null){
                invoices.add(invoice);
            }
        } else {
            invoices = invoiceService.getAllInvoices();
        }

        response.setTableData(invoices);
        response.setSuccess(true);
        return response;

    }


}
