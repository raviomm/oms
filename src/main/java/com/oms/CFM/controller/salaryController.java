package com.oms.CFM.controller;

import com.oms.common.dto.Response;
import com.oms.job.dto.SalaryDTO;
import com.oms.job.services.IJobService;
import com.oms.reporting.services.IReportingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;

/**
 * Created by RAVI KALUARACHCHI on 12/15/2017.
 */
@Controller
@RequestMapping ("/salary")
public class salaryController {
//
//    @Autowired
//    private ISalaryService salaryService;
//
    @Autowired
    private IReportingService reportingService;

    @Autowired
    private IJobService jobService;

//
//

    @RequestMapping (value = "/calculateSalary" , method = RequestMethod.GET)
    @ResponseBody
    public Response<SalaryDTO> getEmployeeSalary(@RequestParam Integer employeeId, @RequestParam String year, @RequestParam String month, @RequestParam Integer attendence)

    {
        Response<SalaryDTO> response = new Response<>();
        SalaryDTO salaryDTO = jobService.calculateJobSalary(employeeId, year, month, attendence);
        response.setSuccess(true);
        if(salaryDTO == null){
            response.setSuccess(false);
        }
        response.setData(salaryDTO);

        return response;

    }

    @RequestMapping (value = "/generateReport" , method = RequestMethod.GET)
    @ResponseBody
    public Response<String> generateReport(@RequestParam Integer employeeId, @RequestParam String year, @RequestParam String month, @RequestParam Integer attendence)

    {
        Response<String> response = new Response<>();
        SalaryDTO salaryDTO = jobService.calculateJobSalary(employeeId, year, month, attendence);
        if(salaryDTO == null){
            response.setSuccess(false);
        }
        String base64String = reportingService.generateSalarySlip(salaryDTO, year, month);
        response.setData(base64String);
        response.setSuccess(true);

        return response;

    }

}
