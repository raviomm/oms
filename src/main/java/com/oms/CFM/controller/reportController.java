package com.oms.CFM.controller;

import com.oms.common.dto.Response;
import com.oms.employee.services.IEmployeeService;
import com.oms.invoice.services.IInvoiceService;
import com.oms.job.dto.JobDTO;
import com.oms.job.dto.JobItemDTO;
import com.oms.job.services.IJobService;
import com.oms.reporting.services.IReportingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 11/9/2018.
 */
@Controller
@RequestMapping("report")
public class reportController {

    @Autowired
    IReportingService reportingService;

   @Autowired
   IEmployeeService employeeService;

   @Autowired
   IJobService jobService;

   @Autowired
    IInvoiceService invoiceService;



   @RequestMapping(value = "/getAllJobsMonthWise", method = RequestMethod.GET)
   @ResponseBody
    public Response<JobDTO> getJobsHistoryDateWise(@RequestParam String year, @RequestParam String month) {

        Response<JobDTO> response = new Response();
        List<JobDTO> jobs = new ArrayList<>();
        if (year != null && year != "" && month != null && month != "") {
            jobs = jobService.getJobByDate(year, month);
        }
        response.setTableData(jobs);
        response.setSuccess(true);
        return response;
   }

    @RequestMapping(value = "/getAllItemsMonthWise", method = RequestMethod.GET)
    @ResponseBody
    public Response<String> getItemsByMonth(@RequestParam String year, @RequestParam String month) {

        Response<String> response = new Response();
        List<String> items = new ArrayList<>();
        if (year != null && year != "" && month != null && month != "") {
            items = jobService.getItemsByMonth(year, month);
        }
        response.setTableData(items);
        response.setSuccess(true);
        return response;
    }

    @RequestMapping(value = "/getAllRepairsMonthWise", method = RequestMethod.GET)
    @ResponseBody
    public Response<String> getRepairsByMonth(@RequestParam String year, @RequestParam String month) {

        Response<String> response = new Response();
        List<String> repairs = new ArrayList<>();
        if (year != null && year != "" && month != null && month != "") {
            repairs = jobService.getRepairsByMonth(year, month);
        }
        response.setTableData(repairs);
        response.setSuccess(true);
        return response;
    }



    @RequestMapping(value = "/getAllRentsMonthWise", method = RequestMethod.GET)
    @ResponseBody
    public Response<String> getRentsByMonth(@RequestParam String year, @RequestParam String month) {

        Response<String> response = new Response();
        List<String> rents = new ArrayList<>();
        if (year != null && year != "" && month != null && month != "") {
            rents = jobService.getRentsByMonth(year, month);
        }
        response.setTableData(rents);
        response.setSuccess(true);
        return response;
    }

    @RequestMapping(value = "/getAllInvocesMonthWise", method = RequestMethod.GET)
    @ResponseBody
    public Response<String> getInvoiceByMonth(@RequestParam String year, @RequestParam String month) {

        Response<String> response = new Response();
        List<String> invoices = new ArrayList<>();
        if (year != null && year != "" && month != null && month != "") {
            invoices = invoiceService.getInvoiceByMonth(year, month);
        }
        response.setTableData(invoices);
        response.setSuccess(true);
        return response;
    }
}