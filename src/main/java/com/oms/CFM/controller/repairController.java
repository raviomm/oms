package com.oms.CFM.controller;

import com.oms.common.dto.Response;
import com.oms.repair.dto.RepairDTO;
import com.oms.repair.services.IRepairServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 11/11/2019.
 */
@Controller
@RequestMapping("/repair")
public class repairController {
    @Autowired
    private IRepairServices repairService;

    @RequestMapping(value = "/saveRepair", method = RequestMethod.POST)
    @ResponseBody
    public Response saveRepair(final RepairDTO repairDTO) {
        Response response = new Response();
        boolean saveSuccess = repairService.saveRepair(repairDTO);
        response.setSuccess(saveSuccess);
        return response;

    }


    @RequestMapping(value = "/getAllRepairs", method = RequestMethod.POST)
    @ResponseBody
    public Response getAllRepairs(String repairCode) {
        Response<RepairDTO> response = new Response();
        List<RepairDTO> reparis = new ArrayList<>();
        if(repairCode !=  null && repairCode != ""){
            RepairDTO repair = repairService.getRepairByCode(repairCode);
            if(repair != null){
                reparis.add(repair);
            }
        } else {
            reparis = repairService.getAllRepairs();
        }

        response.setTableData(reparis);
        response.setSuccess(true);
        return response;


    }

    @RequestMapping(value = "/updateRepair", method = RequestMethod.POST)
    @ResponseBody
    public Response updateRepair(final RepairDTO repairDTO) {
        Response response = new Response();
        boolean saveSuccess = repairService.updateRepair(repairDTO);
        response.setSuccess(saveSuccess);
        return response;
    }

    @RequestMapping(value = "/getAllRepairsForDropdowns", method = RequestMethod.GET)
    @ResponseBody
    public Response getAllRepairsForDropdowns() {
        Response response = new Response();
        List<RepairDTO> repairDTOList = repairService.getAllRepairs();
        response.setSuccess(true);
        response.setData(repairDTOList);
        return response;

    }

    /*@RequestMapping(value = "/deleteRepair", method = RequestMethod.POST)
    @ResponseBody
    public Response deleteRepair(final RepairDTO repairDTO) {
        Response response = new Response();
        boolean saveSuccess = repairService.deleteRepair(repairDTO);
        response.setSuccess(saveSuccess);
        return response;
    }*/


}
