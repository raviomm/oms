package com.oms.CFM.controller;

import com.oms.common.dto.Response;
import com.oms.job.dto.JobSlipDTO;
import com.oms.job.services.IJobService;
import com.oms.reporting.services.IReportingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by RAVI KALUARACHCHI on 12/12/2019.
 */
@Controller
@RequestMapping("/jobStatus")
public class jobStatusController {

    @Autowired
    private IJobService jobService;

    @Autowired
    private IReportingService reportingService;


    @RequestMapping (value = "/generateJobSlip" , method = RequestMethod.GET)
    @ResponseBody
    public Response<String> generateJobSlip(@RequestParam Integer id)

    {
        Response<String> response = new Response<>();
        JobSlipDTO jobSlipDTO = jobService.calculateJobPrice(id);
        if(jobSlipDTO == null){
            response.setSuccess(false);
        }
        String base64String = reportingService.generateJobSlip(jobSlipDTO);
        response.setData(base64String);
        response.setSuccess(true);

        return response;

    }
}
