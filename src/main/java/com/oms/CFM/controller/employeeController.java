package com.oms.CFM.controller;
import com.oms.common.dto.Response;
import com.oms.employee.dto.EmployeeDTO;
import com.oms.employee.model.Employee;
import com.oms.employee.services.IEmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 11/28/2017.
 */

@Controller
@RequestMapping("/employee")
public class employeeController {

    @Autowired
    private IEmployeeService employeeService;

    @RequestMapping(value = "/saveEmployees", method = RequestMethod.POST)
    @ResponseBody
    public Response saveEmployees(final EmployeeDTO employeeDTO) {
        Response response = new Response();
        boolean saveSuccess = employeeService.saveEmployee(employeeDTO);
        response.setSuccess(saveSuccess);
        return response;

    }


    @RequestMapping(value = "/getAllEmployees", method = RequestMethod.POST)
    @ResponseBody
    public Response getAllEmployees(String employeeNIC) {
        Response<EmployeeDTO> response = new Response();
        List<EmployeeDTO> employees = new ArrayList<>();
        if(employeeNIC !=  null && employeeNIC != ""){
            EmployeeDTO employee = employeeService.getEmployeeByNIC(employeeNIC);
            if(employee != null){
                employees.add(employee);
            }
        } else {
            employees = employeeService.getAllEmployees();
        }

        response.setTableData(employees);
        response.setSuccess(true);
        return response;

    }



    @RequestMapping(value = "/updateEmployee", method = RequestMethod.POST)
    @ResponseBody
    public Response updateEmployee(final EmployeeDTO employeeDTO) {
        Response response = new Response();
        boolean saveSuccess = employeeService.updateEmployee(employeeDTO);
        response.setSuccess(saveSuccess);
        return response;
    }

    @RequestMapping(value = "/deleteEmployee", method = RequestMethod.POST)
    @ResponseBody
    public Response deleteEmployee(final EmployeeDTO employeeDTO) {
        Response response = new Response();
       boolean saveSuccess = employeeService.deleteEmployee(employeeDTO);
        response.setSuccess(saveSuccess);
        return response;
    }

    @RequestMapping(value = "/getAllTechnicianForDropdowns", method = RequestMethod.GET)
    @ResponseBody
    public Response getAllTechnicianForDropdowns() {
        Response response = new Response();
        List<EmployeeDTO> technicianDTOList = employeeService.getAllEmployees();
        response.setSuccess(true);
        response.setData(technicianDTOList);
        return response;

    }

    @RequestMapping(value = "/getEmployeeById", method = RequestMethod.GET)
    @ResponseBody
    public Response getEmployeeById(@RequestParam Integer employeeId) {
        Response response = new Response();
        Employee employee = employeeService.getEmployeeByID(employeeId);
        response.setSuccess(true);
        response.setData(employee);
        return response;

    }
}



