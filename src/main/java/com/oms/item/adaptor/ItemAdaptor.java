package com.oms.item.adaptor;

import com.oms.common.adaptor.BasicAdaptor;
import com.oms.item.dto.ItemDTO;
import com.oms.item.model.Item;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by RAVI KALUARACHCHI on 11/11/2019.
 */
public class ItemAdaptor extends BasicAdaptor<Item,ItemDTO> {
    @Override
    public Item toModel(ItemDTO dto) {

        Item item = new Item();

        item.setItemId(dto.getItemId());
        item.setItemName(dto.getItemName());
        item.setItemCode(dto.getItemCode());
        Date date1= null;
        try {
            date1 = new SimpleDateFormat("yyyy-MM-dd").parse(dto.getItemAddDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        item.setItemAddDate(date1);
        item.setItemCompany(dto.getItemCompany());
        item.setItemHorsepower(dto.getItemHorsepower());
        item.setRetailPrice(dto.getRetailPrice());
        item.setSellPrice(dto.getSellPrice());
        item.setItemStatus(dto.getItemStatus());


        return item;

    }

    @Override
    public ItemDTO toDTO(Item model) {

        ItemDTO dto = new ItemDTO();
        dto.setItemId(model.getItemId());
        dto.setItemName(model.getItemName());
        dto.setItemCode(model.getItemCode());
        dto.setItemAddDate(model.getItemAddDate().toString());
        dto.setItemCompany(model.getItemCompany());
        dto.setItemHorsepower(model.getItemHorsepower());
        dto.setRetailPrice(model.getRetailPrice());
        dto.setSellPrice(model.getSellPrice());
        dto.setItemStatus(model.getItemStatus());
        dto.setCount(model.getCount());


        return dto;


    }
}
