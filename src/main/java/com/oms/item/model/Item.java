package com.oms.item.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by RAVI KALUARACHCHI on 11/11/2019.
 */
@Entity
@Table(name="item")

public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "item_id")
    public Integer itemId;
    @Column(name = "item_name")
    public String itemName;
    @Column(name = "item_code")
    public String itemCode;
    @Column(name = "item_add_date")
    public Date itemAddDate;
    @Column(name = "item_company")
    public String itemCompany;
    @Column(name = "item_horse_power")
    public Integer itemHorsepower;
    @Column(name = "retail_price")
    public Double retailPrice;
    @Column(name = "sell_price")
    public Double sellPrice;
    @Column(name = "item_status")
    public String itemStatus;
    public Integer count;



    public Integer getItemId() { return itemId; }

    public void setItemId(Integer itemId) { this.itemId = itemId; }

    public String getItemName() { return itemName; }

    public void setItemName(String itemName) { this.itemName = itemName; }

    public String getItemCode() { return itemCode; }

    public void setItemCode(String itemCode) { this.itemCode = itemCode; }

    public Date getItemAddDate() { return itemAddDate; }

    public void setItemAddDate(Date itemAddDate) { this.itemAddDate = itemAddDate; }

    public String getItemCompany() { return itemCompany; }

    public void setItemCompany(String itemCompany) { this.itemCompany = itemCompany; }

    public Integer getItemHorsepower() { return itemHorsepower; }

    public void setItemHorsepower(Integer itemHorsepower) { this.itemHorsepower = itemHorsepower; }

    public Double getRetailPrice() { return retailPrice; }

    public void setRetailPrice(Double retailPrice) { this.retailPrice = retailPrice; }

    public Double getSellPrice() { return sellPrice; }

    public void setSellPrice(Double sellPrice) { this.sellPrice = sellPrice; }

    public String getItemStatus() { return itemStatus; }

    public void setItemStatus(String itemStatus) { this.itemStatus = itemStatus; }

    public Integer getCount() { return count; }

    public void setCount(Integer count) { this.count = count; }
}
