package com.oms.item.model;


import javax.persistence.*;
import java.util.Date;


/**
 * Created by RAVI KALUARACHCHI on 11/11/2019.
 */

@Entity
@Table(name="item_stock")
public class ItemStock {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer id;
    @ManyToOne
    @JoinColumn(name = "item_id", referencedColumnName = "item_id")
    private Item item;
    public Integer balance;
    @Column(name = "trans_date")
    public Date transDate;
    @Column(name = "trans_type")
    public String transType; //ADD or REMOVE
    @Column(name = "trans_count")
    public Integer transCount;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public Date getTransDate() {
        return transDate;
    }

    public void setTransDate(Date transDate) {
        this.transDate = transDate;
    }

    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public Integer getTransCount() {
        return transCount;
    }

    public void setTransCount(Integer transCount) {
        this.transCount = transCount;
    }
}
