package com.oms.item.services;

import com.oms.item.dto.ItemDTO;

import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 11/11/2019.
 */
public interface IItemService {

    boolean saveItem(ItemDTO itemDTO);

    List<ItemDTO> getAllItems();
    ItemDTO getItemByCode(String itemCode);
    boolean updateItem (ItemDTO itemDTO);
    /*boolean deleteItem (ItemDTO itemDTO);*/


}
