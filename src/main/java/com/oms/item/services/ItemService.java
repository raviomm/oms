package com.oms.item.services;

import com.oms.item.adaptor.ItemAdaptor;
import com.oms.item.dao.IItemDAO;
import com.oms.item.dto.ItemDTO;
import com.oms.item.model.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 11/11/2019.
 */

@Service
public class ItemService implements IItemService {

    @Autowired
    private IItemDAO itemDAO;
    @Override
    public boolean saveItem(ItemDTO itemDTO){

        ItemAdaptor adaptor = new ItemAdaptor();
        Item item = adaptor.toModel(itemDTO);
//        employee.setCreatedTime(new Date());
//        employee.setModifiedTime(new Date());
//        employee.setVersion(0);
        return itemDAO.saveItem(item);

    }
    @Override
    public List<ItemDTO> getAllItems(){

        List<ItemDTO> itemDTOList = new ArrayList<>();
        ItemAdaptor adaptor = new ItemAdaptor();
        List<Item> list = itemDAO.getAllItems();

        if(list != null & !list.isEmpty()) {

            for(Item item : list){
                ItemDTO itemDTO = adaptor.toDTO(item);
                itemDTOList.add(itemDTO);
            }
        }

        return itemDTOList;

    }
    @Override
    public ItemDTO getItemByCode(String itemCode){

        ItemAdaptor adaptor = new ItemAdaptor();
        Item item = itemDAO.getItemByCode(itemCode);

        ItemDTO itemDTOList = adaptor.toDTO(item);
        return itemDTOList;
    }

    @Override
    public boolean updateItem (ItemDTO itemDTO){
        ItemAdaptor adaptor = new ItemAdaptor();
        Item item = adaptor.toModel(itemDTO);
        Item existingItem = itemDAO.getItemByCode(item.getItemCode());
        if (existingItem != null){


            return itemDAO.updateItem(item);

        } else {
            return false;
        }

    }

    /*@Override
    public boolean deleteItem (ItemDTO itemDTO){
        ItemAdaptor adaptor = new ItemAdaptor();
        Item item = adaptor.toModel(itemDTO);
        Item existingItem = itemDAO.getItemByCode(item.getItemCode());
        if (existingItem !=null){
            return itemDAO.deleteItem(existingItem);
        }else{
            return false;
        }

    }*/
}
