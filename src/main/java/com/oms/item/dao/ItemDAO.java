package com.oms.item.dao;

import com.oms.invoice.model.Invoice;
import com.oms.item.model.Item;
import com.oms.item.model.ItemStock;
import com.oms.job.model.Job;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 11/11/2019.
 */
@Transactional
@Repository

public class ItemDAO implements IItemDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Item getItembyId (Integer itemId) {

        Item item = null ;
        String ehq1 = "FROM Item i WHERE  i.itemId = :itemId" ;
        List<Item> items = entityManager.createQuery(ehq1).setParameter( "itemId",itemId).getResultList() ;
        if (items != null && items.size() > 0){

            item = items.get(0) ;
        }

        return item ;

    }

    @Override
    public boolean saveItem (Item item) {

        Item persistedItem = null ;
        entityManager.persist(item) ;
        persistedItem = getItembyId(item.getItemId()) ;
        if (persistedItem != null){

            return true;
        }
        return false;

    }

    @Override
    public  List<Item> getAllItems() {

        List<Item> items = entityManager.createQuery("select item from Item item").getResultList();

        return items;

    }

    @Override
    public  boolean jobCancelItemTrans(Item item, Integer quantity){

        List<ItemStock> previousItemStockList = null;
        Integer balance= null;
        try {
            previousItemStockList = entityManager.createQuery("from ItemStock itemStock where itemStock.item.itemId = :itemId ORDER BY itemStock.transDate DESC ").setParameter("itemId", item.getItemId()).getResultList();

        } catch (Exception e) {
            e.printStackTrace();
        }
        if(previousItemStockList != null || !previousItemStockList.isEmpty()){
            ItemStock previousItemStock =  previousItemStockList.get(0);
            balance = previousItemStock.getBalance() + quantity;

        }
        ItemStock itemStock = new ItemStock();
        itemStock.setItem(item);
        itemStock.setTransType("JOB CSL");
        itemStock.setTransCount(quantity);
        itemStock.setBalance(balance);
        itemStock.setTransDate(new Date());
        entityManager.persist(itemStock);

        item.setCount(balance);
        entityManager.merge(item);

        return true;

    }

    @Override
    public boolean createInvoiceItemTrans(Invoice invoice, Item item, Integer quantity) {
        List<ItemStock> previousItemStockList = null;
        Integer balance;
        try {
            previousItemStockList = entityManager.createQuery("from ItemStock itemStock where itemStock.item.itemId = :itemId ORDER BY itemStock.transDate DESC ").setParameter("itemId", item.getItemId()).getResultList();

        } catch (Exception e) {
            e.printStackTrace();
        }
        if(previousItemStockList == null || previousItemStockList.isEmpty()){
            balance =  quantity;
        } else {
            ItemStock previousItemStock =  previousItemStockList.get(0);
            balance = previousItemStock.getBalance() + quantity;
        }

        ItemStock itemStock = new ItemStock();
        itemStock.setItem(item);
        itemStock.setTransType("ADD");
        itemStock.setTransCount(quantity);
        itemStock.setBalance(balance);
        itemStock.setTransDate(new Date());
        entityManager.persist(itemStock);

        item.setCount(balance);
        entityManager.merge(item);

        return true;
    }

    @Override
    public boolean createJobItemTrans(Job job, Item item, Integer quantity) {
        List<ItemStock> previousItemStockList = null;
        Integer balance;
        try {
            previousItemStockList = entityManager.createQuery("from ItemStock itemStock where itemStock.item.itemId = :itemId ORDER BY itemStock.transDate DESC ").setParameter("itemId", item.getItemId()).getResultList();

        } catch (Exception e) {
            e.printStackTrace();
        }
        if(previousItemStockList == null || previousItemStockList.isEmpty()){
            throw new RuntimeException("Items should be invoices first to use");
        } else {
            ItemStock previousItemStock =  previousItemStockList.get(0);
            if(previousItemStock.getBalance() - quantity < 0){
                throw new RuntimeException("cannot exceed balance count for item");
            }
            balance = previousItemStock.getBalance() - quantity;
        }

        ItemStock itemStock = new ItemStock();
        itemStock.setItem(item);
        itemStock.setTransType("REMOVE");
        itemStock.setTransCount(quantity);
        itemStock.setBalance(balance);
        itemStock.setTransDate(new Date());
        entityManager.persist(itemStock);

        item.setCount(balance);
        entityManager.merge(item);

        return true;
    }

    @Override
    public Item getItemByName (String itemName){
        List<Item> items = entityManager.createQuery("select item from Item item where item.itemName = :itemName").
                setParameter("itemName", itemName).getResultList();
        if(items != null && !items.isEmpty()){
            return items.get(0);
        }
        return  null;
    }

    @Override
    public Item getItemByCode (String itemCode){
        List<Item> items = entityManager.createQuery("select item from Item item where item.itemCode = :itemCode").
                setParameter("itemCode", itemCode).getResultList();
        if(items != null && !items.isEmpty()){
            return items.get(0);
        }
        return  null;
    }

    @Override
    public boolean updateItem(Item item) {
        Item persistedItem = null;
        entityManager.merge(item);
        persistedItem = getItemByCode(item.getItemCode());
        if(persistedItem != null){
            return  true;
        }
        return false;
    }

    @Override
    public Integer getItemBalance (Integer itemId){
        String hql = "SELECT i.balance from ItemStock i where Item.itemId = :itemId ORDER BY itemStock.transDate DESC";
        return (Integer) entityManager.createQuery(hql).setParameter("itemId",itemId).getSingleResult();

    }

    /*@Override
    public boolean deleteItem(Item item) {
        try{
            entityManager.remove(item);
        }catch (Exception e){
            return false;
        }

        return true;
    }*/

}
