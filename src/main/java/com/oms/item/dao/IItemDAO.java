package com.oms.item.dao;

import com.oms.invoice.model.Invoice;
import com.oms.item.model.Item;
import com.oms.job.model.Job;

import java.util.List;

/**
 * Created by RAVI KALUARACHCHI on 11/11/2019.
 */
public interface IItemDAO {

    Item getItembyId (Integer itemId);
    boolean saveItem (Item item);
    Item getItemByName (String itemName);
    List<Item> getAllItems();
    boolean createInvoiceItemTrans(Invoice invoice, Item item, Integer quantity);
    boolean createJobItemTrans(Job job, Item item, Integer quantity);
    Item getItemByCode (String itemCode);
    boolean updateItem(Item item);
    /*boolean deleteItem(Item item);*/
    Integer getItemBalance (Integer itemId);
    boolean jobCancelItemTrans(Item item, Integer quantity);
}
