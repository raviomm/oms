package com.oms.item.dto;

import java.io.Serializable;

/**
 * Created by RAVI KALUARACHCHI on 11/11/2019.
 */
public class ItemDTO implements Serializable {

    private static final long serialVersionUID = 7526472295222376148L;

    private Integer itemId;
    private String itemName;
    private String itemCode;
    private String itemAddDate;
    private String itemCompany;
    private Integer itemHorsepower;
    private Double retailPrice;
    private Double sellPrice;
    private String itemStatus;
    private Integer count;





    public static long getSerialVersionUID() { return serialVersionUID; }

    public Integer getItemId() { return itemId; }

    public void setItemId(Integer itemId) { this.itemId = itemId; }

    public String getItemName() { return itemName; }

    public void setItemName(String itemName) { this.itemName = itemName; }

    public String getItemCode() { return itemCode; }

    public void setItemCode(String itemCode) { this.itemCode = itemCode; }

    public String getItemAddDate() { return itemAddDate; }

    public void setItemAddDate(String itemAddDate) { this.itemAddDate = itemAddDate; }

    public String getItemCompany() { return itemCompany; }

    public void setItemCompany(String itemCompany) { this.itemCompany = itemCompany; }

    public Integer getItemHorsepower() { return itemHorsepower; }

    public void setItemHorsepower(Integer itemHorsepower) { this.itemHorsepower = itemHorsepower; }

    public Double getRetailPrice() { return retailPrice; }

    public void setRetailPrice(Double retailPrice) { this.retailPrice = retailPrice; }

    public Double getSellPrice() { return sellPrice; }

    public void setSellPrice(Double sellPrice) { this.sellPrice = sellPrice; }

    public String getItemStatus() { return itemStatus; }

    public void setItemStatus(String itemStatus) { this.itemStatus = itemStatus; }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
