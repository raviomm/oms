-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 23, 2019 at 06:58 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `oms`
--

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `emp_id` int(11) NOT NULL,
  `address1` varchar(255) DEFAULT NULL,
  `allowance` decimal(19,2) DEFAULT NULL,
  `basic_salary` decimal(19,2) DEFAULT NULL,
  `designation` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `mobile` int(11) DEFAULT NULL,
  `emp_nic` varchar(255) DEFAULT NULL,
  `epf_no` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `invoice_id` int(11) NOT NULL,
  `invoice_count` int(11) DEFAULT NULL,
  `invoice_date` datetime DEFAULT NULL,
  `invoice_number` varchar(255) DEFAULT NULL,
  `select_invoice_item` varchar(255) DEFAULT NULL,
  `vendor_address` varchar(255) DEFAULT NULL,
  `vendor_mobile` varchar(255) DEFAULT NULL,
  `vendor_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE `item` (
  `item_id` int(11) NOT NULL,
  `item_add_date` datetime DEFAULT NULL,
  `item_code` varchar(255) DEFAULT NULL,
  `item_company` varchar(255) DEFAULT NULL,
  `item_horse_power` int(11) DEFAULT NULL,
  `item_name` varchar(255) DEFAULT NULL,
  `item_status` varchar(255) DEFAULT NULL,
  `retail_price` double DEFAULT NULL,
  `sell_price` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`item_id`, `item_add_date`, `item_code`, `item_company`, `item_horse_power`, `item_name`, `item_status`, `retail_price`, `sell_price`) VALUES
(1, '2019-11-12 00:00:00', 'rbs235689', 'SUZUKI', 15, 'rubber bush', 'ACT', 100, 110),
(2, '2019-11-12 00:00:00', 'abt25489', 'MITSUBISHI', 25, 'Anker Bolt', 'ACT', 100.01, 110.01);

-- --------------------------------------------------------

--
-- Table structure for table `job`
--

CREATE TABLE `job` (
  `id` int(11) NOT NULL,
  `cus_mobile` varchar(255) DEFAULT NULL,
  `customer_nic` varchar(255) DEFAULT NULL,
  `job_date` datetime DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job`
--

INSERT INTO `job` (`id`, `cus_mobile`, `customer_nic`, `job_date`, `employee_id`) VALUES
(1, 'asdasd', '23423423', '2019-01-20 00:11:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `job_item`
--

CREATE TABLE `job_item` (
  `id` int(11) NOT NULL,
  `count` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `job_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_item`
--

INSERT INTO `job_item` (`id`, `count`, `item_id`, `job_id`) VALUES
(1, 2, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `job_rent`
--

CREATE TABLE `job_rent` (
  `id` int(11) NOT NULL,
  `job_id` int(11) DEFAULT NULL,
  `rent_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_rent`
--

INSERT INTO `job_rent` (`id`, `job_id`, `rent_id`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `job_repair`
--

CREATE TABLE `job_repair` (
  `id` int(11) NOT NULL,
  `job_id` int(11) DEFAULT NULL,
  `repair_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_repair`
--

INSERT INTO `job_repair` (`id`, `job_id`, `repair_id`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `product_id` int(11) NOT NULL,
  `product_category` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `porfit_per_unit` decimal(19,2) DEFAULT NULL,
  `product_status` varchar(255) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rent`
--

CREATE TABLE `rent` (
  `rent_id` int(11) NOT NULL,
  `rent_charge` double DEFAULT NULL,
  `rent_code` varchar(255) DEFAULT NULL,
  `rent_description` varchar(255) DEFAULT NULL,
  `rent_resource` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rent`
--

INSERT INTO `rent` (`rent_id`, `rent_charge`, `rent_code`, `rent_description`, `rent_resource`) VALUES
(1, 5000.01, 'rcd12589', 'lathe rent....', 'lathe-01');

-- --------------------------------------------------------

--
-- Table structure for table `repair`
--

CREATE TABLE `repair` (
  `repair_id` int(11) NOT NULL,
  `repair_charge` double DEFAULT NULL,
  `repair_code` varchar(255) DEFAULT NULL,
  `repair_description` varchar(255) DEFAULT NULL,
  `repair_name` varchar(255) DEFAULT NULL,
  `repair_type` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `repair`
--

INSERT INTO `repair` (`repair_id`, `repair_charge`, `repair_code`, `repair_description`, `repair_name`, `repair_type`) VALUES
(1, 3000.01, 'er12535', 'fadss', '25cc Engine Repair', 'ENGINE');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `role_id` int(11) NOT NULL,
  `created_time` datetime DEFAULT NULL,
  `modified_time` datetime DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `active` bit(1) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `created_time` datetime DEFAULT NULL,
  `modified_time` datetime DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `active` bit(1) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `created_time`, `modified_time`, `version`, `active`, `email`, `name`, `password`, `user_id`) VALUES
(1, '2017-09-30 00:00:00', '2017-09-30 00:00:00', 1, b'1', 'hdranawaeera@gmail.com', 'harshika', 'harshika123', 'harshika'),
(2, '2017-09-30 00:00:00', '2017-09-30 00:00:00', 1, b'1', 'ul2ravionline@gmail.com', 'ravi', 'ravi123', 'ravi');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`emp_id`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`invoice_id`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`item_id`);

--
-- Indexes for table `job`
--
ALTER TABLE `job`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKkbfwqadwjxuh6vqgis77q7c7f` (`employee_id`);

--
-- Indexes for table `job_item`
--
ALTER TABLE `job_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKspw24feqlk2js2lemp3na4gn9` (`item_id`),
  ADD KEY `FK52la4kqha9lw2xi86k8qqkbsl` (`job_id`);

--
-- Indexes for table `job_rent`
--
ALTER TABLE `job_rent`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK4tr50k88y49044hiy81h20u0t` (`job_id`),
  ADD KEY `FK97pogmdxmhcj3uoj9sbda7atp` (`rent_id`);

--
-- Indexes for table `job_repair`
--
ALTER TABLE `job_repair`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK8wgvqoju1wf9nvquubbs6xg1b` (`job_id`),
  ADD KEY `FKod678brb1b2n6ccmbruhp56pm` (`repair_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `rent`
--
ALTER TABLE `rent`
  ADD PRIMARY KEY (`rent_id`);

--
-- Indexes for table `repair`
--
ALTER TABLE `repair`
  ADD PRIMARY KEY (`repair_id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`,`role_id`),
  ADD KEY `FKa68196081fvovjhkek5m97n3y` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `emp_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `invoice_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `item`
--
ALTER TABLE `item`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `job`
--
ALTER TABLE `job`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `job_item`
--
ALTER TABLE `job_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `job_rent`
--
ALTER TABLE `job_rent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `job_repair`
--
ALTER TABLE `job_repair`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rent`
--
ALTER TABLE `rent`
  MODIFY `rent_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `repair`
--
ALTER TABLE `repair`
  MODIFY `repair_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `job`
--
ALTER TABLE `job`
  ADD CONSTRAINT `FKkbfwqadwjxuh6vqgis77q7c7f` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`emp_id`);

--
-- Constraints for table `job_item`
--
ALTER TABLE `job_item`
  ADD CONSTRAINT `FK52la4kqha9lw2xi86k8qqkbsl` FOREIGN KEY (`job_id`) REFERENCES `job` (`id`),
  ADD CONSTRAINT `FKspw24feqlk2js2lemp3na4gn9` FOREIGN KEY (`item_id`) REFERENCES `item` (`item_id`);

--
-- Constraints for table `job_rent`
--
ALTER TABLE `job_rent`
  ADD CONSTRAINT `FK4tr50k88y49044hiy81h20u0t` FOREIGN KEY (`job_id`) REFERENCES `job` (`id`),
  ADD CONSTRAINT `FK97pogmdxmhcj3uoj9sbda7atp` FOREIGN KEY (`rent_id`) REFERENCES `rent` (`rent_id`);

--
-- Constraints for table `job_repair`
--
ALTER TABLE `job_repair`
  ADD CONSTRAINT `FK8wgvqoju1wf9nvquubbs6xg1b` FOREIGN KEY (`job_id`) REFERENCES `job` (`id`),
  ADD CONSTRAINT `FKod678brb1b2n6ccmbruhp56pm` FOREIGN KEY (`repair_id`) REFERENCES `repair` (`repair_id`);

--
-- Constraints for table `user_role`
--
ALTER TABLE `user_role`
  ADD CONSTRAINT `FK2oqpqfi5ut3xylw61wqhl5ggo` FOREIGN KEY (`id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FKa68196081fvovjhkek5m97n3y` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
